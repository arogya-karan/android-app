package com.example.project;

import androidx.appcompat.app.AppCompatActivity;
import androidx.security.crypto.EncryptedSharedPreferences;
import androidx.security.crypto.MasterKeys;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Register extends AppCompatActivity {

    Button btn_reg, btn_fake;
    EditText et_name_reg, et_email_reg, et_email_pass, et_email_conf_pass, et_phone;
    TextView tv_address;
    private View mProgressView;
    private View mView;
    String abc;

    Integer in;
    //Boolean check;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        tv_address = findViewById(R.id.et_address_reg);
        et_name_reg = findViewById(R.id.et_name_reg);
        et_email_pass = findViewById(R.id.et_paass_reg);
        btn_fake = findViewById(R.id.btn_fake_reg);
        et_phone = findViewById(R.id.et_phone_reg);
        et_email_conf_pass = findViewById(R.id.et_pass_conf_reg);
        et_email_reg = findViewById(R.id.et_email_reg);
        mProgressView = findViewById(R.id.prog_reg);
        mView = findViewById(R.id.rl_view);
        btn_fake.setVisibility(View.GONE);

        if(!(getIntent().getStringExtra("address") ==null)){

            tv_address.setText(getIntent().getStringExtra("address"));
            et_name_reg.setText(getIntent().getStringExtra("name"));
            et_phone.setText(getIntent().getStringExtra("phone"));




        }


        btn_reg = findViewById(R.id.btn_reg);
        btn_reg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                if(isEmpty(et_name_reg) || isEmpty(et_phone) || isEmpty(et_email_reg) || isEmpty(et_email_pass) || isEmpty(et_email_conf_pass)){
                    Toast.makeText(getApplicationContext(),"All fields compulsory",Toast.LENGTH_SHORT).show();
                }
                else{

                    if(et_email_pass.getText().toString().equals(et_email_conf_pass.getText().toString())) {

                        if(et_email_pass.getText().toString().length()>=8){
                            post_it();
                        }
                        else{
                            Toast.makeText(getApplicationContext(),"Password Must be 8 characters long",Toast.LENGTH_LONG).show();

                        }

                    }
                    else{
                        Toast.makeText(getApplicationContext(),"Password mismatch",Toast.LENGTH_SHORT).show();
                    }
                }




            }
        });






    }
    private boolean isEmpty(EditText etText) {

        if(etText.getText().toString().trim().length() == 0){
            etText.setError("Cannot be Empty");

            return true;
        }
        else{
            return false;
        }
    }

    public void post_it(){
        showProgress(true);


        final  String name = et_name_reg.getText().toString().trim();
        final  String phone = et_phone.getText().toString().trim();
        final  String email = et_email_reg.getText().toString().trim();
        final  String password = et_email_pass.getText().toString().trim();
        final  String conf_pass = et_email_conf_pass.getText().toString().trim();
        final  String address = getIntent().getStringExtra("address");//


        user_reg user_reg = new user_reg();
        user_reg.setEmail(email);
        user_reg.setName(name);
        user_reg.setPassword(password);
        user_reg.setPhone(phone);
        user_reg.setAddress(address);

        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        List<Address> addresses = null;
        try {
            addresses = geocoder.getFromLocationName(address, 1);
            Address address2 = addresses.get(0);
            double longitude = address2.getLongitude();
            double latitude = address2.getLatitude();
            user_reg.setLongi(String.valueOf(longitude));
            user_reg.setLat(String.valueOf(latitude));



        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(getApplicationContext(),e.getMessage(),Toast.LENGTH_SHORT).show();
        }

        Call<response_here> call = retro_class.getInstance_two().getapi2().create_user(user_reg);
        call.enqueue(new Callback<response_here>() {
            @Override
            public void onResponse(Call<response_here> call, Response<response_here> response) {


                assert response.body() != null;
                String string = null;
                try {




                    if (response.code() == 200) {
                        //string = response.body();
                       showProgress(false);

                       Toast.makeText(Register.this,"A link has been sent to your email",Toast.LENGTH_LONG).show();
                       btn_reg.setVisibility(View.GONE);
                       btn_fake.setVisibility(View.VISIBLE);

                        Shared_pref shared_pref = new Shared_pref(Register.this);
                        shared_pref.add_address(address);
                      //  shared_pref.add_token(response.body().getToken());



                    } else {
                        assert response.errorBody() != null;
                        string = response.errorBody().string();
                        Toast.makeText(getApplicationContext(),"error : " + response.message(),Toast.LENGTH_SHORT).show();

                       /* Shared_pref shared_pref = new Shared_pref(Register.this);
                        shared_pref.add_address(address);*/



                    }
                    //string = response.body().string();
                } catch (Exception e) {
                    e.printStackTrace();
                    showProgress(false);
                    Toast.makeText(Register.this,"Check your internet connection : " + e.getMessage(),Toast.LENGTH_LONG).show();



                }
                //Gson gson = new Gson();
                //String string =  gson.toJson(response.body());
                //Toast.makeText(MainActivity.this,string,Toast.LENGTH_LONG).show();


                //tverror.setText(string);

            }

            @Override
            public void onFailure(Call<response_here> call, Throwable t) {
                Toast.makeText(Register.this, "error" + t.getMessage(), Toast.LENGTH_LONG).show();

            }
        });
    }

    public void go_to_login(View view) {

        startActivity(new Intent(this,com.example.project.MainActivity.class));
    }

    public void address(View view) {

        Intent intent = new Intent(this,address.class);
        intent.putExtra("name",et_name_reg.getText().toString());
        intent.putExtra("phone" , et_phone.getText().toString());
        startActivity(intent);



    }

    public void fake(View view) {
        Toast.makeText(Register.this,"A link has already been sent to your email",Toast.LENGTH_LONG).show();
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mView.setVisibility(show ? View.GONE : View.VISIBLE);
            mView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });

        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            // tvLoad.setVisibility(show ? View.VISIBLE : View.GONE);
            mView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }


//imp

   /* private boolean checking_empty(){
        if(name.isEmpty()|| phone.isEmpty() || email.isEmpty() || password.isEmpty() || conf_pass.isEmpty()||address.isEmpty()){
            return false;

        }else{
           return true;
        }
    }
    private boolean pass(){
        if(password.equals(conf_pass)){
            return  true;

        }
        else
        {
            return false;

        }
    }
    private boolean correct(){
        if(pass() && checking_empty() ){
            return true;
        }
        else{
            return false;
        }
    }

    public static boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }*/

   /* public void register(View view) {
        post_it();
        *//*if(correct()){
            post_it();

        }
        else{
            Toast.makeText(Register.this,"no",Toast.LENGTH_SHORT).show();
        }
*//*

    }*/
}
