package com.example.project.ui.home;

public class act_list_class {

    private String _id;
    private String quantity;
    private String pid;
    private String sid;
    private String dateofRequest;
    private Boolean isFinalised;

    public Boolean getFinalised() {
        return isFinalised;
    }

    public void setFinalised(Boolean finalised) {
        isFinalised = finalised;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }

    public String getDateofRequest() {
        return dateofRequest;
    }

    public void setDateofRequest(String dateofRequest) {
        this.dateofRequest = dateofRequest;
    }
}
