package com.example.project.ui.home;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.project.R;
import com.example.project.cases;
import com.example.project.recycler_adapter;
import com.example.project.ui.send.recycler_adapter_products;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class search_adapter extends RecyclerView.Adapter<search_adapter.MyHolder> {
    private Context context;

    private LayoutInflater inflater;
    private ArrayList<sel> dataModelArrayList;
    private search_adapter.OnNoteList onNoteList;

    public search_adapter(Context ctx, ArrayList<sel> dataModelArrayList, OnNoteList onNoteList){

        inflater = LayoutInflater.from(ctx);
        this.dataModelArrayList = dataModelArrayList;
        this.onNoteList = onNoteList;
        this.context = ctx;
    }


    @NonNull
    @Override
    public search_adapter.MyHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.product_search_row_layout, parent, false);
        MyHolder holder = new MyHolder(view,onNoteList);

        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull search_adapter.MyHolder holder, int position) {
        Picasso.with(context).load(dataModelArrayList.get(position).getImageThumbnail()).into(holder.iv);
        holder.product_certified.setText("Certified : " +dataModelArrayList.get(position).getIsCertified());
        if(dataModelArrayList.get(position).getProductDesc().length() > 30 ){
            holder.prodcut_desc_seller.setText("Description : "+dataModelArrayList.get(position).getProductDesc().substring(0,9) + ".....");
        }
        else{
            holder.prodcut_desc_seller.setText("Description : "+dataModelArrayList.get(position).getProductDesc());
        }

        holder.product_available.setText("Available : "+dataModelArrayList.get(position).getAvailablity());
        holder.product_price_seller.setText("Price : "+dataModelArrayList.get(position).getProductPrice() + " Rupees Only");

        holder.product_type_seller.setText("Type : " +dataModelArrayList.get(position).getProductType());

        holder.distance.setText("Distance : " + dataModelArrayList.get(position).getDis() + " Km");
        holder.product_name_seller.setText(dataModelArrayList.get(position).getProductName());
        // holder.product_quantity_seller.setText("Quantity : "+dataModelArrayList.get(position).getQuantity());
        // holder.seller_name.setText("Recoveries : "+dataModelArrayList.get(position).getSellerName());
        //  holder.seller_id.setText("Seller ID : "+dataModelArrayList.get(position).getSellerID());



    }

    @Override
    public int getItemCount() {
        return dataModelArrayList.size();
    }

    public class MyHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView product_name_seller, prodcut_desc_seller, product_type_seller, product_quantity_seller, product_price_seller, product_available,
                product_certified, seller_name, seller_id , distance;
        ImageView iv;
        OnNoteList onNoteList;
        public MyHolder(@NonNull View itemView, OnNoteList onNoteList) {
            super(itemView);

            prodcut_desc_seller = itemView.findViewById(R.id.product_desc_seller);
            product_available = itemView.findViewById(R.id.available_seller);
            // product_quantity_seller = itemView.findViewById(R.id.quantity_seller);
            product_price_seller = itemView.findViewById(R.id.price_seller);
            product_type_seller = itemView.findViewById(R.id.product_type_seller);
            product_name_seller = itemView.findViewById(R.id.product_name_sell);
            product_certified = itemView.findViewById(R.id.certified_seller);
            //   seller_name = itemView.findViewById(R.id.seller_name_seller);
            //seller_id = itemView.findViewById(R.id.seller_id_seller);

            iv = itemView.findViewById(R.id.iv_product_seller);
            distance = itemView.findViewById(R.id.distance_seller);
            itemView.setOnClickListener(this);
            this.onNoteList = onNoteList;

        }

        @Override
        public void onClick(View v) {
            onNoteList.OnnoteClick(dataModelArrayList.get(getAdapterPosition()));
        }
    }
    public interface OnNoteList {
        void OnnoteClick(sel sel);


    }
    public void filteredlist(ArrayList<sel> filterlist){
        dataModelArrayList = filterlist;
        notifyDataSetChanged();
    }
}

