package com.example.project.ui.home;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.doodle.android.chips.model.Contact;
import com.example.project.R;
import com.example.project.Shared_pref;
import com.example.project.api_header;
import com.example.project.ui.send.recycler_adapter_products;
import com.google.gson.JsonArray;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class images extends AppCompatActivity {
    RecyclerView recyclerView;
    images_adapter adapter;
    ArrayList arrayList;
    TextView tv_empty;
    String id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_images);
        recyclerView = findViewById(R.id.rv_images);
        arrayList = new ArrayList();
        tv_empty = findViewById(R.id.tv_empty);
        tv_empty.setVisibility(View.GONE);

        id = getIntent().getStringExtra("id");
        Shared_pref shared_pref = new Shared_pref(getApplicationContext());
        final String token = shared_pref.get_token();
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request newRequest = chain.request().newBuilder()
                        .addHeader("Authorization", " Bearer " + token)
                        .build();
                return chain.proceed(newRequest);
            }
        }).build();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://covid-19-info-123.herokuapp.com/")
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        api_header api = retrofit.create(api_header.class);

        Call<ResponseBody> call = api.get_products_by_product(id);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {

                    String hello = null;
                    if (response.body() == null) {
                        //id);
                        //response.body().string());
                        tv_empty.setVisibility(View.VISIBLE);

                    } else {
                        tv_empty.setVisibility(View.GONE);
                        hello = response.body().string();
                        //hello);
                        JSONObject object = new JSONObject(hello);

                        JSONObject obj = object.getJSONObject("product");
                        JSONArray array = obj.getJSONArray("media");
                        //array);

                        //arrayList = array;
                        adapter = new images_adapter(array, getApplicationContext());
                        recyclerView.setAdapter(adapter);
                        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));


                        // Picasso.with(getApplicationContext()).load(Uri.parse((obj.getString("imageThumbnail")).trim())).into(iv);


                        //   writeRecycler(hello);
                    }
                }catch (IOException | JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {


            }
        });

    }

    public void back_to_pro_details(View view) {
        String sellerid = getIntent().getStringExtra("seller");
        String uid = getIntent().getStringExtra("uid");
        Intent intent = new Intent(this,product_details.class);
        intent.putExtra("id",id);
        if(uid!=null) {
            intent.putExtra("selleruid", uid);
        }
        intent.putExtra("seller",sellerid);
        startActivity(intent);
    }
}
