package com.example.project;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class retro_class {

    private static final String base_url = "https://covid-19-info-123.herokuapp.com/api/";
    private static retro_class instance;
    private Retrofit retrofit; //retrofit object

    private retro_class() { //constructor
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder()
                .callTimeout(2, TimeUnit.MINUTES)
                .connectTimeout(20, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS);

        retrofit = new Retrofit.Builder().baseUrl(base_url).addConverterFactory(GsonConverterFactory.create()).client(httpClient.build()).build();

    }

    public static synchronized retro_class getInstance_two() {
        if (instance == null) {
            instance = new retro_class();
        }
        return instance;

    }

    public api_jwt getapi2() {
        return retrofit.create(api_jwt.class);
    }
}
