package com.example.project;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Covid_info extends AppCompatActivity {
    Button btn_mha, btn_mohfw;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_covid_info);
        btn_mha = findViewById(R.id.mha);
        btn_mohfw = findViewById(R.id.mohfw);


        Shared_pref shared_pref = new Shared_pref(getApplicationContext());
        String address = shared_pref.get_Address();
        if(address.toLowerCase().contains("india")){
            btn_mohfw.setVisibility(View.VISIBLE);
            btn_mha.setVisibility(View.VISIBLE);
        }
        else{
            btn_mha.setVisibility(View.GONE);
            btn_mohfw.setVisibility(View.GONE);
        }
    }

    public void protect(View view) {
        Uri uri = Uri.parse("https://www.cdc.gov/coronavirus/2019-nCoV/index.html"); // missing 'http://' will cause crashed
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(intent);
    }
    public void mohfw(View view) {
        Uri uri = Uri.parse("https://www.mohfw.gov.in/"); // missing 'http://' will cause crashed
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(intent);
    }
    public void mha(View view) {
        Uri uri = Uri.parse("https://www.mha.gov.in/"); // missing 'http://' will cause crashed
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(intent);
    }


    public void back_to_product_show(View view) {
        if(getIntent().getStringExtra("c")==null){
            startActivity(new Intent(getApplicationContext(), com.example.project.profile.class));
        }
        else {
            startActivity(new Intent(getApplicationContext(), com.example.project.ui.welcome.class));
        }
    }

    public void cases(View view) {
        Intent intent = new Intent(getApplicationContext(),com.example.project.ui.slideshow.view_list.class);
        intent.putExtra("d","d");
        startActivity(intent);
    }
}
