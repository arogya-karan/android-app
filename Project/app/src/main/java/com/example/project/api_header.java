package com.example.project;

import com.example.project.ui.home.buy;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.HTTP;
import retrofit2.http.Header;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface api_header {

    @POST("api/product/addproduct") //change
    Call<ResponseBody> add_product_jwt(@Body add_product_class product_class);

    @POST("api/seller/addseller")
    Call<response_here> add_seller_jwt(@Body add_seller_class add_seller_class);

    @GET("api/seller/products")
    Call<ResponseBody> get_seller_products();

    @GET("api/product/products/{code}")
    Call<ResponseBody> get_one_product(@Path("code") String code);

    @GET("api/seller/nearby")
    Call<ResponseBody> get_nearby();

    @GET("api/seller/productsbyid/{sellerid}")
    Call<ResponseBody> get_products_by_seller(@Path("sellerid") String sellerid);

    @GET("api/product/productsbyid/{productid}")
    Call<ResponseBody> get_products_by_product(@Path("productid") String productid);

    @POST("api/trans/request")
    Call<response_here> post_buy(@Body buy buy);

    @GET("api/user/{uid}")
    Call<ResponseBody> get_user(@Path("uid") String id);

    /*@POST("api/seller/request")
    Call<response_here> on_accept(@Body com.example.project.ui.home.ids_class ids_class);

*/

    @GET("api/trans/request?onlySeller=true")
    Call<ResponseBody> notif_list();

    @POST("api/trans/transaction")
    Call<ResponseBody> accept_transaction(@Body com.example.project.ui.home.transac_class transac_class);

    @GET("api/trans/request")
    Call<ResponseBody> get_active_request();

    @GET("api/seller/{sid}")
    Call<ResponseBody> get_seller_from_id(@Path("sid") String sid);

    @GET("api/trans/transaction")
    Call<ResponseBody> get_accepted();

    @GET("api/trans/transaction?onlySeller=true")
    Call<ResponseBody> seller_accepts();



    @GET("api/seller/near")
    Call<ResponseBody> getnear(@Query("long") String longi, @Query("lat") String lat);

    @GET("/api/product/{pid}")

    Call<ResponseBody> get_prod_by_id(@Path("pid") String pid);

    @GET("api/seller/products/{sid}")
    Call<ResponseBody> get_prod_by_sid(@Path("sid")String sid);


    @DELETE("api/product/{pid}")
    Call<ResponseBody> delete_prod(@Path("pid") String pid);

    @PUT("api/product")
    Call<ResponseBody> edit_prod(@Body body_edit body_edit);

    @POST("api/email")
    Call<ResponseBody> send_email(@Body com.example.project.ui.home.mail mail);

    @GET("api/product/search")
    Call<ResponseBody> search(@Query("key") String mask);

    @GET("api/seller/uid/{uid}")
    Call<ResponseBody> get_seller_by_uid(@Path("uid") String uid);








}
