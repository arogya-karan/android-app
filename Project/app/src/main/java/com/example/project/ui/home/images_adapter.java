package com.example.project.ui.home;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.project.R;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

public class images_adapter extends RecyclerView.Adapter<com.example.project.ui.home.images_adapter.MyHolder> {
    ArrayList<String> arrayList;
    JSONArray array;
    private Context context;
    private LayoutInflater inflater;

    public images_adapter(JSONArray array, Context context) {
        this.array = array;
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public images_adapter.MyHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.images_row_list, parent, false);
        images_adapter.MyHolder holder = new images_adapter.MyHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull images_adapter.MyHolder holder, int position) {
        try {
           // holder.imageView.setImageURI(Uri.parse(String.valueOf(array.get(position))));
            Picasso.with(context).load(String.valueOf(array.get(position))).into(holder.imageView);
        } catch (JSONException e) {
            e.printStackTrace();
            Toast.makeText(context,e.getMessage(),Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public int getItemCount() {
        return array.length();
    }

    public class MyHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        public MyHolder(@NonNull View itemView) {
            super(itemView);

            imageView = itemView.findViewById(R.id.iv_image);
        }
    }
}
