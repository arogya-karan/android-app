package com.example.project;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class edit_del_prod_class {
    @SerializedName("productName")
    private String productName;
    @SerializedName("productDesc")
    private String productDesc;
    @SerializedName("productType")
    private String productType;
    @SerializedName("productTags")
    private ArrayList<String> productTags;
    @SerializedName("productPrice")
    private Integer productPrice;
    @SerializedName("imageThumbnail")
    private String imageThumbnail;
    @SerializedName("media")
    private ArrayList<String> media;
    @SerializedName("quantity")
    private Integer quantity;
    @SerializedName("availablity")
    private Boolean avail;
    @SerializedName("isCertified")
    private Boolean isCertified;

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductDesc() {
        return productDesc;
    }

    public void setProductDesc(String productDesc) {
        this.productDesc = productDesc;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public ArrayList<String> getProductTags() {
        return productTags;
    }

    public void setProductTags(ArrayList<String> productTags) {
        this.productTags = productTags;
    }

    public Number getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(Integer productPrice) {
        this.productPrice = productPrice;
    }

    public String getImageThumbnail() {
        return imageThumbnail;
    }

    public void setImageThumbnail(String imageThumbnail) {
        this.imageThumbnail = imageThumbnail;
    }

    public ArrayList<String> getMedia() {
        return media;
    }

    public void setMedia(ArrayList<String> media) {
        this.media = media;
    }

    public Number getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Boolean getAvail() {
        return avail;
    }

    public void setAvail(Boolean avail) {
        this.avail = avail;
    }

    public Boolean getCertified() {
        return isCertified;
    }

    public void setCertified(Boolean certified) {
        isCertified = certified;
    }
}
