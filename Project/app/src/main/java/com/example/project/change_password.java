package com.example.project;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class change_password extends AppCompatActivity {

    EditText et_new_pass, et_old_pass, et_new_pass_confirmed;
    Button btn_submit_change_password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        et_new_pass = findViewById(R.id.et_new_pass);
        et_old_pass = findViewById(R.id.et_old_pass);
        et_new_pass_confirmed = findViewById(R.id.et_new_pass_confirmed);
        btn_submit_change_password = findViewById(R.id.btn_pass_change_submit);



    }

    public void changePassword(View view)  {
        if(et_new_pass.getText().toString().isEmpty() || et_new_pass_confirmed.getText().toString().isEmpty()){
            Toast.makeText(getApplicationContext(),"all text fields compulsory",Toast.LENGTH_LONG).show();


        }
        else{
            if(et_new_pass.getText().toString().equals(et_new_pass_confirmed.getText().toString())){

                if(et_new_pass.getText().toString().length()>8){
                    change_pass_class change_pass_class = new change_pass_class();
                    final Shared_pref shared_pref = new Shared_pref(this);
                    String token = shared_pref.get_token();
                    //"old token : " + token);
                    try {
                        String user =  JWUtils.decodeJWT(token);
                        JSONObject jsonObject = new JSONObject(user);
                        JSONObject ob = jsonObject.getJSONObject("user");

                        String email = ob.getString("email");







                        change_pass_class.setUser_email(email);
                        //"email : " + email);
                        change_pass_class.setNewPassword(et_new_pass.getText().toString());
                        change_pass_class.setOldPassword(et_old_pass.getText().toString());

                        Call<response_here> call = retro_class.getInstance_two().getapi2().change(change_pass_class);
                        call.enqueue(new Callback<response_here>() {
                            @Override
                            public void onResponse(Call<response_here> call, Response<response_here> response) {
                                if(response.code()==200){
                                    assert response.body() != null;
                                    //response.body().isSuccess());


                                    shared_pref.clear();
                                    shared_pref.add_token(response.body().getToken());
                                    Toast.makeText(getApplicationContext(),"Password Changed!",(Toast.LENGTH_SHORT)).show();
                                    startActivity(new Intent(getApplicationContext(),com.example.project.profile.class));



                                }
                                else{
                                    assert response.body() != null;
                                    //"error");
                                    Toast.makeText(getApplicationContext(),"error : " + response.message() ,Toast.LENGTH_SHORT).show();

                                }
                            }

                            @Override
                            public void onFailure(Call<response_here> call, Throwable t) {
                                Toast.makeText(getApplicationContext(),"error : " +t.getMessage() ,Toast.LENGTH_SHORT).show();

                            }
                        });


                    } catch (Exception e) {
                        e.printStackTrace();
                        Toast.makeText(getApplicationContext(),"error : " +e.getMessage() ,Toast.LENGTH_SHORT).show();
                    }




                }
                else{
                    Toast.makeText(getApplicationContext(),"password must be 8 characters long",Toast.LENGTH_LONG).show();

                }
            }
            else{
                Toast.makeText(getApplicationContext(),"Password Mismatch",Toast.LENGTH_LONG).show();

            }

        }

    }

    public void change_passs_back(View view) {
        startActivity(new Intent(this,com.example.project.profile.class));
    }
}
