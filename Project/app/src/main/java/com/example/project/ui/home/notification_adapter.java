package com.example.project.ui.home;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.project.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class notification_adapter extends RecyclerView.Adapter<notification_adapter.MyHolder> {

    private Context context;
    private LayoutInflater inflater;
    private ArrayList<product_class> modelArrayList;
    private OnNoteList onNoteList;

    public notification_adapter(Context context, ArrayList<product_class> modelArrayList,OnNoteList onNoteList) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.modelArrayList = modelArrayList;
        this.onNoteList = onNoteList;
    }

    @NonNull
    @Override
    public notification_adapter.MyHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.notification_product, parent, false);
        notification_adapter.MyHolder holder = new notification_adapter.MyHolder(view, onNoteList);

        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull notification_adapter.MyHolder holder, int position) {
        holder.product_name.setText(modelArrayList.get(position).getProductName());
        holder.product_type.setText("Product Type : " + modelArrayList.get(position).getProductTypes());
        holder.product_price.setText("Price : " + modelArrayList.get(position).getProductPrice() + " only");
        holder.date.setText("Date : " +modelArrayList.get(position).getDate());
        holder.buyer_name.setText("Requested By : " + modelArrayList.get(position).getBuyer_name());
        Picasso.with(context).load(modelArrayList.get(position).getImgThumbnail()).into(holder.iv_request);
        holder.buyer_quantity.setText("Quantity Required : " + modelArrayList.get(position).getBuyer_quantity());


    }

    @Override
    public int getItemCount() {
        return modelArrayList.size();
    }

    public class MyHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView product_name, product_type, product_price, buyer_name, buyer_quantity, date;
        ImageView iv_request;
        OnNoteList onNoteList;


        public MyHolder(@NonNull View itemView, OnNoteList onNoteList) {
            super(itemView);

            product_name = itemView.findViewById(R.id.product_name_req);
            product_price = itemView.findViewById(R.id.product_price_req);
            product_type = itemView.findViewById(R.id.product_type_req);
            buyer_name = itemView.findViewById(R.id.buyer_name_request);
            buyer_quantity = itemView.findViewById(R.id.quantity_req);
            date = itemView.findViewById(R.id.date);
            iv_request = itemView.findViewById(R.id.iv_product_req);
            itemView.setOnClickListener(this);
            this.onNoteList = onNoteList;

        }

        @Override
        public void onClick(View v) {
            onNoteList.OnnoteClick(getAdapterPosition());

        }
    }
    public interface OnNoteList {
        void OnnoteClick(int position);


    }
}
