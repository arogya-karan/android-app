package com.example.project.ui.home;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.project.R;
import com.example.project.Shared_pref;
import com.example.project.api_header;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class request_details extends AppCompatActivity {

    TextView tv_name,tv_email,tv_phone,tv_Address, tv_quantity, tv_date, tvprod_name,tv_prod_price ,tv_type;
    String img, prod_name, prod_type, prod_price, date, quantity;
    ImageView iv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request_details);

        tv_name = findViewById(R.id.call_buyer_name);
        iv = findViewById(R.id.iv_product_seller);
        tvprod_name = findViewById(R.id.call_product_name);
        tv_type = findViewById(R.id.call_tv_type);
        tv_prod_price = findViewById(R.id.call_tv_price_show);
        tv_email = findViewById(R.id.call_tv_email);
        tv_phone = findViewById(R.id.call_tv_phone);
        tv_date = findViewById(R.id.tv_date_2);
        tv_quantity = findViewById(R.id.call_quant_required);
        tv_Address = findViewById(R.id.call_tv_address);

        if(getIntent().getStringExtra("product_name")!=null){

            prod_name = getIntent().getStringExtra("product_name");
            prod_type = getIntent().getStringExtra("product_type");
            prod_price = getIntent().getStringExtra("product_price");
            img = getIntent().getStringExtra("img");
            date = getIntent().getStringExtra("date");
            quantity = getIntent().getStringExtra("quantity");

            tv_type.setText(prod_type);
            tv_prod_price.setText(prod_price);
            tvprod_name.setText(prod_name);
            tv_date.setText(getDate(Long.parseLong(date)));
            tv_quantity.setText(quantity);
            Picasso.with(this).load(img).into(iv);





            String uid = getIntent().getStringExtra("uid");
            final Shared_pref shared_pref = new Shared_pref(getApplicationContext());
            final String token = shared_pref.get_token();
            OkHttpClient client = new OkHttpClient.Builder().addInterceptor(new Interceptor() {
                @Override
                public okhttp3.Response intercept(Chain chain) throws IOException {
                    Request newRequest = chain.request().newBuilder()
                            .addHeader("Authorization", " Bearer " + token)
                            .build();
                    return chain.proceed(newRequest);
                }
            }).build();
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl("https://covid-19-info-123.herokuapp.com/")
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            api_header api = retrofit.create(api_header.class);

            Call<ResponseBody> call = api.get_user(uid);
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    try {
                        assert response.body() != null;
                        String user = response.body().string();
                        JSONObject object = new JSONObject(user).getJSONObject("user");
                        tv_name.setText(object.getString("name"));
                        tv_Address.setText(object.getString("address"));
                        tv_email.setText(object.getString("email"));
                        tv_phone.setText(object.getString("phone"));


                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {

                }
            });
        }
    }




    private String getDate(long timeStamp){

        try{
            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
            Date netDate = (new Date(timeStamp));
            return sdf.format(netDate);
        }
        catch(Exception ex){
            return "xx";
        }
    }

    public void back(View view) {

        startActivity(new Intent(request_details.this,com.example.project.profile.class));
    }

    public void call(View view) {
        if(ContextCompat.checkSelfPermission(request_details.this, Manifest.permission.CALL_PHONE)!= PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(request_details.this,new String[] {Manifest.permission.CALL_PHONE},1);

        }
        else{
            String num = tv_phone.getText().toString();

            Intent intent = new Intent(Intent.ACTION_CALL, Uri.fromParts("tel", num, null));
            startActivity(intent);
            //startActivity(new Intent(Intent.ACTION_CALL, Uri.parse(num)));
        }


    }
}
