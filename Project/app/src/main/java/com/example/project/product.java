package com.example.project;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.cloudinary.Transformation;
import com.cloudinary.android.MediaManager;
import com.cloudinary.android.callback.ErrorInfo;
import com.cloudinary.android.callback.UploadCallback;
import com.doodle.android.chips.ChipsView;
import com.doodle.android.chips.model.Contact;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class product extends AppCompatActivity {

    EditText et_product_name, et_prod_desc, et_prod_price, quantity;
    ImageView img1, img2, img3, img4, img5 ,img6;
    CheckBox checkBox, certified;
    TextView tv_cerified, et_prod_tags, et_prod_type;
    Button btn_product_image;
    Button btn_submit, btnedit;
    private View mProgressView;
    RadioButton r1,r2,r3,r4,r5,r6,r7;
    RadioGroup rg1,rg2;
    RadioButton rb;
    private View mView;
    ArrayList<String> arrayList;
    String thumbnail;

    //business
    TextView price_tag;
    CheckBox want_price;

    private RecyclerView mContacts;
    private ContactsAdapter mAdapter;
    String bfd;
    ArrayList<String> arrayList2;
    private ChipsView mChipsView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product);

        et_prod_desc = findViewById(R.id.et_prod_desc);
        rg1 = findViewById(R.id.rg1);


        et_product_name = findViewById(R.id.et_prod_name);
        btn_submit = findViewById(R.id.bt_product_submit);
        btnedit = findViewById(R.id.bt_product_edit);
        price_tag = findViewById(R.id.price_tag); //
        want_price = findViewById(R.id.want_price); //
        mProgressView = findViewById(R.id.product_progress);
        mView = findViewById(R.id.llview);
        et_prod_price = findViewById(R.id.et_product_price); //
        btnedit.setVisibility(View.GONE);
        et_prod_type = findViewById(R.id.et_product_type);
        //et_prod_tags = findViewById(R.id.et_product_tags);
        quantity = findViewById(R.id.et_quantity);
        checkBox = findViewById(R.id.check);
        tv_cerified = findViewById(R.id.tv_certified);
        tv_cerified.setVisibility(View.GONE);
        certified = findViewById(R.id.check_certified);
        arrayList = new ArrayList<String>();

        //want_price.setVisibility(View.GONE);


        img1 = findViewById(R.id.imageView);
        img2 = findViewById(R.id.imageView2);
        img3 = findViewById(R.id.imageView3);
        img4 = findViewById(R.id.imageView4);
        img5 = findViewById(R.id.imageView5);
        img6 = findViewById(R.id.imageView6);

        img2.setVisibility(View.GONE);
        img3.setVisibility(View.GONE);
        img4.setVisibility(View.GONE);
        img5.setVisibility(View.GONE);

        Shared_pref shared_pref = new Shared_pref(getApplicationContext());

        if(shared_pref.get_is_business()){
            want_price.setVisibility(View.GONE);
        }
        else{
            want_price.setVisibility(View.VISIBLE);
            et_prod_price.setVisibility(View.GONE);
            price_tag.setVisibility(View.GONE);

        }

        if(want_price.isChecked()){
            et_prod_price.setVisibility(View.VISIBLE);
            price_tag.setVisibility(View.VISIBLE);


        }
        else{
            et_prod_price.setVisibility(View.GONE);
            price_tag.setVisibility(View.GONE);

        }

        mContacts = (RecyclerView) findViewById(R.id.rv_contacts);
        mContacts.setLayoutManager(new LinearLayoutManager(this));
        arrayList2 = new ArrayList<>();
        mAdapter = new ContactsAdapter();
        mContacts.setAdapter(mAdapter);

        mChipsView = (ChipsView) findViewById(R.id.cv_contacts);

        mContacts.setVisibility(View.GONE);

        // change EditText config
        mChipsView.getEditText().setCursorVisible(true);

        mChipsView.setChipsValidator(new ChipsView.ChipValidator() {
            @Override
            public boolean isValid(Contact contact) {
                if (contact.getDisplayName().equals("check")) {
                    return false;
                }
                return true;
            }
        });

        inten();





    }
    private void inten(){
        Shared_pref shared_pref = new Shared_pref(getApplicationContext());

        if(getIntent().getStringExtra("click")!=null){
            btnedit.setVisibility(View.VISIBLE);
            btn_submit.setVisibility(View.GONE);


            // Shared_pref shared_pref = new Shared_pref(this);
            String code = getIntent().getStringExtra("click");

            final String token = shared_pref.get_token();

            OkHttpClient client = new OkHttpClient.Builder().addInterceptor(new Interceptor() {
                @Override
                public okhttp3.Response intercept(Chain chain) throws IOException {
                    Request newRequest  = chain.request().newBuilder()
                            .addHeader("Authorization", " Bearer " + token)
                            .build();
                    return chain.proceed(newRequest);
                }
            }).build();
            Retrofit retrofit  = new Retrofit.Builder()
                    .baseUrl("https://covid-19-info-123.herokuapp.com/")
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            api_header api = retrofit.create(api_header.class);

            Call<ResponseBody> call = api.get_one_product(code);
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if(response.code() == 200) {
                        try {
                            edit_del_prod_class edit_del_prod_class = new edit_del_prod_class();


                            assert response.body() != null;
                            String res = response.body().string();

                            JSONObject object = new JSONObject(res);
                            JSONObject product = object.getJSONObject("product");
                            et_product_name.setText(product.getString("productName"));
                            et_prod_desc.setText(product.getString("productDesc"));



                            if(product.getString("productPrice").equals("0")){
                                et_prod_price.setVisibility(View.GONE);
                                price_tag.setVisibility(View.GONE);
                                want_price.setVisibility(View.VISIBLE);
                                want_price.setChecked(true);
                            }
                            else{
                                et_prod_price.setVisibility(View.VISIBLE);
                                et_prod_price.setText(product.getString("productPrice"));
                                price_tag.setVisibility(View.VISIBLE);
                                want_price.setVisibility(View.GONE);
                                want_price.setChecked(false);

                            }


                            et_prod_type.setText(product.getString("productType"));
                            if(product.getString("quantity").equals(String.valueOf(1000000000))){
                                quantity.setText(String.valueOf("not defined by seller"));
                            }
                            else{
                            quantity.setText(product.getString("quantity"));}
                            showProgress(false);
                            //tags
                            String tags;
                            tags = product.getString("productTags");

                            tags = tags.replace('"',' ').replace("["," ").replace("]"," ");
                            tags = tags.replace("\\"," ");

                            String[] tg;
                            tg = tags.split(",");
                            for (String s : tg) {
                                s = s.trim();
                                Contact contact2 = new Contact(null, null,  s,s,null);

                                mChipsView.addChip(s, null, contact2, false);

                            }
                            //

                            Picasso.with(getApplicationContext()).load(Uri.parse((product.getString("imageThumbnail")).trim())).into(img1);

                            /*if(med.contains("[") && med.contains("]")) {
                                med = med.replaceAll(" ","");
                            }*/

                           JSONArray arr = product.getJSONArray("media");
                           arrayList = new ArrayList<>();
                           for(int i = 0; i<arr.length() ; i++){
                               arrayList.add(arr.getString(i));
                           }
                           // arrayList.add(med.trim());


                           /* img1.setImageURI(null);
                            img1.setImageURI(Uri.parse(Uri.decode(product.getString("imageThumbnail")).trim()));*/
                            if(product.get("isCertified").equals(true)){
                                certified.setChecked(true);
                                tv_cerified.setVisibility(View.VISIBLE);
                            }
                            else{
                                certified.setChecked(false);
                                tv_cerified.setVisibility(View.GONE);
                            }
                            if(product.get("availablity").equals(true)){
                                checkBox.setChecked(true);
                            }
                            else{
                                checkBox.setChecked(false);
                            }
                            JSONArray array = product.getJSONArray("media");
                            img2.setImageResource(R.drawable.ic_add_a_photo_black_24dp);
                            img3.setImageResource(R.drawable.ic_add_a_photo_black_24dp);
                            img4.setImageResource(R.drawable.ic_add_a_photo_black_24dp);
                            img5.setImageResource(R.drawable.ic_add_a_photo_black_24dp);
                            img6.setImageResource(R.drawable.ic_video_call_black_24dp);
                            if(array.length() > 0 && array.get(0) != null){
                                Picasso.with(getApplicationContext()).load(Uri.parse(array.get(0).toString())).into(img2);
                            }
                            if(array.length() > 1 && array.get(1) != null){
                                Picasso.with(getApplicationContext()).load(Uri.parse(array.get(1).toString())).into(img3);
                            }
                            if(array.length() > 2 && array.get(2) != null){
                                Picasso.with(getApplicationContext()).load(Uri.parse(array.get(2).toString())).into(img4);
                            }
                            if(array.length() > 3 && array.get(3) != null){
                                Picasso.with(getApplicationContext()).load(Uri.parse(array.get(3).toString())).into(img5);
                            }
                            if(array.length() > 4 && array.get(4) != null){
                                Picasso.with(getApplicationContext()).load(Uri.parse(array.get(4).toString())).into(img6);
                            }
                        } catch (IOException | JSONException e) {
                            e.printStackTrace();
                        }


                    }
                    else{
                       Toast.makeText(getApplicationContext(),response.code(),Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {

                }
            });

        }
        mChipsView.setChipsListener(new ChipsView.ChipsListener() {
            @Override
            public void onChipAdded(ChipsView.Chip chip) {
                for (ChipsView.Chip chipItem : mChipsView.getChips()) {
                    Log.d("ChipList", "chip: " + chipItem.toString());
                    mContacts.setVisibility(View.GONE); //check
                    bfd  = chipItem.getContact().getDisplayName();

                    arrayList2.add(chipItem.getContact().getDisplayName().toString());

                    showProgress(false);




                }

                ArrayList<String> values=new ArrayList<String>();
                HashSet<String> hashSet = new HashSet<String>(arrayList2);
                arrayList2.clear();
                //arrayList2.clear();
                arrayList2.addAll(hashSet);


            }

            @Override
            public void onChipDeleted(ChipsView.Chip chip) {
                mContacts.setVisibility(View.GONE);
                showProgress(false);

            }

            @Override
            public void onTextChanged(CharSequence text) {
                mContacts.setVisibility(View.VISIBLE);
                mAdapter.filterItems(text);
                showProgress(false);
            }

        });





        if(!checkPermissionForReadExtertalStorage()){
            try {
                requestPermissionForReadExtertalStorage();
            } catch (Exception e) {
                e.printStackTrace();

            }
        }





    }

    public void price(View view) {
        if(want_price.isChecked()){
            et_prod_price.setVisibility(View.VISIBLE);
            price_tag.setVisibility(View.VISIBLE);

        }
        else{
            et_prod_price.setVisibility(View.GONE);
            price_tag.setVisibility(View.GONE);

        }
    }

    public void cert(View view) {

        if(certified.isChecked()){

            tv_cerified.setVisibility(View.VISIBLE);
        }
        else{

            tv_cerified.setVisibility(View.GONE);
        }
    }

    public void delete(View view) {
        inten();
        if(getIntent().getStringExtra("click")==null && (getIntent().getStringExtra("pid"))==null){
            Toast.makeText(getApplicationContext(),"Product is not Saved yet",Toast.LENGTH_SHORT).show();

        }
        else {
            btnedit.setVisibility(View.VISIBLE);
            btn_submit.setVisibility(View.GONE);
            String pid = getIntent().getStringExtra("pid");




            //edit_del_prod_class.setQuantity(Integer.parseInt(quantity.getText().toString()));
            //

            // ;




            Shared_pref shared_pref = new Shared_pref(getApplicationContext());
            final String token = shared_pref.get_token();

            OkHttpClient client = new OkHttpClient.Builder().addInterceptor(new Interceptor() {
                @Override
                public okhttp3.Response intercept(Chain chain) throws IOException {
                    Request newRequest = chain.request().newBuilder()
                            .addHeader("Authorization", " Bearer " + token)
                            .build();
                    return chain.proceed(newRequest);
                }
            }).build();
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl("https://covid-19-info-123.herokuapp.com/")
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            api_header api = retrofit.create(api_header.class);

           /* body_edit body_edit = new body_edit();
            body_edit.setPid(pid);
            body_edit.setProduct(edit_del_prod_class);*/

            Call<ResponseBody> call = api.delete_prod(pid);
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if(response.code()==200) {
                        Toast.makeText(getApplicationContext(), "Product Deleted", Toast.LENGTH_SHORT).show();

                        startActivity(new Intent(getApplicationContext(),com.example.project.profile.class));
                    }
                    else{
                        Toast.makeText(getApplicationContext(),"error code : " + response.code(),Toast.LENGTH_SHORT).show();

                    }

                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    Toast.makeText(getApplicationContext(),"Error : " + t.getMessage(),Toast.LENGTH_SHORT).show();

                }
            });
        }




    }

    public void edit_product(View view) {
        inten();
        if(getIntent().getStringExtra("click")==null && (getIntent().getStringExtra("pid"))==null){
            Toast.makeText(getApplicationContext(),"Product is not Saved yet",Toast.LENGTH_SHORT).show();

        }
        else {
            btnedit.setVisibility(View.VISIBLE);
            btn_submit.setVisibility(View.GONE);
            String pid = getIntent().getStringExtra("pid");


            edit_del_prod_class edit_del_prod_class = new edit_del_prod_class();

            edit_del_prod_class.setProductName(et_product_name.getText().toString());
            edit_del_prod_class.setProductDesc(et_prod_desc.getText().toString());
            edit_del_prod_class.setProductType(et_prod_type.getText().toString());
            if(Integer.parseInt(quantity.getText().toString())!=0){
            edit_del_prod_class.setQuantity(Integer.parseInt(quantity.getText().toString()));}
            else{ edit_del_prod_class.setQuantity(1000000000);}

            if(et_prod_price.getText().toString().isEmpty()){
                edit_del_prod_class.setProductPrice(0);
            }
            else{
                edit_del_prod_class.setProductPrice(Integer.parseInt(et_prod_price.getText().toString()));
            }

            if (checkBox.isChecked()) {
                edit_del_prod_class.setAvail(true);
            } else {
                edit_del_prod_class.setAvail(false);

            }
            if (certified.isChecked()) {
                edit_del_prod_class.setCertified(true);
            } else {
                edit_del_prod_class.setCertified(false);

            }
            edit_del_prod_class.setProductTags(arrayList2);
            //edit_del_prod_class.setQuantity(Integer.parseInt(quantity.getText().toString()));
            //
            edit_del_prod_class.setImageThumbnail(thumbnail);


            if (arrayList.isEmpty()) {


                Toast.makeText(this, "You Have to Upload at least 1 picture and 1 thumbnail", Toast.LENGTH_LONG).show();

            } else {
                edit_del_prod_class.setMedia(arrayList);
            }
            // edit_del_prod_class.setProductTags(arrayList2);


            Shared_pref shared_pref = new Shared_pref(getApplicationContext());
            final String token = shared_pref.get_token();
            OkHttpClient client = new OkHttpClient.Builder().addInterceptor(new Interceptor() {
                @Override
                public okhttp3.Response intercept(Chain chain) throws IOException {
                    Request newRequest = chain.request().newBuilder()
                            .addHeader("Authorization", " Bearer " + token)
                            .build();
                    return chain.proceed(newRequest);
                }
            }).build();
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl("https://covid-19-info-123.herokuapp.com/")
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            api_header api = retrofit.create(api_header.class);
            body_edit body_edit = new body_edit();
            body_edit.setPid(pid);
            body_edit.setProduct(edit_del_prod_class);

            Call<ResponseBody> call = api.edit_prod(body_edit);
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if(response.code()==200) {
                        Toast.makeText(getApplicationContext(), "Changes Saved", Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(getApplicationContext(),com.example.project.profile.class));
                    }
                    else{
                        Toast.makeText(getApplicationContext(),"error code : " + response.code(),Toast.LENGTH_SHORT).show();
                    }

                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    Toast.makeText(getApplicationContext(),"Error : " + t.getMessage(),Toast.LENGTH_SHORT).show();

                }
            });
        }



    }

    public void rb_button(View view) {

        int radioid = rg1.getCheckedRadioButtonId();



        if(radioid != -1){
            rb = findViewById(radioid);
            et_prod_type.setText(rb.getText());
        }



    }

    public class ContactsAdapter extends RecyclerView.Adapter<CheckableContactViewHolder> {

        private String[] data = new String[]{
                "PPE Kits",
                "Covid19",
                "Work From Home",
                "Stay Home",
                "Be Safe",
                "Healthcare",
                "Social Distancing"



        };

        private List<String> filteredList = new ArrayList<>();

        public ContactsAdapter() {
            Collections.addAll(filteredList, data);
        }

        @Override
        public CheckableContactViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(product.this).inflate(R.layout.item_checkable_contact, parent, false);
            return new CheckableContactViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(CheckableContactViewHolder holder, int position) {
            holder.name.setText(filteredList.get(position));
        }

        @Override
        public int getItemCount() {
            return filteredList.size();
        }

        public void filterItems(CharSequence text) {
            filteredList.clear();
            if (TextUtils.isEmpty(text)) {
                Collections.addAll(filteredList, data);
            } else {
                for (String s : data) {
                    if (s.contains(text)) {
                        filteredList.add(s);
                    }
                }
            }
            notifyDataSetChanged();
        }

        @Override
        public int getItemViewType(int position) {
            return Math.abs(filteredList.get(position).hashCode());
        }
    }

    public class CheckableContactViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public final TextView name;
        public final CheckBox selection;

        public CheckableContactViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.tv_contact_name);
            selection = (CheckBox) itemView.findViewById(R.id.cb_contact_selection);
            selection.setOnClickListener(this);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    selection.performClick();
                    mContacts.setVisibility(View.GONE);
                }
            });
        }

        @Override
        public void onClick(View v) {
            String email = name.getText().toString();

            Contact contact = new Contact(null, null, email, email, null);

            if (selection.isChecked()) {
                mChipsView.addChip(email,null,contact,false);
                // mChipsView.addChip(email, imgUrl, contact);
            } else {
                mChipsView.removeChipBy(contact);
            }
        }
    }
    public boolean checkPermissionForReadExtertalStorage() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int result = getApplicationContext().checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE);
            return result == PackageManager.PERMISSION_GRANTED;
        }
        return false;
    }
    public void requestPermissionForReadExtertalStorage() throws Exception {
        try {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    9);
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


            if (requestCode == 1 && resultCode == RESULT_OK && data != null) {
                Uri selectedImage = data.getData();
                img1.setImageURI(selectedImage);

                get_thumbnail(selectedImage);


            } else if (requestCode == 2 && resultCode == RESULT_OK && data != null) {
                Uri selectedImage = data.getData();
                img2.setImageURI(selectedImage);
                get(selectedImage);


            } else if (requestCode == 3 && resultCode == RESULT_OK && data != null) {
                Uri selectedImage = data.getData();
                img3.setImageURI(selectedImage);
                get(selectedImage);


            } else if (requestCode == 4 && resultCode == RESULT_OK && data != null) {
                Uri selectedImage = data.getData();
                img4.setImageURI(selectedImage);
                get(selectedImage);


            } else if (requestCode == 5 && resultCode == RESULT_OK && data != null) {
                Uri selectedImage = data.getData();
                img5.setImageURI(selectedImage);
                get(selectedImage);


            }
            else if (requestCode == 6 && resultCode == RESULT_OK && data != null) {
                Uri selectedImage = data.getData();
                img6.setImageURI(selectedImage);
                get_video(selectedImage);


            }
        }



    public void img1(View view) {
         Intent galleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(galleryIntent,1);
        img2.setVisibility(View.VISIBLE);
    }

    public void img5(View view) {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(galleryIntent,5);

    }

    public void img4(View view) {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(galleryIntent,4);
        img5.setVisibility(View.VISIBLE);

    }

    public void img3(View view) {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(galleryIntent,3);
        img4.setVisibility(View.VISIBLE);
    }

    public void img2(View view) {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(galleryIntent,2);
        img3.setVisibility(View.VISIBLE);
    }

    private void get(Uri filepath){
        try {

         //   MediaManager.init(this);
            String requestId = MediaManager.get().upload(filepath)


                    .callback(new UploadCallback() {
                        @Override
                        public void onStart(String requestId) {

                            Toast.makeText(product.this,"uploading",Toast.LENGTH_SHORT).show();
                            showProgress(true);

                        }

                        @Override
                        public void onProgress(String requestId, long bytes, long totalBytes) {
                            Double progress = (double) bytes/totalBytes;
                            // post progress to app UI (e.g. progress bar, notification)

                            showProgress(true);


                        }
                        @Override
                        public void onSuccess(String requestId, Map resultData) {
                            showProgress(false);

                            Toast.makeText(product.this,"uploaded",Toast.LENGTH_SHORT).show();
                            arrayList.add(Objects.requireNonNull(resultData.get("secure_url")).toString());

                           // arrayList = resultData.get("secure_url");






                        }

                        @Override
                        public void onError(String requestId, ErrorInfo error) {

                            showProgress(false);
                            Toast.makeText(product.this,"error : " + error,Toast.LENGTH_SHORT).show();

                        }

                        @Override
                        public void onReschedule(String requestId, ErrorInfo error) {

                        }

                    })
                    .dispatch();


            //MediaManager.init(null);
        }
        catch (Exception e){
           Toast.makeText(getApplicationContext(),e.getMessage(),Toast.LENGTH_SHORT).show();

        }

    }
    private void get_thumbnail(Uri filepath){
        try {

          //  MediaManager.init(this);
            String requestId = MediaManager.get().upload(filepath)

                    .callback(new UploadCallback() {
                        @Override
                        public void onStart(String requestId) {

                            Toast.makeText(product.this,"uploading",Toast.LENGTH_SHORT).show();
                            showProgress(true);

                        }

                        @Override
                        public void onProgress(String requestId, long bytes, long totalBytes) {
                            Double progress = (double) bytes/totalBytes;
                            showProgress(true);
                            // post progress to app UI (e.g. progress bar, notification)


                        }
                        @Override
                        public void onSuccess(String requestId, Map resultData) {
                            showProgress(false);


                            Toast.makeText(product.this,"uploaded",Toast.LENGTH_SHORT).show();
                            thumbnail = Objects.requireNonNull(resultData.get("secure_url")).toString();







                        }

                        @Override
                        public void onError(String requestId, ErrorInfo error) {

                            showProgress(false);
                            Toast.makeText(product.this,"error : " + error,Toast.LENGTH_SHORT).show();

                        }

                        @Override
                        public void onReschedule(String requestId, ErrorInfo error) {

                        }

                    })
                    .dispatch();


            //MediaManager.init(null);
        }
        catch (Exception e){

        }

    }
    private void get_video(Uri filepath){
        try {

        //   MediaManager.init(this);

            String requestId = MediaManager.get().upload(filepath)
                    .option("resource_type", "video")


                    .callback(new UploadCallback() {
                        @Override
                        public void onStart(String requestId) {

                            Toast.makeText(product.this,"uploading",Toast.LENGTH_SHORT).show();
                            showProgress(true);

                        }

                        @Override
                        public void onProgress(String requestId, long bytes, long totalBytes) {
                            Double progress = (double) bytes/totalBytes;
                            showProgress(true);
                            // post progress to app UI (e.g. progress bar, notification)



                        }
                        @Override
                        public void onSuccess(String requestId, Map resultData) {
                            showProgress(false);


                            Toast.makeText(product.this,"uploaded",Toast.LENGTH_SHORT).show();
                            arrayList.add(Objects.requireNonNull(resultData.get("secure_url")).toString());






                        }

                        @Override
                        public void onError(String requestId, ErrorInfo error) {
                            showProgress(false);
                            Toast.makeText(product.this,"error : " + error,Toast.LENGTH_SHORT).show();

                        }

                        @Override
                        public void onReschedule(String requestId, ErrorInfo error) {

                        }

                    })
                    .dispatch();


          //  MediaManager.init(null);
        }
        catch (Exception e){
            Toast.makeText(getApplicationContext(),e.getMessage(),Toast.LENGTH_SHORT).show();
        }

    }

    public void img6(View view) {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Video.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(galleryIntent,6);
    }

    public void product_submit_ok(View view) {



        final Shared_pref shared_pref = new Shared_pref(this);
        if(et_prod_type.getText().toString().isEmpty() || et_product_name.getText().toString().isEmpty() ||
        et_prod_desc.getText().toString().isEmpty()){
            Toast.makeText(this,"all fields mandatory",Toast.LENGTH_LONG).show();

        }
        else{

            final String token = shared_pref.get_token();
            final add_product_class product_class = new add_product_class();
       /* ArrayList<String> stringArrayList = new ArrayList<>();
        stringArrayList.add("heelo");
        stringArrayList.add("prop");*/
            product_class.setName(et_product_name.getText().toString());
            if(checkBox.isChecked()){
                product_class.setAvail(true);

            }
            else{
                product_class.setAvail(false);
            }
            if(certified.isChecked()){
                product_class.setCertified(true);
                //tv_cerified.setVisibility(View.VISIBLE);
            }
            else{
                product_class.setCertified(false);
                //tv_cerified.setVisibility(View.GONE);
            }

            product_class.setDesc(et_prod_desc.getText().toString());



            if(want_price.isChecked()){
                et_prod_price.setVisibility(View.VISIBLE);
                price_tag.setVisibility(View.VISIBLE);
                product_class.setPrice(Integer.parseInt(et_prod_price.getText().toString()));


            }
            else{
                et_prod_price.setVisibility(View.GONE);
                price_tag.setVisibility(View.GONE);
                product_class.setPrice(0);

            }

            product_class.setTags(arrayList2);
            if(Integer.parseInt(quantity.getText().toString())==0 || quantity.getText().toString().isEmpty()){
                product_class.setQuantity(1000000000);
      }
            else{
                product_class.setQuantity(Integer.parseInt(quantity.getText().toString()));

            }

            product_class.setThumbnail(thumbnail);


            if(arrayList.isEmpty()){


                Toast.makeText(this,"You Have to Upload at least 1 picture and 1 thumbnail",Toast.LENGTH_LONG).show();

            }
            else{
                product_class.setMedia(arrayList);
            }

            if(product_class.getPrice() == null){
                product_class.setPrice(0);

            }
            product_class.setType(et_prod_type.getText().toString());
            OkHttpClient client = new OkHttpClient.Builder().addInterceptor(new Interceptor() {
                @Override
                public okhttp3.Response intercept(Chain chain) throws IOException {
                    Request newRequest  = chain.request().newBuilder()
                            .addHeader("Authorization", " Bearer " + token)
                            .build();
                    return chain.proceed(newRequest);
                }
            }).build();
            Retrofit retrofit  = new Retrofit.Builder()
                    .baseUrl("https://covid-19-info-123.herokuapp.com/")
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            api_header api = retrofit.create(api_header.class);

            Call<ResponseBody> call = api.add_product_jwt(product_class);
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if(response.code() == 200) {
                        // //response.body().getMessage());
                        assert response.body() != null;
                        // //response.body().isSuccess());

                        Toast.makeText(product.this,"Product Added",Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(getApplicationContext(),com.example.project.ui.send.products_show.class));
                    }
                    else{

                        assert response.errorBody() != null;
                        Toast.makeText(getApplicationContext(),response.errorBody()+"",Toast.LENGTH_SHORT).show();

                        assert response.body() != null;

                        // //response.body().getMessage());
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {

                }
            });

        }

    }

    public void back_to_product_show(View view) {
        startActivity(new Intent(this,com.example.project.profile.class));
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mView.setVisibility(show ? View.GONE : View.VISIBLE);
            mView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });

        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            // tvLoad.setVisibility(show ? View.VISIBLE : View.GONE);
            mView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }
}
