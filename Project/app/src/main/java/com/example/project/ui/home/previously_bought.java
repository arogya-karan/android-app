package com.example.project.ui.home;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.project.R;
import com.example.project.Shared_pref;
import com.example.project.api_header;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class previously_bought extends AppCompatActivity  implements previously_bought_adapter.OnNoteList{
    RecyclerView rv;
    previously_bought_adapter adapter;
    TextView tv_empty;
    ArrayList<active_req_class> arrayList_req;
    private View mProgressView;
    private View mView;
    ArrayList<act_list_class> arrayList_notif;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_previously_bought);
        tv_empty = findViewById(R.id.tv_empty);
        mProgressView = findViewById(R.id.pb_progress);
        mView = findViewById(R.id.pb_rl);
        rv = findViewById(R.id.rv_orders);
        arrayList_req = new ArrayList<active_req_class>();
        showProgress(true);

        getJ();
    }

    private void getJ(){

        Shared_pref shared_pref = new Shared_pref(getApplicationContext());
        final String token = shared_pref.get_token();
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request newRequest = chain.request().newBuilder()
                        .addHeader("Authorization", " Bearer " + token)
                        .build();
                return chain.proceed(newRequest);
            }
        }).build();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://covid-19-info-123.herokuapp.com/")
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        final api_header api = retrofit.create(api_header.class);

        Call<ResponseBody> call = api.get_accepted();
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                showProgress(false);

                String res = null;
                try {
                    if (response.body() == null) {
                        tv_empty.setVisibility(View.VISIBLE);

                    } else {
                        tv_empty.setVisibility(View.GONE);
                        res = response.body().string();


                        JSONObject object = new JSONObject(res);
                        final JSONArray array = object.getJSONArray("transactions");


                        arrayList_notif = new ArrayList<act_list_class>();

                        for (int i = 0; i < array.length(); i++) {
                            act_list_class act_list_class = new act_list_class();
                            JSONObject dataobj = array.getJSONObject(i);

                            act_list_class.set_id(dataobj.getString("_id"));
                            act_list_class.setDateofRequest(dataobj.getString("date"));
                            act_list_class.setPid(dataobj.getString("pid"));
                            act_list_class.setQuantity(dataobj.getString("quantity"));
                            act_list_class.setSid(dataobj.getString("sid"));
                            act_list_class.setFinalised(dataobj.getBoolean("finalized"));

                            arrayList_notif.add(act_list_class);


                        }
                        for (int j = 0; j < arrayList_notif.size(); j++) {
                            String pid = arrayList_notif.get(j).getPid();
                            final String quant = arrayList_notif.get(j).getQuantity();
                            final String date = arrayList_notif.get(j).getDateofRequest();
                            final active_req_class active_req_class = new active_req_class();

                            final String sid = arrayList_notif.get(j).getSid();

                            /////////////////////////////////////////////////
                            Call<ResponseBody> call1 = api.get_seller_from_id(sid);
                            call1.enqueue(new Callback<ResponseBody>() {
                                @Override
                                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                    String hello = null;
                                    try {
                                        assert response.body() != null;
                                        hello = response.body().string();

                                        //"hello " + hello);
                                        JSONObject object = new JSONObject(hello).getJSONObject("seller");
                                        active_req_class.setSeller_name(object.getString("sellerName"));

                                    } catch (JSONException | IOException e) {
                                        e.printStackTrace();
                                    }
                                }

                                @Override
                                public void onFailure(Call<ResponseBody> call, Throwable t) {

                                }
                            });

                            ////////////////////////////////////////////////

                            Call<ResponseBody> call2 = api.get_products_by_product(pid);
                            call2.enqueue(new Callback<ResponseBody>() {
                                @Override
                                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                    String hello = null;
                                    showProgress(false);
                                    try {
                                        assert response.body() != null;
                                        hello = response.body().string();

                                        //"hello " + hello);
                                        JSONObject object = new JSONObject(hello);

                                        JSONObject obj = object.getJSONObject("product");
                                        active_req_class.setProductName(obj.getString("productName"));
                                        active_req_class.setProductPrice(obj.getString("productPrice"));
                                        active_req_class.setProductTypes(obj.getString("productType"));

                                        active_req_class.setImgThumbnail(obj.getString("imageThumbnail"));
                                        active_req_class.setSeller_quantity(quant);
                                        active_req_class.setDate(getDate(Long.parseLong(date)));
                                        // product_class.setBuyer_name(uid);
                                        arrayList_req.add(active_req_class);


                                    } catch (IOException | JSONException e) {
                                        e.printStackTrace();
                                        //e.getMessage());
                                    }

                                    //"arraylist : " + arrayList_req);

                                    adapter = new previously_bought_adapter(getApplicationContext(), arrayList_req, previously_bought.this);
                                    rv.setAdapter(adapter);
                                    rv.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));


                                }

                                @Override
                                public void onFailure(Call<ResponseBody> call, Throwable t) {
                                    //"error " + t.getMessage());

                                }
                            });
                        }

                    }
                }catch(IOException | JSONException e){
                    e.printStackTrace();
                }

            }


            @Override
            public void onFailure (Call < ResponseBody > call, Throwable t){

            }

        });

    }
    private String getDate(long timeStamp){

        try{
            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
            Date netDate = (new Date(timeStamp));
            return sdf.format(netDate);
        }
        catch(Exception ex){
            return "xx";
        }
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mView.setVisibility(show ? View.GONE : View.VISIBLE);
            mView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });

        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            // tvLoad.setVisibility(show ? View.VISIBLE : View.GONE);
            mView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }





    @Override
    public void OnnoteClick(int position) {

        if(arrayList_notif.get(position).getFinalised()) {
            Intent intent = new Intent(previously_bought.this, previously_ordered_details.class);
            intent.putExtra("seller_name", arrayList_req.get(position).getSeller_name());
            // //"buyer : " + arrayList.get(position).getBuyer_name());
            intent.putExtra("seller_quantity", arrayList_req.get(position).getSeller_quantity());
            intent.putExtra("date", arrayList_req.get(position).getDate());
            intent.putExtra("product_name", arrayList_req.get(position).getProductName());
            intent.putExtra("product_type", arrayList_req.get(position).getProductTypes());
            intent.putExtra("product_price", arrayList_req.get(position).getProductPrice());


            intent.putExtra("img", arrayList_req.get(position).getImgThumbnail());

            intent.putExtra("date_request", arrayList_notif.get(position).getDateofRequest());
            intent.putExtra("quantity", arrayList_notif.get(position).getQuantity());
            intent.putExtra("pid", arrayList_notif.get(position).getPid());
            intent.putExtra("sid", arrayList_notif.get(position).getSid());


            startActivity(intent);
        }
        else{
            Toast.makeText(previously_bought.this,"Request Rejected",Toast.LENGTH_SHORT).show();
        }







    }






    public void back_to_product_show(View view) {
        startActivity(new Intent(previously_bought.this,com.example.project.profile.class));
    }
}
