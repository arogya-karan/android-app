package com.example.project;

import com.google.gson.annotations.SerializedName;

public class add_seller_class {

    @SerializedName("name")
    private String name;
    @SerializedName("phone")
    private Number phone;
    @SerializedName("email")
    private String email;
    @SerializedName("address")
    private String address;
    @SerializedName("long")
    private String longi;
    @SerializedName("lat")
    private String lat;
    @SerializedName("isBusiness")
    private Boolean isBusiness;
    @SerializedName("isNewAddress")
    private Boolean isNewAddress;

    public void setName(String name) {
        this.name = name;
    }

    public void setPhone(Number phone) {
        this.phone = phone;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setLongi(String longi) {
        this.longi = longi;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public void setBusiness(Boolean business) {
        isBusiness = business;
    }

    public void setNewAddress(Boolean newAddress) {
        isNewAddress = newAddress;
    }

    public String getName() {
        return name;
    }

    public Number getPhone() {
        return phone;
    }

    public String getEmail() {
        return email;
    }

    public String getAddress() {
        return address;
    }

    public String getLongi() {
        return longi;
    }

    public String getLat() {
        return lat;
    }

    public Boolean getBusiness() {
        return isBusiness;
    }

    public Boolean getNewAddress() {
        return isNewAddress;
    }
}
