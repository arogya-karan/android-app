package com.example.project;

import android.app.Application;

import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;
import androidx.lifecycle.ProcessLifecycleOwner;

import com.cloudinary.android.MediaManager;

public class ApplicationClass extends Application implements LifecycleObserver {


   // MediaManager mediaManager;

   /* public ApplicationClass() {
       // this.mediaManager = mediaManager;




    }
*/
    @Override
    public void onCreate() {
        super.onCreate();

        MediaManager.init(this);
        ProcessLifecycleOwner.get().getLifecycle().addObserver(this);
    }
    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    public void onAppBackgrounded() {

        //App in background
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    public void onAppForegrounded() {
        // App in foreground
    }

}
