package com.example.project.ui.home;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.project.JWUtils;
import com.example.project.R;
import com.example.project.Shared_pref;
import com.example.project.api_header;
import com.example.project.response_here;
import com.pusher.client.Pusher;
import com.pusher.client.PusherOptions;
import com.pusher.client.channel.Channel;
import com.pusher.client.channel.ChannelEventListener;
import com.pusher.client.channel.PusherEvent;
import com.pusher.client.connection.ConnectionEventListener;
import com.pusher.client.connection.ConnectionState;
import com.pusher.client.connection.ConnectionStateChange;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class request extends AppCompatActivity {

    TextView tv_product_name, tv_product_price, tv_product_type, _tv_buyer_name, tv_buyer_quantity, tv_buyer_date ,etemail;
    ImageView iv;
    String buyer_name,buyer_quantity,date,product_name,product_type,product_price,img,uid,pid,quantity,date_request , buyer_email;





    // String buyer_name = getIntent().getStringExtra("buyer_name");


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request);



        tv_buyer_date = findViewById(R.id.tv_date_2);
        tv_product_name = findViewById(R.id.tv_name);
        tv_product_price = findViewById(R.id.tv_price_show);
        tv_product_type = findViewById(R.id.tv_type);
        tv_buyer_quantity = findViewById(R.id.quant_required);
        _tv_buyer_name = findViewById(R.id.buyer_name);
        etemail = findViewById(R.id.etemail);
        iv = findViewById(R.id.iv_product_seller);
        if(getIntent().getStringExtra("buyer_name")!=null) {
             buyer_name = getIntent().getStringExtra("buyer_name");
             buyer_quantity = getIntent().getStringExtra("buyer_quantity");
             date = getIntent().getStringExtra("date");
             buyer_email = getIntent().getStringExtra("buyer_email");
             product_name = getIntent().getStringExtra("product_name");
             product_type = getIntent().getStringExtra("product_type");
             product_price = getIntent().getStringExtra("product_price");
             img = getIntent().getStringExtra("img");

             uid = getIntent().getStringExtra("uid");
             pid = getIntent().getStringExtra("pid");
             date_request = getIntent().getStringExtra("date_request");
             quantity = getIntent().getStringExtra("quantity");

            tv_buyer_quantity.setText(buyer_quantity);
            tv_buyer_date.setText(getDate(Long.parseLong(date_request)));
            tv_product_type.setText(product_type);
            tv_product_name.setText(product_name);
            tv_product_price.setText(product_price);
            _tv_buyer_name.setText(buyer_name);
            etemail.setText(buyer_email);
           // iv.setImageURI(Uri.parse(img));
            Picasso.with(getApplicationContext()).load(img).into(iv);





        }


    }
    private String getDate(long timeStamp){

        try{
            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
            Date netDate = (new Date(timeStamp));
            return sdf.format(netDate);
        }
        catch(Exception ex){
            return "xx";
        }
    }

    public void btn_accept(View view) {
        push();

        transac_class transac_class = new transac_class();
        transac_class.setDateofRequest(date_request);
        transac_class.setFinalised(true);
        transac_class.setPid(pid);
        transac_class.setUid(uid);
        transac_class.setQuantity(quantity);

        final Shared_pref shared_pref = new Shared_pref(getApplicationContext());
        final String token = shared_pref.get_token();
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request newRequest = chain.request().newBuilder()
                        .addHeader("Authorization", " Bearer " + token)
                        .build();
                return chain.proceed(newRequest);
            }
        }).build();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://covid-19-info-123.herokuapp.com/")
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        api_header api = retrofit.create(api_header.class);

        Call<ResponseBody> call = api.accept_transaction(transac_class);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                try {
                    assert response.body() != null;
                    Toast.makeText(getApplicationContext(),response.body().string(),Toast.LENGTH_SHORT).show();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                push();

                send_email();
                Intent intent = new Intent(request.this,request_details.class);
                intent.putExtra("uid",uid);
                intent.putExtra("img",img);
                intent.putExtra("date",date_request);
                intent.putExtra("quantity",quantity);
                intent.putExtra("product_name",product_name);
                intent.putExtra("product_type",product_type);
                intent.putExtra("product_price",product_price);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);

                startActivity(intent);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });

    }

    private void send_email() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://covid-19-info-123.herokuapp.com/")
                // .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        api_header api = retrofit.create(api_header.class);
        mail mail = new mail();

        Shared_pref shared_pref = new Shared_pref(getApplicationContext());
        try {
            String token = JWUtils.decodeJWT(shared_pref.get_token());
            JSONObject jsonObject = new JSONObject(token).getJSONObject("user");
           /* name = jsonObject.getString("name");
            email = jsonObject.getString("email");*/

        } catch (Exception e) {
            e.printStackTrace();


        }


        mail.setEmail(etemail.getText().toString());
        mail.setSubject("Arogya Karan : Request For Product");
        String content="<h1>Request Received</h2>\n" +

                "        <p>Product Name :  <b>"+ tv_product_name.getText().toString()  +"</b>.</p>\n" +
                "        <p>Product Type :  <b>"+ tv_product_type.getText().toString()  +"</b>.</p>\n" +
                "        <p>Accepted By :  <b>"+  buyer_name  +"</b>.</p>\n" +
                "        <p>Accepted By :  <b>"+  etemail.getText().toString()  +"</b>.</p>\n" +
                "        <p>Quantity Requested :  <b>"+  buyer_quantity  +"</b>.</p>\n";


        mail.setMsg(content);
        mail.setPasscode("0b0YghA8vfVbQhv1q6EB5pRlU3zxUsKCtbOUXNN2JFdNZHttzGEa7q6TsxYAXwk");

        Call<ResponseBody> call = api.send_email(mail);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(response.code() == 200){
                    Toast.makeText(getApplicationContext(),"Mail Sent",Toast.LENGTH_SHORT).show();
                }
                else{
                    Toast.makeText(getApplicationContext(),"error : " + response.message(),Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });



    }


    public void push() {

        String cluster = getString(R.string.pusher_cluster);
        String key = getString(R.string.pusher_key);
        String channel_name = getString(R.string.channel_name);
        String event_name2 = getString(R.string.event_name_2);

        PusherOptions options = new PusherOptions().setCluster(cluster);

        Pusher pusher = new Pusher(key, options);
        pusher.connect(new ConnectionEventListener() {
            @Override
            public void onConnectionStateChange(ConnectionStateChange change) {
               /* //"State changed to " + change.getCurrentState() +
                        " from " + change.getPreviousState());*/


            }

            @Override
            public void onError(String message, String code, Exception e) {
                //"There was a problem connecting! : " + message);
            }
        }, ConnectionState.ALL);

        final Channel channel = pusher.subscribe(channel_name, new ChannelEventListener() {         //sos

            @Override
            public void onSubscriptionSucceeded(String channelName) {
                //"Subscribed!");
                //channelName);




            }

            @Override
            public void onEvent(PusherEvent event) { //notification

                //event.getData());
                try {
                    Shared_pref shared_pref = new Shared_pref(request.this);

                    JSONObject obj = new JSONObject(event.getData());
                    String sid = obj.getString("sid");

                    String user =  JWUtils.decodeJWT(shared_pref.get_token());
                    JSONObject obj2 = new JSONObject(user);
                    //obj2);
                    JSONObject object = obj2.getJSONObject("user");
                    String use = object.getString("_id");

                    //"_id" +use);


                    if(use.equals(sid)){

                        NotificationManagerCompat notificationManagerCompat;
                        notificationManagerCompat = NotificationManagerCompat.from(request.this);
                        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                            NotificationChannel channel1 = new NotificationChannel(
                                    "channel1","channel1", NotificationManager.IMPORTANCE_DEFAULT
                            );

                            channel1.setDescription("Request received!");
                            NotificationManager manager = getSystemService(NotificationManager.class);
                            assert manager != null;
                            manager.createNotificationChannel(channel1);

                        }

                        Context context = getApplicationContext();
                        Date currentTime = Calendar.getInstance().getTime();
                        Intent myIntent = new Intent(context, com.example.project.ui.home.notification_list.class);


                        myIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        PendingIntent intent = PendingIntent.getActivity(context,1,myIntent,PendingIntent.FLAG_UPDATE_CURRENT);

                        Notification notification = new NotificationCompat.Builder(request.this,"channel1").setContentTitle("Request Accepted").setContentText("Tap to checkout")

                                .setPriority(4).setCategory(NotificationCompat.CATEGORY_MESSAGE).setDefaults(Notification.DEFAULT_SOUND).setAutoCancel(true)
                                .setContentIntent(intent)
                                .setSmallIcon(R.drawable.ic_menu_send)
                                .build();

                        notificationManagerCompat.notify(1,notification);


                    }



                } catch (Exception e) {
                    e.printStackTrace();
                    //e.getMessage());
                }

            }



        },event_name2);

    }



    public void back(View view) {
        Intent intent = new Intent(request.this,notification_list.class);

        /*intent.putExtra("uid",uid);

        intent.putExtra("pid",pid);
        intent.putExtra("quantity", quantity);
        intent.putExtra("date",date);


*/
        startActivity(intent);

    }

    public void btn_reject(View view) {

        transac_class transac_class = new transac_class();
        transac_class.setDateofRequest(date_request);
        transac_class.setFinalised(false);
        transac_class.setPid(pid);
        transac_class.setUid(uid);
        transac_class.setQuantity(quantity);

        final Shared_pref shared_pref = new Shared_pref(getApplicationContext());
        final String token = shared_pref.get_token();
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request newRequest = chain.request().newBuilder()
                        .addHeader("Authorization", " Bearer " + token)
                        .build();
                return chain.proceed(newRequest);
            }
        }).build();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://covid-19-info-123.herokuapp.com/")
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        api_header api = retrofit.create(api_header.class);

        Call<ResponseBody> call = api.accept_transaction(transac_class);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                push();

                startActivity(new Intent(getApplicationContext(),com.example.project.profile.class));
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });

    }
}
