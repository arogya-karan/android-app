package com.example.project.ui.slideshow;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.project.R;
import com.example.project.cases;
import com.example.project.recycler_adapter;
import com.example.project.retrofit_client;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class view_list extends AppCompatActivity implements recycler_adapter.OnNoteList {
    RecyclerView recyclerView;
    Button btn_go;
    recycler_adapter adapter;
    TextView tv;
    EditText et_search;

    ArrayList<cases> modelRecyclerArrayList;
    private View mProgressView;
    private View mView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_list);

        mView = findViewById(R.id.relative_view_list);
        mProgressView = findViewById(R.id.view_list_progress);
        recyclerView = findViewById(R.id.recycler_view);

        et_search = findViewById(R.id.et_search);

        showProgress(true);
        btn_go = findViewById(R.id.btn_go);
        search();
        Json();




    }

    private void Json(){

        Call<ResponseBody> call = retrofit_client.getInstance().getapi().summary();
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                assert response.body() != null;
                showProgress(false);

                //Toast.makeText()
                if (response.code()==200) {
                    if (response.body() != null) {

                        String jsonresponse = null;
                        try {
                            jsonresponse = response.body().string();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        writeRecycler(jsonresponse);

                    } else {
                        Log.i("onEmptyResponse", "Returned empty response");//Toast.makeText(getContext(),"Nothing returned",Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
               Toast.makeText(getApplicationContext(),"fail : " + t.getMessage(),Toast.LENGTH_SHORT).show();
                showProgress(false);

            }
        });
    }

    private void writeRecycler(String response){

        try {
            showProgress(false);
            //getting the whole json object from the response
            JSONObject obj = new JSONObject(response);


            modelRecyclerArrayList = new ArrayList<>();
            JSONArray dataArray  = obj.getJSONArray("Countries");

            for (int i = 0; i < dataArray.length(); i++) {

                cases modelRecycler = new cases();
                JSONObject dataobj = dataArray.getJSONObject(i);

                //modelRecycler.setImgURL(dataobj.getString("imgURL"));
                modelRecycler.setCountry_name(dataobj.getString("Country"));
                modelRecycler.setCases(dataobj.getString("TotalConfirmed"));
                modelRecycler.setDeaths(dataobj.getString("TotalDeaths"));
                modelRecycler.setRecovered(dataobj.getString("TotalRecovered"));

                modelRecyclerArrayList.add(modelRecycler);

            }

            adapter = new recycler_adapter(this,modelRecyclerArrayList,this);
            recyclerView.setAdapter(adapter);
            recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));



        } catch (JSONException e) {
            e.printStackTrace();
            Toast.makeText(getApplicationContext(),"fail : " + e.getMessage(),Toast.LENGTH_SHORT).show();

        }

    }

    @Override
    public void OnnoteClick(int position) {

        Uri uri = Uri.parse("https://www.cdc.gov/"); // missing 'http://' will cause crashed
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(intent);



    }


    public void search() {

        et_search.setVisibility(View.VISIBLE);

        et_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                filter(s.toString());

            }
        });


    }
    private void filter(String s){
        ArrayList<cases> filteredlist = new ArrayList<>();
        for(cases cases : modelRecyclerArrayList){
            if(cases.getCountry_name().toLowerCase().contains(s.toLowerCase())){
                filteredlist.add(cases);
            }

        }

        adapter.filteredlist(filteredlist);
    }

   /* public void products_show_back(View view) {
        startActivity(new Intent(this,com.example.project.profile.class));
    }*/

    public void cases_back(View view) {
        if(getIntent().getStringExtra("d")==null){
            startActivity(new Intent(this,com.example.project.profile.class));
        }
        else{
            startActivity(new Intent(this,com.example.project.ui.welcome.class));
        }

    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mView.setVisibility(show ? View.GONE : View.VISIBLE);
            mView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });

        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            // tvLoad.setVisibility(show ? View.VISIBLE : View.GONE);
            mView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }


    /*LayoutInflater layoutInflater;
        layoutInflater = LayoutInflater.from(getApplicationContext());

        final View view2 = layoutInflater.inflate(R.layout.edit_search, null);
        final AlertDialog alertDialog = new AlertDialog.Builder(getApplicationContext()).create();
        alertDialog.setTitle("Your Title Here");
        //alertDialog.setIcon("Icon id here");
        alertDialog.setCancelable(false);
        alertDialog.setMessage("Your Message Here");


        final EditText etComments = (EditText) view2.findViewById(R.id.etComments);

        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                String search = etComments.getText().toString();



            }
        });


        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                alertDialog.dismiss();
            }
        });


        alertDialog.setView(view);
        alertDialog.show();
    }*/
}
