package com.example.project;

import android.util.Base64;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

public class JWUtils {

    public static String decodeJWT(String Encode) throws Exception{

        String[] split = Encode.split("\\.");
        //"out_header : " + get_json(split[0]));
        //"out_payload : " + get_json(split[1]) );
        return get_json(split[1]);




    }


    public static String get_json(String Encode){
        byte[] decode_byte = Base64.decode(Encode,Base64.URL_SAFE);
        try {
            return new String(decode_byte,"UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            //"error : " + e.getMessage());
            return ("error");

        }

    }

}
