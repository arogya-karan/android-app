package com.example.project;

public class body_edit {

    String pid;
    edit_del_prod_class product;

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public edit_del_prod_class getProduct() {
        return product;
    }

    public void setProduct(edit_del_prod_class product) {
        this.product = product;
    }
}
