package com.example.project.ui.send;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.project.R;
import com.example.project.Shared_pref;
import com.example.project.api_header;
import com.example.project.cases;
import com.example.project.recycler_adapter;
import com.example.project.response_here;
import com.example.project.ui.slideshow.view_list;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class products_show extends AppCompatActivity implements recycler_adapter_products.OnNoteList2{
    FloatingActionButton floatingActionButton;
    RecyclerView recyclerView;
    String code;
    ArrayList<String> productCode;
    recycler_adapter_products adapter;
    TextView tv_empty;
    ArrayList<String> pid;

    ArrayList<products_class> modelRecyclerArrayList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_products_show);
        tv_empty = findViewById(R.id.tv_empty);
        pid = new ArrayList<>();


        recyclerView = findViewById(R.id.recycler_view_products);

        productCode = new ArrayList<>();
        tv_empty.setVisibility(View.GONE);
        get_all_products();

        floatingActionButton = findViewById(R.id.fab_add_products);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                startActivity(new Intent(getApplicationContext(),com.example.project.product.class));
            }
        });


    }

    private void get_all_products(){
        Shared_pref shared_pref =  new Shared_pref(this);
        final String token = shared_pref.get_token();


        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request newRequest  = chain.request().newBuilder()
                        .addHeader("Authorization", "Bearer " + token)
                        .build();
                return chain.proceed(newRequest);
            }
        }).build();
        Retrofit retrofit  = new Retrofit.Builder()
                .baseUrl("https://covid-19-info-123.herokuapp.com/")
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        api_header api = retrofit.create(api_header.class);

        Call<ResponseBody> call = api.get_seller_products();
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.code() == 200) {

                        //response.body());
                    //String jsonresponse = response.body();
                    try {

                        if (response.body() == null) {
                            tv_empty.setVisibility(View.GONE);
                        }
                        else{
                            tv_empty.setVisibility(View.GONE);
                            writeRecycler(response.body().string());


                        }


                    } catch (IOException e) {
                        e.printStackTrace();
                        Toast.makeText(getApplicationContext(),"error : " +e.getMessage() ,Toast.LENGTH_SHORT).show();

                    }

                }
                else{
                    //response.code());
                    Toast.makeText(getApplicationContext(),"error : " +response.message() ,Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                //"fail : " + t.getMessage());

            }
        });


    }

    private void writeRecycler(String response) {
        try {
            //getting the whole json object from the response
            JSONObject obj = new JSONObject(response);


            modelRecyclerArrayList = new ArrayList<>();
            JSONArray dataArray  = obj.getJSONArray("products");

            for (int i = 0; i < dataArray.length(); i++) {

                products_class prod = new products_class();
                JSONObject dataobj = dataArray.getJSONObject(i);

                prod.setImageThumbnail(dataobj.getString("imageThumbnail"));
                prod.setProductName(dataobj.getString("productName"));
                prod.setProductDesc(dataobj.getString("productDesc"));
                prod.setProductType(dataobj.getString("productType"));
                prod.setAvailable(dataobj.getString("availablity"));

                prod.setCertified(dataobj.getString("isCertified"));
                if(dataobj.getString("quantity").equals(String.valueOf(1000000000))){
                    prod.setQuantity("not defined");
                }
                else{
                    prod.setQuantity(dataobj.getString("quantity"));
                }

                prod.setProductPrice(dataobj.getString("productPrice"));
                pid.add(dataobj.getString("_id"));

                productCode.add(dataobj.getString("productCode"));


                //prod.getProductName());
                modelRecyclerArrayList.add(prod);

            }
            //modelRecyclerArrayList);

            if(modelRecyclerArrayList.isEmpty()){
                tv_empty.setVisibility(View.GONE);
            }
            else{
                tv_empty.setVisibility(View.GONE);
            }

            adapter = new recycler_adapter_products(this,modelRecyclerArrayList,this);
            recyclerView.setAdapter(adapter);
            recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));



        } catch (JSONException e) {
            e.printStackTrace();
            //"error" + e.getMessage());
        }



    }



    @Override
    public void OnnoteClick2(int position) {
        Toast.makeText(this,productCode.get(position),Toast.LENGTH_LONG).show();
        //"clicked");
        // Toast.makeText(view_list.this,modelRecyclerArrayList.get(position).getCountry_name().toString(),Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this,com.example.project.product.class);
        intent.putExtra("click" , productCode.get(position));
        intent.putExtra("pid",pid.get(position));

        startActivity(intent);


    }



    public void back_to_pro(View view) {
        startActivity(new Intent(this,com.example.project.ui.welcome.class));
    }
}
//// {"success":true,"products":[{"productTags":["heelo","prop"],"media":["https://res.cloudinary.com/nikserver/image/upload/v1587052966/h9td1eflunfw4bqgnwht.jpg"],"imageThumbnail":"https://res.cloudinary.com/nikserver/image/upload/v1587052957/annd2ouethtxrkgtm1vg.png","isCertified":false,"_id":"5e9881ab91ff380017f807ee","productName":"abc","productCode":"YWJj.sanket.c2luaGFzYW5rZXQwOTlAZ21haWwuY29t","productDesc":"hshs","productType":"bsbs","quantity":645,"availablity":true,"sellerId":"5e987f0e91ff380017f807e5","dateOfIssue":"1587052971778","geometry":{"type":"Point","_id":"5e98817a91ff380017f807ea","coordinates":[78.96288,20.593684]},"__v":0},{"productTags":["heelo","prop"],"media":["https://res.cloudinary.com/nikserver/image/upload/v1587053250/ff58zeszbdysurjaondv.jpg"],"imageThumbnail":"https://res.cloudinary.com/nikserver/image/upload/v1587053266/n6r1eummos9ya6vmhyjt.png","isCertified":false,"_id":"5e9882d891ff380017f807f3","productName":"hehs","productCode":"aGVocw==.sanket.c2luaGFzYW5rZXQwOTlAZ21haWwuY29t","productDesc":"gehe","productType":"gege","quantity":5484,"availablity":true,"sellerId":"5e987f0e91ff380017f807e5","dateOfIssue":"1587053272020","geometry":{"type":"Point","_id":"5e98817a91ff380017f807ea","coordinates":[78.96288,20.593684]},"__v":0},{"productTags":["heelo","prop"],"media":["https://res.cloudinary.com/nikserver/image/upload/v1587054719/ys6etb3roeu5h83hwros.jpg"],"imageThumbnail":"https://res.cloudinary.com/nikserver/image/upload/v1587054709/xwhqew9gtvfj8098fyzt.png","isCertified":false,"_id":"5e9888852c8a560017648230","productName":"hello","productCode":"aGVsbG8=.sanket.c2luaGFzYW5rZXQwOTlAZ21haWwuY29t","productDesc":"hehs","productType":"bsbs","quantity":94,"availablity":true,"sellerId":"5e987f0e91ff380017f807e5","dateOfIssue":"1587054725861","geometry":{"type":"Point","_id":"5e98817a91ff380017f807ea","coordinates":[78.96288,20.593684]},"__v":0}]}