package com.example.project.ui;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.example.project.JWUtils;
import com.example.project.R;
import com.example.project.Shared_pref;
import com.example.project.ui.home.request_details;

import org.json.JSONObject;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class welcome extends AppCompatActivity {
    TextView tv, tvsell;
    private int PERMISSION_ID = 44;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        Shared_pref shared_pref = new Shared_pref(getApplicationContext());
        tv = findViewById(R.id.tv_hi);
        tvsell = findViewById(R.id.textView_sell);


        Uri uri = getIntent().getData();
        if(uri != null){
            List<String> params = uri.getPathSegments();
            String id = params.get(params.size()-1);

            // Toast.makeText(this,"id : " + id,Toast.LENGTH_SHORT).show();
            //"ehllo");

            try {
                JWUtils.decodeJWT(id);


            } catch (Exception e) {
                e.printStackTrace();
                //"mail : : " +e.getMessage());
            }

            //// SHARED PREF

            shared_pref.add_token(id);
            //Toast.makeText(this,"token : done ",Toast.LENGTH_LONG).show();


        }
        else{
            // Toast.makeText(this,"id :  error",Toast.LENGTH_SHORT).show();
        }

       /* if(!checkPermissions()){
            requestPermissions();
        }*/

        try {
            String tok = JWUtils.decodeJWT(shared_pref.get_token());
            JSONObject object = new JSONObject(tok).getJSONObject("user");
            boolean b = object.getBoolean("isSeller");
            if(b){
                  tvsell.setText("Your Products");
            }
            else{
                tvsell.setText("Become a Seller");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }


        getUser();
    }

    private boolean checkPermissions() {
        if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        return false;
    }

    private void requestPermissions() {
        ActivityCompat.requestPermissions(
                this,
                new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION},
                PERMISSION_ID
        );
    }

    public void getUser(){
        Shared_pref shared_pref = new Shared_pref(getApplicationContext());
        String id = shared_pref.get_token();
        try {
            String id1 = JWUtils.decodeJWT(id);
            JSONObject jsonObject = new JSONObject(id1).getJSONObject("user");
            tv.setText("Hi " + jsonObject.getString("name") + " !");


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void nearby_seller(View view) {
        startActivity(new Intent(getApplicationContext(),com.example.project.profile.class));

    }

    public void covidinfo(View view) {
        Intent i  = new Intent(getApplicationContext(),com.example.project.Covid_info.class);
        i.putExtra("c","c");
        startActivity(i);
    }

    public void go_to_view_list(View view) {
        Intent intent = new Intent(getApplicationContext(),com.example.project.ui.slideshow.view_list.class);
        intent.putExtra("d","d");
        startActivity(intent);
    }

    public void go_to_covid_info(View view) {
        Intent i  = new Intent(getApplicationContext(),com.example.project.Covid_info.class);
        i.putExtra("c","c");
        startActivity(i);
        //startActivity(new Intent(getApplicationContext(),com.example.project.Covid_info.class));
    }

    public void go_to_labs(View view) {
        Shared_pref shared_pref1 = new Shared_pref(getApplicationContext());
        String add = shared_pref1.get_Address();
        Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
        List<Address> addresses = null;
        try {
            addresses = geocoder.getFromLocationName(add, 1);
            Address address2 = addresses.get(0);
            double longitude = address2.getLongitude();
            double latitude = address2.getLatitude();

            Uri gmm = Uri.parse("geo:"+latitude+","+longitude+"?q=near by clinical labs");
            Intent mapIntent = new Intent(Intent.ACTION_VIEW,gmm);
            mapIntent.setPackage("com.google.android.apps.maps");
            startActivity(mapIntent);

            //startActivity(new Intent(getApplicationContext(), com.example.project.ui.gallery.MapsActivity2.class));

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void go_to_bb(View view) {
        Shared_pref shared_pref1 = new Shared_pref(getApplicationContext());
        String add = shared_pref1.get_Address();
        Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
        List<Address> addresses = null;
        try {
            addresses = geocoder.getFromLocationName(add, 1);
            Address address2 = addresses.get(0);
            double longitude = address2.getLongitude();
            double latitude = address2.getLatitude();

            Uri gmm = Uri.parse("geo:"+latitude+","+longitude+"?q=near by blood bank");
            Intent mapIntent = new Intent(Intent.ACTION_VIEW,gmm);
            mapIntent.setPackage("com.google.android.apps.maps");
            startActivity(mapIntent);

            //startActivity(new Intent(getApplicationContext(), com.example.project.ui.gallery.MapsActivity2.class));

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void go_to_hospital(View view) {
        Shared_pref shared_pref1 = new Shared_pref(getApplicationContext());
        String add = shared_pref1.get_Address();
        Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
        List<Address> addresses = null;
        try {
            addresses = geocoder.getFromLocationName(add, 1);
            Address address2 = addresses.get(0);
            double longitude = address2.getLongitude();
            double latitude = address2.getLatitude();

            Uri gmm = Uri.parse("geo:"+latitude+","+longitude+"?q=near by hospitals");
            Intent mapIntent = new Intent(Intent.ACTION_VIEW,gmm);
            mapIntent.setPackage("com.google.android.apps.maps");
            startActivity(mapIntent);

            //startActivity(new Intent(getApplicationContext(), com.example.project.ui.gallery.MapsActivity2.class));

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void go_to_profile(View view) {
        startActivity(new Intent(getApplicationContext(),com.example.project.profile.class));
    }

    public void call_ambulance(View view) {

        if(ContextCompat.checkSelfPermission(welcome.this, Manifest.permission.CALL_PHONE)!= PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(welcome.this,new String[] {Manifest.permission.CALL_PHONE},1);

        }
        else{
            String num = "102";

            Intent intent = new Intent(Intent.ACTION_CALL, Uri.fromParts("tel", num, null));
            startActivity(intent);
            //startActivity(new Intent(Intent.ACTION_CALL, Uri.parse(num)));
        }
    }

    public void become_seller(View view) {

        if(tvsell.getText().toString().equals("Your Products")){
            startActivity(new Intent(getApplicationContext(),com.example.project.ui.send.products_show.class));
        }
        else{
            startActivity(new Intent(getApplicationContext(),com.example.project.ui.share.sell_Activity.class));
        }


    }
}
