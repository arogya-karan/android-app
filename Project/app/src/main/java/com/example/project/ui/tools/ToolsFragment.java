package com.example.project.ui.tools;

import android.content.DialogInterface;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.project.R;
import com.example.project.Register;
import com.example.project.Shared_pref;
import com.example.project.add_seller_class;
import com.example.project.api_header;
import com.example.project.response_here;
import com.example.project.ui.share.new_address;

import java.io.IOException;
import java.security.KeyStore;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ToolsFragment extends Fragment {

    private ToolsViewModel toolsViewModel;
    AlertDialog.Builder builder;
    TextView btn_logout, btn_change_password;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        toolsViewModel =
                ViewModelProviders.of(this).get(ToolsViewModel.class);
        View root = inflater.inflate(R.layout.fragment_tools, container, false);

        toolsViewModel.getText().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                            }
        });

        btn_change_password = root.findViewById(R.id.tv_pass_change);
        btn_logout = root.findViewById(R.id.tv_logout);
        builder = new AlertDialog.Builder(Objects.requireNonNull(getContext()));

        btn_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logout();
            }
        });

        btn_change_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changepassword();
            }
        });

        return root;
    }

    private void logout() {

        builder.setMessage("Do you want to LOGOUT ?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Shared_pref shared_pref = new Shared_pref(getContext());
                        shared_pref.clear();
                       // shared_pref.clear_flag();
                        shared_pref.clear_address();
                        shared_pref.clear_flag();

                        Toast.makeText(getContext(),"Logged Out",Toast.LENGTH_SHORT).show();


                        startActivity(new Intent(getActivity(),com.example.project.MainActivity.class));






                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //  Action for 'NO' Button
                        dialog.cancel();


                    }
                });
        //Creating dialog box
        AlertDialog alert = builder.create();
        //Setting the title manually
        alert.setTitle("Logout");
        alert.show();


    }

    private  void changepassword(){
        startActivity(new Intent(getActivity(),com.example.project.change_password.class));

    }


}