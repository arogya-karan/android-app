package com.example.project;

import android.content.Context;
import android.content.SharedPreferences;

import androidx.security.crypto.EncryptedSharedPreferences;
import androidx.security.crypto.MasterKeys;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.PublicKey;

public class Shared_pref  {
    Context context;
    private static Shared_pref instance;

    public Shared_pref(Context context) {
        this.context = context;
    }

    /*public static synchronized Shared_pref getInstance(Context context) {
        if (instance == null) {
            instance = new Shared_pref(context);

        }
        return instance;
    }*/



    private SharedPreferences getEncryptedSharedPreferences(){



        SharedPreferences sharedPreferences = null;
        String masterKeyAlias = null;
        try {
            masterKeyAlias = MasterKeys.getOrCreate(MasterKeys.AES256_GCM_SPEC);
            sharedPreferences = EncryptedSharedPreferences.create(
                    "secret_shared_prefs_file",
                    masterKeyAlias,
                    context,
                    EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
                    EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM
            );
        } catch (GeneralSecurityException | IOException e) {
            e.printStackTrace();
        }
        return sharedPreferences;
    }

    public void add_token(String token){

        getEncryptedSharedPreferences().edit()
                .putString("Token", token)
                .apply();

    }

    public String get_token(){
        String token = getEncryptedSharedPreferences().getString("Token", "token : nil");
        return  token;
    }

    public void clear(){
        getEncryptedSharedPreferences().edit().remove("Token").apply();


    }
    public void add_flag(Integer flag){
        getEncryptedSharedPreferences().edit().putInt("flag",flag).apply();
    }

    public Integer get_flag(){
        Integer flag = getEncryptedSharedPreferences().getInt("flag",0);
        return flag;


    }
    public void add_address(String address){
        getEncryptedSharedPreferences().edit().putString("address",address).apply();

    }
    public String get_Address(){
        String address = getEncryptedSharedPreferences().getString("address","earth");
        return address;
    }

   public void clear_flag(){
        getEncryptedSharedPreferences().edit().remove("flag").apply();
   }

   public void is_Business(Boolean isBusiness){
        getEncryptedSharedPreferences().edit().putBoolean("is",isBusiness).apply();
   }
   public  Boolean get_is_business(){
        Boolean bool = getEncryptedSharedPreferences().getBoolean("is",false);
        return bool;

   }
   public void clear_address(){
       getEncryptedSharedPreferences().edit().remove("address").apply();


   }
   public void add_lat(String lat){
        getEncryptedSharedPreferences().edit().putString("lat",lat).apply();
   }
   public  void add_long(String longi){
        getEncryptedSharedPreferences().edit().putString("long",longi).apply();
   }
   public String getLat(){
        String lat = getEncryptedSharedPreferences().getString("lat","0");
        return lat;
   }
   public String getLongi(){
        String longi = getEncryptedSharedPreferences().getString("long","0");
        return longi;
   }




}
