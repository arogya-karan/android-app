package com.example.project.ui.home;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Looper;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.DrawableRes;
import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.project.JWUtils;
import com.example.project.R;
import com.example.project.Shared_pref;
import com.example.project.api_header;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.pusher.client.Pusher;
import com.pusher.client.PusherOptions;
import com.pusher.client.channel.Channel;
import com.pusher.client.channel.ChannelEventListener;
import com.pusher.client.channel.PusherEvent;
import com.pusher.client.channel.SubscriptionEventListener;
import com.pusher.client.connection.ConnectionEventListener;
import com.pusher.client.connection.ConnectionState;
import com.pusher.client.connection.ConnectionStateChange;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Objects;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class HomeFragment extends Fragment implements search_adapter.OnNoteList  {

    private HomeViewModel homeViewModel;
    MapView mMapView;
    private GoogleMap googleMap;
    String longitude, latitude;
    RecyclerView recyclerView;
    private ArrayList<String> latarray;
    EditText editText;
    private ArrayList<String> longarray;
    private ArrayList<Boolean> isBusiness;
    Boolean bool_isbusiness, buizz;
    ArrayList<String> array;
    ArrayList<String> productid;

    private int PERMISSION_ID = 44;

    FusedLocationProviderClient mFusedLocationClient;
    search_adapter adapter;

    ArrayList<sel> modelRecyclerArrayList;
    String s_uid;
    ArrayList<String> s_uid_array;
    private int i, j, c = 0;
    ArrayList<Integer> c_Array;
    ArrayList<String> seller_name_array;
    String sellername;

    private String lat, longi;
    ArrayList<String> product_code;

    Button btn_sell, btn_buy;
    String sellerid;
    ArrayList<String> sellerid_Array;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel =
                ViewModelProviders.of(Objects.requireNonNull(getActivity())).get(HomeViewModel.class);
        View root = inflater.inflate(R.layout.fragment_home, container, false);
        //final TextView textView = root.findViewById(R.id.text_home);
        homeViewModel.getText().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                //textView.setText(s);
            }
        });

        mMapView = (MapView) root.findViewById(R.id.mapView);
        s_uid_array = new ArrayList();
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(Objects.requireNonNull(getContext()));

        getLastLocation();
        mMapView.onCreate(savedInstanceState);
        longarray = new ArrayList<>();
        array = new ArrayList();
        recyclerView = root.findViewById(R.id.rv_mark);
        seller_name_array = new ArrayList<>();
        c_Array = new ArrayList<>();
        editText = root.findViewById(R.id.etmarker);
        isBusiness = new ArrayList<>();
       // recyclerView = root.findViewById(R.id.rv_search);
        productid = new ArrayList<>();
        latarray = new ArrayList<>();
        product_code = new ArrayList<>();
        sellerid_Array = new ArrayList<>();
        Shared_pref shared_pref = new Shared_pref(getContext());
        lat = "Null";
        recyclerView.setVisibility(View.GONE);
        longi = "nUll";
        //push();
       // getcoords();
        mMapView.onResume(); // needed to get the map to display immediately




        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
            requestPermissions();
            //mMapView.onPause();
            getLastLocation();
            //getcoords();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if(!checkPermissions()){
            requestPermissions();

            getLastLocation();

        }
        else{
            getLastLocation();
           getcoords();
          // mMapView.onResume();

        }

       // getcoords();

        if (checkPermissions()) {
            getcoords();
           // mMapView.onResume();

            /*f(shared_pref.get_Address().equals("earth")){
                //"getnear");
                 getcoords();
                //get_near();
            }
            else{
                getLastLocation();
                getcoords();
            }*/



            //push();

        } else {
            requestPermissions();
            //Toast.makeText(getContext(), "Please enable gps", Toast.LENGTH_LONG).show();
           // get_near();

            /*if (checkPermissions()) {
                if(shared_pref.get_Address().equals("earth")){
                    get_near();
                }
                else{
                    getcoords();
                }



            } else {
                Toast.makeText(getContext(), "please enable gps", Toast.LENGTH_LONG).show();
            }*/
        }

        search2();




      //  mMapView.onResume(); // needed to get the map to display immediately


        return root;
    }


   /* private boolean checkPermissions() {
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED)
                {
            return true;
        }
        return false;
    }

    private void requestPermissions() {
        ActivityCompat.requestPermissions(
                getActivity(),
                new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION},
                PERMISSION_ID
        );
    }*/


    @SuppressLint("MissingPermission")
    private void getLastLocation(){
        if (checkPermissions()) {
            if (isLocationEnabled()) {
                mFusedLocationClient.getLastLocation().addOnCompleteListener(
                        new OnCompleteListener<Location>() {
                            @Override
                            public void onComplete(@NonNull Task<Location> task) {
                                Location location = task.getResult();
                                if (location == null) {
                                    requestNewLocationData();
                                } else {
                                    latitude = String.valueOf(location.getLatitude());
                                    longitude = String.valueOf(location.getLongitude());
                                    Shared_pref shared_pref = new Shared_pref(getContext());
                                    shared_pref.add_lat(latitude);
                                    shared_pref.add_long(longitude);

                                }
                            }
                        }
                );
            } else {
                Toast.makeText(getContext(), "Turn on location", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }
        } else {
            requestPermissions();
        }
    }


    @SuppressLint("MissingPermission")
    private void requestNewLocationData(){

        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(0);
        mLocationRequest.setFastestInterval(0);
        mLocationRequest.setNumUpdates(1);

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getContext());
        mFusedLocationClient.requestLocationUpdates(
                mLocationRequest, mLocationCallback,
                Looper.myLooper()
        );

    }

    private LocationCallback mLocationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            Location mLastLocation = locationResult.getLastLocation();
            latitude = String.valueOf(mLastLocation.getLatitude());
            longitude = String.valueOf(mLastLocation.getLongitude());

            Shared_pref shared_pref = new Shared_pref(getContext());
            shared_pref.add_long(longitude);
            shared_pref.add_lat(latitude);
        }
    };

    private boolean checkPermissions() {
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        return false;
    }

    private void requestPermissions() {
        ActivityCompat.requestPermissions(
                getActivity(),
                new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION},
                PERMISSION_ID
        );
    }

    private boolean isLocationEnabled() {
        LocationManager locationManager = (LocationManager) Objects.requireNonNull(getContext()).getSystemService(Context.LOCATION_SERVICE);
        assert locationManager != null;
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(
                LocationManager.NETWORK_PROVIDER
        );
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_ID) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                getLastLocation();
                requestNewLocationData();
                getcoords();
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (checkPermissions()) {
            getLastLocation();
            requestNewLocationData();
            getcoords();
        }
        else {
            requestPermissions();
        }

    }

    private BitmapDescriptor bitmapDescriptorFromVector(Context context, @DrawableRes int vectorDrawableResourceId) {
        Drawable background = ContextCompat.getDrawable(context, R.drawable.ic_location_on_black_24dp);
        background.setBounds(0, 0, background.getIntrinsicWidth(), background.getIntrinsicHeight());
        Drawable vectorDrawable = ContextCompat.getDrawable(context, vectorDrawableResourceId);
        vectorDrawable.setBounds(40, 20, vectorDrawable.getIntrinsicWidth() + 40, vectorDrawable.getIntrinsicHeight() + 20);
        Bitmap bitmap = Bitmap.createBitmap(background.getIntrinsicWidth(), background.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        background.draw(canvas);
        vectorDrawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }




        private void getcoords(){


        final Shared_pref shared_pref = new Shared_pref(getContext());
        final String token = shared_pref.get_token();
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request newRequest  = chain.request().newBuilder()
                        .addHeader("Authorization", " Bearer " + token)
                        .build();
                return chain.proceed(newRequest);
            }
        }).build();
        Retrofit retrofit  = new Retrofit.Builder()
                .baseUrl("https://covid-19-info-123.herokuapp.com/")
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        api_header api = retrofit.create(api_header.class);

        Call<ResponseBody> call = api.get_nearby();
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                if(response.code() == 200) {
                    try {
                        assert response.body() != null;
                        String hello = (response.body().string());

                        JSONObject jsonObject = new JSONObject(hello);

                        JSONArray array = jsonObject.getJSONArray("sellers");
                        if(array.length() <=1) {

                            JSONObject obj = array.getJSONObject(0);
                            sellerid = obj.getString("_id");
                            sellername = obj.getString("sellerName");
                            s_uid = obj.getString("uid");

                            bool_isbusiness = obj.getBoolean("isBusiness");
                            JSONObject geom = obj.getJSONObject("geometry");
                            JSONArray code = obj.getJSONArray("productList");
                            c = code.length();

                            JSONArray coords = geom.getJSONArray("coordinates");
                            latarray.add(coords.get(1).toString());
                            longarray.add(coords.get(0).toString());
                            lat = coords.get(1).toString();
                            longi = coords.get(0).toString();

                        }else{
                            for( i =0; i<array.length() ; i++) {
                                JSONObject obj = array.getJSONObject(i);
                                JSONObject geom = obj.getJSONObject("geometry");
                                sellerid_Array.add(obj.getString("_id"));
                                s_uid_array.add(obj.getString("uid"));
                                JSONArray code = obj.getJSONArray("productList");
                                seller_name_array.add(obj.getString("sellerName"));
                                isBusiness.add(obj.getBoolean("isBusiness"));
                                JSONArray coords = geom.getJSONArray("coordinates");
                                c_Array.add(code.length());
                                latarray.add(coords.get(1).toString());
                                longarray.add(coords.get(0).toString());
                                /*lat = coords.get(1).toString();
                                longi = coords.get(0).toString();*/
                            }
                        }

                        mMapView.getMapAsync(new OnMapReadyCallback() {
                            @Override
                            public void onMapReady(GoogleMap mMap) {
                                googleMap = mMap;

                                // For showing a move to my location button
                                googleMap.setMyLocationEnabled(true);
                                googleMap.setIndoorEnabled(false);


                                // For dropping a marker at a point on the Map
                                if (latarray.size() <= 1) {

                                    String latt = shared_pref.getLat();
                                    String lngi = shared_pref.getLongi();

                                    LatLng current = new LatLng(Double.parseDouble(latt), Double.parseDouble(lngi));
                                    LatLng position = new LatLng(Double.parseDouble(lat), Double.parseDouble(longi));
                                    googleMap.addMarker(new MarkerOptions().position(position).title(sellername)


                                            .snippet("(" +sellerid +")" + "\n" + "[" + s_uid + "]"));

                                    // For zooming automatically to the location of the user

                                    CameraPosition cameraPosition = new CameraPosition.Builder().target(current).zoom(13).build();
                                    googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

                                    mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {

                                        @Override
                                        public View getInfoWindow(Marker arg0) {
                                            return null;
                                        }

                                        @Override
                                        public View getInfoContents(Marker marker) {

                                            LinearLayout info = new LinearLayout(getContext());
                                            info.setOrientation(LinearLayout.VERTICAL);

                                            TextView title = new TextView(getContext());
                                            title.setTextColor(Color.BLACK);
                                            title.setGravity(Gravity.CENTER);
                                            title.setTypeface(null, Typeface.BOLD);
                                            title.setText(marker.getTitle());

                                            TextView snippet = new TextView(getContext());
                                            snippet.setTextColor(Color.BLACK);
                                            snippet.setGravity(Gravity.CENTER);
                                            snippet.setText(" Seller id : " +marker.getSnippet());

                                            TextView business = new TextView(getContext());
                                            if(bool_isbusiness.equals(false)) {
                                                business.setText("Volunteer / Individual");
                                            }
                                            else{
                                                business.setText("Business / Institution ");
                                            }
                                            business.setGravity(Gravity.CENTER);
                                            business.setTextColor(Color.BLACK);


                                            TextView mark = new TextView(getContext());
                                            mark.setText("Tap for details");
                                            mark.setTextColor(Color.BLUE);
                                            mark.setGravity(Gravity.CENTER);
                                            mark.setTypeface(Typeface.MONOSPACE, Typeface.BOLD);
                                            mark.setPadding(10,10,10,10);


                                            info.addView(title);
                                            info.addView(snippet);
                                            info.addView(business);
                                            info.addView(mark);


                                            return info;
                                        }
                                    });

                                } else {
                                    String latt = shared_pref.getLat();
                                    String lngi = shared_pref.getLongi();

                                    LatLng current = new LatLng(Double.parseDouble(latt), Double.parseDouble(lngi));
                                    for (j = 0; j < latarray.size(); j++) { //brackets .. for loop extended..check ..test then go..
                                        //  LatLng current = new LatLng(Double.parseDouble(latitude), Double.parseDouble(longitude));
                                        LatLng position = new LatLng(Double.parseDouble(latarray.get(j)), Double.parseDouble(longarray.get(j)));
                                        if(isBusiness.get(j).equals(false)){
                                            googleMap.addMarker(new MarkerOptions().position(position).title(seller_name_array.get(j))


                                                    .snippet("Seller id: " + '('+sellerid_Array.get(j) + ')' + "\n" + '[' + s_uid_array.get(j) + ']' +"\n" + " Volunteer / Individual"));

                                        }
                                        else{
                                            googleMap.addMarker(new MarkerOptions().position(position).title(seller_name_array.get(j))

                                                    .snippet("Seller id: " + '(' + sellerid_Array.get(j) + ')' + "\n" + '[' + s_uid_array.get(j) + ']' +"\n" +  " Business / Institution"));

                                        }


                                        // For zooming automatically to the location of the marker6



                                        CameraPosition cameraPosition = new CameraPosition.Builder().target(current).zoom(10).build();
                                        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                                    }

                                        mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {

                                            @Override
                                            public View getInfoWindow(Marker arg0) {
                                                return null;
                                            }

                                            @Override
                                            public View getInfoContents(Marker marker) {

                                                LinearLayout info = new LinearLayout(getContext());
                                                info.setOrientation(LinearLayout.VERTICAL);

                                                TextView title = new TextView(getContext());
                                                title.setTextColor(Color.BLACK);

                                                title.setGravity(Gravity.CENTER);
                                                title.setTypeface(null, Typeface.BOLD);

                                                title.setText(marker.getTitle());

                                                TextView snippet = new TextView(getContext());
                                                //snippet.setTextColor(Color.GRAY);
                                                snippet.setText(marker.getSnippet());
                                                snippet.setTextColor(Color.BLACK);
                                                snippet.setGravity(Gravity.CENTER);




                                                TextView mark = new TextView(getContext());
                                                mark.setText("Tap for details");
                                                mark.setTextColor(Color.BLUE);
                                                mark.setGravity(Gravity.CENTER);
                                                mark.setTypeface(Typeface.MONOSPACE, Typeface.BOLD);
                                                mark.setPadding(10,10,10,10);


                                                info.addView(title);
                                                info.addView(snippet);
                                                info.addView(mark);


                                                return info;
                                            }
                                        });



                                }
                                googleMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {

                                    @Override
                                    public void onInfoWindowClick(Marker arg0) {

                                        String sell = arg0.getSnippet();
                                        int i = sell.indexOf('(');
                                        int j = sell.indexOf(')');
                                        String sell1 = sell.substring(i+1,j); //sell1 is _id

                                        String suid = arg0.getSnippet(); //suid is uid
                                        int i1 = suid.indexOf('[');
                                        int j1 = suid.indexOf(']');
                                        suid = suid.substring(i1+1,j1);

                                        String me = null;
                                        try {
                                            me = JWUtils.decodeJWT(shared_pref.get_token());
                                            JSONObject jsonObject1 = new JSONObject(me).getJSONObject("user");
                                            String my_id = jsonObject1.getString("_id");
                                            //"new tok : "+shared_pref.get_token());
                                            //my_id);
                                            //suid);

                                            if(my_id.equals(suid)){

                                                Toast.makeText(getContext(),"This Seller is You, you cannot buy your own products",Toast.LENGTH_SHORT).show();
                                            }
                                            else{
                                                Intent intent = new Intent(getContext(),seller_details.class);
                                                intent.putExtra("seller", sell1);
                                                startActivity(intent);

                                            }
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }









                                        // TODO Auto-generated method stub
                                      //  Toast.makeText(getContext(),"click",Toast.LENGTH_LONG).show();




                                    }
                                });


                            }
                        });



                       /* JSONObject obj = array.getJSONObject(0);
                        JSONObject geom = obj.getJSONObject("geometry");
                        //obj);
                        JSONArray coords = geom.getJSONArray("coordinates");
                        //"print "+ coords.get(0)+ "\n" + coords.get(1) );

*/
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }


                }
                else{
                    //response.code());
                    //response.message());
                    try {
                        Toast.makeText(getContext(),response.errorBody().string(),Toast.LENGTH_SHORT).show();

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    private void get_near(){
        getLastLocation();
        requestNewLocationData();
        Shared_pref shared_pref = new Shared_pref(getContext());
        final String token = shared_pref.get_token();
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request newRequest  = chain.request().newBuilder()
                        .addHeader("Authorization", " Bearer " + token)
                        .build();
                return chain.proceed(newRequest);
            }
        }).build();
        Retrofit retrofit  = new Retrofit.Builder()
                .baseUrl("https://covid-19-info-123.herokuapp.com/")
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        api_header api = retrofit.create(api_header.class);
        //"coords" + shared_pref.getLongi() + shared_pref.getLat());

        Call<ResponseBody> call = api.getnear(shared_pref.getLongi(),shared_pref.getLat());
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                if(response.code() == 200) {
                    try {
                        assert response.body() != null;
                        String hello = (response.body().string());
                        //"hel : " +hello);
                        JSONObject jsonObject = new JSONObject(hello);

                        JSONArray array = jsonObject.getJSONArray("sellers");
                        if(array.length() <=1) {

                            JSONObject obj = array.getJSONObject(0);
                            sellerid = obj.getString("_id");
                            sellername = obj.getString("sellerName");
                            JSONObject geom = obj.getJSONObject("geometry");
                            JSONArray code = obj.getJSONArray("productList");
                            c = code.length();

                            JSONArray coords = geom.getJSONArray("coordinates");
                            latarray.add(coords.get(1).toString());
                            longarray.add(coords.get(0).toString());
                            lat = coords.get(1).toString();
                            longi = coords.get(0).toString();

                        }else{
                            for( i =0; i<array.length() ; i++) {
                                JSONObject obj = array.getJSONObject(i);
                                JSONObject geom = obj.getJSONObject("geometry");
                                sellerid_Array.add(obj.getString("_id"));
                                JSONArray code = obj.getJSONArray("productList");
                                seller_name_array.add(obj.getString("sellerName"));
                                JSONArray coords = geom.getJSONArray("coordinates");
                                c_Array.add(code.length());
                                latarray.add(coords.get(1).toString());
                                longarray.add(coords.get(0).toString());
                                /*lat = coords.get(1).toString();
                                longi = coords.get(0).toString();*/
                            }
                        }

                        mMapView.getMapAsync(new OnMapReadyCallback() {
                            @Override
                            public void onMapReady(GoogleMap mMap) {
                                googleMap = mMap;

                                // For showing a move to my location button
                                googleMap.setMyLocationEnabled(true);
                                googleMap.setIndoorEnabled(false);


                                // For dropping a marker at a point on the Map
                                if (latarray.size() <= 1) {
                                    //lat + longi);
                                    LatLng current = new LatLng(Double.parseDouble(latitude), Double.parseDouble(longitude));
                                    LatLng position = new LatLng(Double.parseDouble(lat), Double.parseDouble(longi));
                                    googleMap.addMarker(new MarkerOptions().position(position).title(sellername)

                                            .snippet(sellerid));

                                    // For zooming automatically to the location of the marker6

                                    CameraPosition cameraPosition = new CameraPosition.Builder().target(current).zoom(13).build();
                                    googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

                                    mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {

                                        @Override
                                        public View getInfoWindow(Marker arg0) {
                                            return null;
                                        }

                                        @Override
                                        public View getInfoContents(Marker marker) {

                                            LinearLayout info = new LinearLayout(getContext());
                                            info.setOrientation(LinearLayout.VERTICAL);

                                            TextView title = new TextView(getContext());
                                            title.setTextColor(Color.BLACK);
                                            title.setGravity(Gravity.CENTER);
                                            title.setTypeface(null, Typeface.BOLD);
                                            title.setText(marker.getTitle());

                                            TextView snippet = new TextView(getContext());
                                            snippet.setTextColor(Color.GRAY);
                                            snippet.setText("Seller id : " +marker.getSnippet());

                                            TextView mark = new TextView(getContext());
                                            mark.setText("Tap for details");

                                            info.addView(title);
                                            info.addView(snippet);
                                            info.addView(mark);


                                            return info;
                                        }
                                    });

                                } else {
                                    for (j = 0; j < latarray.size(); j++) {
                                        LatLng current = new LatLng(Double.parseDouble(latitude), Double.parseDouble(longitude));
                                        LatLng position = new LatLng(Double.parseDouble(latarray.get(j)), Double.parseDouble(longarray.get(j)));
                                        googleMap.addMarker(new MarkerOptions().position(position).title(seller_name_array.get(j))

                                                .snippet(sellerid_Array.get(j)));

                                        // For zooming automatically to the location of the marker6

                                        CameraPosition cameraPosition = new CameraPosition.Builder().target(current).zoom(10).build();
                                        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                                    }
                                    mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {

                                        @Override
                                        public View getInfoWindow(Marker arg0) {
                                            return null;
                                        }

                                        @Override
                                        public View getInfoContents(Marker marker) {

                                            LinearLayout info = new LinearLayout(getContext());
                                            info.setOrientation(LinearLayout.VERTICAL);

                                            TextView title = new TextView(getContext());
                                            title.setTextColor(Color.BLACK);
                                            title.setGravity(Gravity.CENTER);
                                            title.setTypeface(null, Typeface.BOLD);
                                            title.setText(marker.getTitle());

                                            TextView snippet = new TextView(getContext());
                                            snippet.setTextColor(Color.GRAY);
                                            snippet.setText("seller id : "+marker.getSnippet());

                                            TextView mark = new TextView(getContext());
                                            mark.setText(String.valueOf("Tap for details"));

                                            info.addView(title);
                                            info.addView(snippet);
                                            info.addView(mark);


                                            return info;
                                        }
                                    });


                                }
                                googleMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {

                                    @Override
                                    public void onInfoWindowClick(Marker arg0) {

                                        String sell = arg0.getSnippet();
                                        Intent intent = new Intent(getContext(),products_of_one_seller.class);
                                        intent.putExtra("seller", sell);
                                        //"here");
                                        startActivity(intent);




                                        // TODO Auto-generated method stub
                                        Toast.makeText(getContext(),"click",Toast.LENGTH_LONG).show();




                                    }
                                });


                            }
                        });



                       /* JSONObject obj = array.getJSONObject(0);
                        JSONObject geom = obj.getJSONObject("geometry");
                        //obj);
                        JSONArray coords = geom.getJSONArray("coordinates");
                        //"print "+ coords.get(0)+ "\n" + coords.get(1) );

*/
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }


                }
                else{
                    //response.code());
                    //response.message());
                    try {
                        Toast.makeText(getContext(),response.errorBody().string(),Toast.LENGTH_SHORT).show();

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });


    }

    public void search2() {
        // filter.setVisibility(View.VISIBLE);

        editText.setVisibility(View.VISIBLE);
        // tvwelcome.setVisibility(View.GONE);
        // tvdesc.setVisibility(View.GONE);

        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if(editText.getText().toString().isEmpty()){
                    mMapView.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.INVISIBLE);
                }
                else{
                    mMapView.setVisibility(View.INVISIBLE);
                    recyclerView.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

                if(editText.getText().toString().isEmpty()){
                    mMapView.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.INVISIBLE);
                }
                else{
                    mMapView.setVisibility(View.INVISIBLE);
                    recyclerView.setVisibility(View.VISIBLE);
                }


               filter(s.toString());
                //filter(editText.getText().toString());



            }
        });


    }

    private void filter(String s){
        Shared_pref shared_pref = new Shared_pref(getContext());
        final String token = shared_pref.get_token();
        //token);
       OkHttpClient client = new OkHttpClient.Builder().addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request newRequest  = chain.request().newBuilder()
                        .addHeader("Authorization", " Bearer " + token)
                        .build();
                return chain.proceed(newRequest);
            }
        }).build();
        Retrofit retrofit  = new Retrofit.Builder()
                .baseUrl("https://covid-19-info-123.herokuapp.com/")
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        api_header api = retrofit.create(api_header.class);

        Call<ResponseBody> call = api.search(s);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                String hello = null;
                try {
                    if (response.body() != null) {
                        hello = response.body().string();
                        writeRecycler(hello);
                    }
                    else{
                        Toast.makeText(getContext(),"No products Found",Toast.LENGTH_SHORT).show();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });

    }
    private void writeRecycler(String response) {
        try {
            //getting the whole json object from the response
            JSONObject obj = new JSONObject(response);


            modelRecyclerArrayList = new ArrayList<>();
            JSONArray dataArray  = obj.getJSONArray("search");

            for (int i = 0; i < dataArray.length(); i++) {

                sel modelRecycler = new sel();
                JSONObject dataobj = dataArray.getJSONObject(i);

                modelRecycler.setImageThumbnail(dataobj.getString("imageThumbnail"));
                modelRecycler.setProductName(dataobj.getString("productName"));
                modelRecycler.setProductDesc(dataobj.getString("productDesc"));
                modelRecycler.setProductType(dataobj.getString("productType"));
                modelRecycler.setAvailablity(dataobj.getString("availablity"));
                modelRecycler.setIsCertified(dataobj.getString("isCertified"));
                //  modelRecycler.setQuantity(dataobj.getString("quantity"));
                modelRecycler.setProductPrice(dataobj.getString("productPrice"));
                modelRecycler.set_id(dataobj.getString("_id"));

                Double d = dataobj.getDouble("dis");
               // String s = String.valueOf(d/1000);
                String s =  String.format(Locale.getDefault(),"%.2f", d/1000);
                modelRecycler.setDis(s);
                modelRecycler.setUid(dataobj.getString("sellerId")); //sellerid
                productid.add(dataobj.getString("_id"));


                //  productCode.add(dataobj.getString("productCode"));



                modelRecyclerArrayList.add(modelRecycler);

            }

            adapter = new search_adapter(getContext(),modelRecyclerArrayList,this);
            recyclerView.setAdapter(adapter);
            recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));




        } catch (JSONException e) {
            e.printStackTrace();
            //"error" + e.getMessage());
        }



    }


    @Override
    public void OnnoteClick(sel sel) {
        //String id = getIntent().getStringExtra("seller");
        Shared_pref shared_pref = new Shared_pref(getContext());
        String me = null;
        try {
            me = JWUtils.decodeJWT(shared_pref.get_token());
            JSONObject jsonObject1 = new JSONObject(me).getJSONObject("user");
            String my_id = jsonObject1.getString("_id");
            //"new tok : "+shared_pref.get_token());


            if(my_id.equals(sel.getUid())){

                Toast.makeText(getContext(),"This Seller is You, you cannot buy your own products",Toast.LENGTH_SHORT).show();
            }
            else{
                Intent intent = new Intent(getContext(),product_details.class);
                intent.putExtra("id", sel.get_id());
                intent.putExtra("selleruid",sel.getUid());

                startActivity(intent);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }







/*
    @Override
    public void onSubscriptionSucceeded(String channelName) {
        //"Subscribed");
    }

    @Override
    public void onEvent(PusherEvent event) {
        //event.getData());

    }*/
}