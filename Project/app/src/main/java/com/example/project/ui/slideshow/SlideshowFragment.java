package com.example.project.ui.slideshow;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.project.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SlideshowFragment extends Fragment {



    TextView tv_tot_conf, tv_tot_deaths, tv_tot_recovered;
    Button btn_view_list;
    private View mProgressView;
    private View mView;

    private SlideshowViewModel slideshowViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        slideshowViewModel =
                ViewModelProviders.of(this).get(SlideshowViewModel.class);
        View root = inflater.inflate(R.layout.fragment_slideshow, container, false);
        // final TextView textView = root.findViewById(R.id.text_slideshow);
        slideshowViewModel.getText().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                // textView.setText(s);
            }
        });

        mView = root.findViewById(R.id.relative_cases);
        mProgressView = root.findViewById(R.id.case_progress);
        tv_tot_conf = root.findViewById(R.id.tv_tot_confirmed);
        tv_tot_deaths = root.findViewById(R.id.tv_tot_deaths);
        tv_tot_recovered = root.findViewById(R.id.tv_tot_recoveries);
        btn_view_list = root.findViewById(R.id.btn_view_list);
        showProgress(true);


        getcases();

        btn_view_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                view_list();
            }
        });

        return root;
    }

    public void getcases() {
        showProgress(true);

       Call<ResponseBody> call = com.example.project.retrofit_client.getInstance().getapi().summary();
       call.enqueue(new Callback<ResponseBody>() {
           @Override
           public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
               showProgress(false);
               String st = null;
               try {

                   if (response.body() != null) {
                       st = response.body().string();
                       showProgress(false);
                   }
                   else{
                       showProgress(true);
                   }
               } catch (IOException e) {
                   e.printStackTrace();
               }
               try {
                   assert st != null;
                   JSONObject jsonObject = new JSONObject(st);


                   JSONObject ob = jsonObject.getJSONObject("Global");


                  tv_tot_conf.setText("TOTAL CONFIRMED : "+ob.getString("TotalConfirmed"));
                  tv_tot_deaths.setText("UNFORTUNATE DEATHS : "+ob.getString("TotalDeaths"));
                  tv_tot_recovered.setText("TOTAL RECOVERED : " +ob.getString("TotalRecovered"));


               } catch (JSONException e) {
                   e.printStackTrace();
               }

           }

           @Override
           public void onFailure(Call<ResponseBody> call, Throwable t) {
               showProgress(false);

           }
       });
    }
    public void view_list(){
        showProgress(false);

        Intent intent = new Intent(getActivity(), view_list.class);
        startActivity(intent);
    }


    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mView.setVisibility(show ? View.GONE : View.VISIBLE);
            mView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });

        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
           // tvLoad.setVisibility(show ? View.VISIBLE : View.GONE);
            mView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }


}