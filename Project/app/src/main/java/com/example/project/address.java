package com.example.project;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class address extends AppCompatActivity {

    EditText et_address_line_1, et_address_line_2, city, state, pin, country;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_address);

        et_address_line_1 = findViewById(R.id.address_line_1);
        et_address_line_2 = findViewById(R.id.address_line_2);
        city = findViewById(R.id.city);
        state = findViewById(R.id.state);
        pin = findViewById(R.id.et_pin);
        country = findViewById(R.id.et_country);




    }

    public void address_ok(View view) {

        String address_1 = et_address_line_1.getText().toString();
        String address2 = et_address_line_2.getText().toString();
        String your_city = city.getText().toString();
        String your_state = state.getText().toString();
        String your_pin = pin.getText().toString();
        String your_country = country.getText().toString();
        String name = getIntent().getStringExtra("name");
        String phone = getIntent().getStringExtra("phone");

        if(isEmpty(et_address_line_1)||isEmpty(et_address_line_2)||isEmpty(city)||isEmpty(state)||isEmpty(pin)||isEmpty(country)){
            Toast.makeText(getApplicationContext(),"All fields Compulsory",Toast.LENGTH_SHORT).show();

        }
        else{
            Intent intent = new Intent(this,com.example.project.Register.class);
        /*intent.putExtra("address1",address_1);
        intent.putExtra("address2",address2);
        intent.putExtra("city", your_city);
        intent.putExtra("state", your_state);
        intent.putExtra("pin", your_pin);
        intent.putExtra("country",your_country);*/

            intent.putExtra("address" , address_1 + "," + address2 + "," + your_city + "," + your_state + "," + your_pin + "," + your_country);
            intent.putExtra("name",name);
            intent.putExtra("phone",phone);
            startActivity(intent);


        }


    }
    private boolean isEmpty(EditText etText) {

        if(etText.getText().toString().trim().length() == 0){
            etText.setError("Cannot be Empty");

            return true;
        }
        else{
            return false;
        }
    }



}
