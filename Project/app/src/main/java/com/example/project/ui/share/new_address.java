package com.example.project.ui.share;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.project.R;
import com.example.project.Shared_pref;
import com.example.project.add_seller_class;
import com.example.project.api_header;
import com.example.project.response_here;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class new_address extends AppCompatActivity {

    EditText et_newaddress_line1, et_newaddressline2, et_newaddress_city, et_newaddress_state, et_newaddress_pin, et_newaddress_country;
    Button new_Address_submit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_address);

        et_newaddress_city = findViewById(R.id.seller_city);
        et_newaddress_country = findViewById(R.id.seller_country);
        et_newaddress_line1 = findViewById(R.id.seller_address_line_1);
        et_newaddress_pin = findViewById(R.id.seller_et_pin);
        et_newaddressline2 = findViewById(R.id.seller_address_line_2);
        et_newaddress_state = findViewById(R.id.seller_state);
        new_Address_submit = findViewById(R.id.btn_new_address_ok);




    }

    public void new_Address_submit(View view) {

        final Shared_pref shared_pref = new Shared_pref(this);
        //String registered_address = shared_pref.get_Address();
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        List<Address> addresses = null;
        String add = et_newaddress_line1.getText().toString() + et_newaddressline2.getText().toString() + " , " + et_newaddress_city.getText().toString()
                + " , " + et_newaddress_state.getText().toString() + " , " + et_newaddress_pin.getText().toString() + " , " + et_newaddress_country.getText().toString();
        try {
            addresses = geocoder.getFromLocationName(add, 1);
            Address address2 = addresses.get(0);
            double longitude = address2.getLongitude();
            double latitude = address2.getLatitude();

            add_seller_class add_seller_class = new add_seller_class();
            add_seller_class.setNewAddress(true);
            //"name? : " + add_seller_class.getName());
            add_seller_class.setLat(String.valueOf(latitude));
            add_seller_class.setLongi(String.valueOf(longitude));
            add_seller_class.setBusiness(false);
            add_seller_class.setAddress(add);

            OkHttpClient client = new OkHttpClient.Builder().addInterceptor(new Interceptor() {
                @Override
                public okhttp3.Response intercept(Chain chain) throws IOException {
                    Request newRequest  = chain.request().newBuilder()
                            .addHeader("Authorization", "Bearer " + shared_pref.get_token())
                            .build();
                    return chain.proceed(newRequest);
                }
            }).build();
            Retrofit retrofit  = new Retrofit.Builder()
                    .baseUrl("https://covid-19-info-123.herokuapp.com/")
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            api_header api = retrofit.create(api_header.class);



            Call<response_here> call = api.add_seller_jwt(add_seller_class);
            call.enqueue(new Callback<response_here>() {
                @Override
                public void onResponse(Call<response_here> call, Response<response_here> response) {
                    if(response.code() == 200){
                        //response.body());

                        assert response.body() != null;
                        String new_tok = response.body().getToken();
                        shared_pref.clear();
                        shared_pref.add_token(new_tok);
                        if(shared_pref.get_flag() != 1) {
                            shared_pref.add_flag(1);
                        }
                        Toast.makeText(getApplicationContext(),"You are Now a seller",Toast.LENGTH_LONG).show();
                        startActivity(new Intent(getApplicationContext(),com.example.project.ui.send.products_show.class));
                    }
                    else{
                        //response.code());


                    }
                }

                @Override
                public void onFailure(Call<response_here> call, Throwable t) {

                }
            });


            //longitude + "\n" + latitude);

        } catch (Exception e) {
            e.printStackTrace();
            //e.getMessage());
        }




    }

    public void sellerback(View view) {
        startActivity(new Intent(this,com.example.project.profile.class));
    }
}
