package com.example.project;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class Intro_Activity extends AppCompatActivity {
    Shared_pref shared_pref = new Shared_pref(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro_);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if(!shared_pref.get_Address().equals("earth")){
            Intent intent = new Intent(getApplicationContext(),com.example.project.ui.welcome.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }
    }

    public void start(View view) {
        startActivity(new Intent(getApplicationContext(),com.example.project.Register.class));
    }
}
