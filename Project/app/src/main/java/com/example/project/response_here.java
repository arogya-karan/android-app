package com.example.project;

import com.google.gson.annotations.SerializedName;

public class response_here {

   private boolean success;
   private String token;
   @SerializedName("msg")
   private  String msg;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getMessage() {
        return msg;
    }

    public void setMessage(String message) {
        this.msg = message;
    }
}
