package com.example.project.ui.home;

import retrofit2.http.Body;

public class transac_class {

    private String pid;
    private String uid;
    private String quantity;
    private String dateOfRequest;
    private Boolean isFinalised;

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getDateofRequest() {
        return dateOfRequest;
    }

    public void setDateofRequest(String dateofRequest) {
        this.dateOfRequest = dateofRequest;
    }

    public Boolean getFinalised() {
        return isFinalised;
    }

    public void setFinalised(Boolean finalised) {
        isFinalised = finalised;
    }
}
