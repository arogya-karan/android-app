package com.example.project.ui.send;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.project.R;
import com.example.project.Shared_pref;
import com.example.project.api_header;
import com.example.project.response_here;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SendFragment extends Fragment implements  recycler_adapter_products.OnNoteList2 {

    private SendViewModel sendViewModel;
    FloatingActionButton floatingActionButton;
    private View mProgressView;
    private View mView;
    RecyclerView recyclerView;
    TextView tv_empty;
    ArrayList<String> pid;
    String code;
    ArrayList<String> productCode;
    recycler_adapter_products adapter;

    ArrayList<products_class> modelRecyclerArrayList;
    //FloatingActionButton floatingActionButton;
    //RecyclerView recyclerView;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        sendViewModel =
                ViewModelProviders.of(this).get(SendViewModel.class);
        View root = inflater.inflate(R.layout.fragment_send, container, false);

        sendViewModel.getText().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {

            }
        });

        startActivity(new Intent(getContext(),products_show.class));

        ////////////////////////////
        tv_empty = root.findViewById(R.id.tv_empty);

        mProgressView = root.findViewById(R.id.product_show_progress);
        pid = new ArrayList<>();
        mView = root.findViewById(R.id.product_show_view);
        recyclerView = root.findViewById(R.id.recycler_view_products);
        tv_empty.setVisibility(View.GONE);

        productCode = new ArrayList<>();
        showProgress(true);

        //get_all_products();

        floatingActionButton = root.findViewById(R.id.fab_add_products);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showProgress(false);


                startActivity(new Intent(getContext(),com.example.project.product.class));
            }
        });


        //recyclerView = root.findViewById(R.id.recycler_view_products);
       // startActivity(new Intent(getContext(),products_show.class));
        //get_all_products();
        //floatingActionButton = root.findViewById(R.id.fab_add_products);
        /*floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                startActivity(new Intent(getContext(),com.example.project.product.class));
            }
        });*/



        return root;
    }

    private void get_all_products(){
        final Shared_pref shared_pref =  new Shared_pref(getContext());
        final String token = shared_pref.get_token();

        //token);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request newRequest  = chain.request().newBuilder()
                        .addHeader("Authorization", "Bearer " + token)
                        .build();
                return chain.proceed(newRequest);
            }
        }).build();
        Retrofit retrofit  = new Retrofit.Builder()
                .baseUrl("https://covid-19-info-123.herokuapp.com/")
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        api_header api = retrofit.create(api_header.class);

        Call<ResponseBody> call = api.get_seller_products();
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                showProgress(false);
                if (response.code() == 200) {

                    //response.body());
                    //String jsonresponse = response.body();
                    try {

                        if (response.body() == null) {
                            tv_empty.setVisibility(View.VISIBLE);

                        }
                        else{
                            tv_empty.setVisibility(View.GONE);
                            writeRecycler(response.body().string());
                        }


                    } catch (IOException e) {
                        e.printStackTrace();

                    }

                }
                else{
                    //response.code());
                    Toast.makeText(getContext(),response.message(),Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                //"fail : " + t.getMessage());
                showProgress(false);

            }
        });


    }

    private void writeRecycler(String response) {
        try {
            //getting the whole json object from the response
            JSONObject obj = new JSONObject(response);


            modelRecyclerArrayList = new ArrayList<>();
            JSONArray dataArray  = obj.getJSONArray("products");

            for (int i = 0; i < dataArray.length(); i++) {

                products_class modelRecycler = new products_class();
                JSONObject dataobj = dataArray.getJSONObject(i);

                modelRecycler.setImageThumbnail(dataobj.getString("imageThumbnail"));
                modelRecycler.setProductName(dataobj.getString("productName"));
                modelRecycler.setProductDesc(dataobj.getString("productDesc"));
                modelRecycler.setProductType(dataobj.getString("productType"));
                modelRecycler.setAvailable(dataobj.getString("availablity"));
                modelRecycler.setCertified(dataobj.getString("isCertified"));
                modelRecycler.setQuantity(dataobj.getString("quantity"));
                modelRecycler.setProductPrice(dataobj.getString("productPrice"));
                pid.add(dataobj.getString("_id"));

                productCode.add(dataobj.getString("productCode"));



                modelRecyclerArrayList.add(modelRecycler);

            }

            if(modelRecyclerArrayList.isEmpty()){
                tv_empty.setVisibility(View.VISIBLE);
            }
            else{
                tv_empty.setVisibility(View.GONE);
            }


            adapter = new recycler_adapter_products(getContext(),modelRecyclerArrayList,this);
            recyclerView.setAdapter(adapter);
            recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));




        } catch (JSONException e) {
            e.printStackTrace();
            //"error" + e.getMessage());
        }



    }



    @Override
    public void OnnoteClick2(int position) {
        showProgress(false);
        Toast.makeText(getContext(),productCode.get(position),Toast.LENGTH_LONG).show();
        //"clicked");
        // Toast.makeText(view_list.this,modelRecyclerArrayList.get(position).getCountry_name().toString(),Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(getContext(),com.example.project.product.class);
        intent.putExtra("click" , productCode.get(position));
        intent.putExtra("pid",pid.get(position));

        startActivity(intent);


    }

    public void productsShow(View view) {
        startActivity(new Intent(getContext(),com.example.project.profile.class));
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mView.setVisibility(show ? View.GONE : View.VISIBLE);
            mView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });

        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            // tvLoad.setVisibility(show ? View.VISIBLE : View.GONE);
            mView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }
}