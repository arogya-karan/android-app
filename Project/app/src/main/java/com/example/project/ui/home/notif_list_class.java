package com.example.project.ui.home;

public class notif_list_class {

    private String _id;
    private String quantity;
    private String pid;
    private String uid;
    private String dateofRequest;


    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getDateofRequest() {
        return dateofRequest;
    }

    public void setDateofRequest(String dateofRequest) {
        this.dateofRequest = dateofRequest;
    }
}
