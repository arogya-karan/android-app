package com.example.project.ui.home;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.project.R;
import com.example.project.Shared_pref;
import com.example.project.api_header;
import com.example.project.ui.send.recycler_adapter_products;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class notification_list extends AppCompatActivity implements notification_adapter.OnNoteList {
    RecyclerView recyclerView;
    TextView tv_empty;
    ArrayList<product_class> arrayList;
    notification_adapter adapter;
    ArrayList<ids_class> arrayList_uid;
    private View mProgressView;
    private View mView;
    ArrayList<notif_list_class> arrayList_notif;







    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification_list);


        recyclerView = findViewById(R.id.rv);
        arrayList = new ArrayList<>();
        tv_empty = findViewById(R.id.tv_empty);
        tv_empty.setVisibility(View.GONE);
        mProgressView = findViewById(R.id.nl_progress);
        mView = findViewById(R.id.nl_rl);

        showProgress(true);


        getJ();


       /* if(getIntent()!=null){
            onNewIntent(getIntent());

        }*/


        }

        private void getJ(){

            Shared_pref shared_pref = new Shared_pref(getApplicationContext());
            final String token = shared_pref.get_token();
            OkHttpClient client = new OkHttpClient.Builder().addInterceptor(new Interceptor() {
                @Override
                public okhttp3.Response intercept(Chain chain) throws IOException {
                    Request newRequest = chain.request().newBuilder()
                            .addHeader("Authorization", " Bearer " + token)
                            .build();
                    return chain.proceed(newRequest);
                }
            }).build();
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl("https://covid-19-info-123.herokuapp.com/")
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            final api_header api = retrofit.create(api_header.class);

            Call<ResponseBody> call = api.notif_list();
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    showProgress(false);
                    try {

                        String res = null;
                        if (response.body() == null) {
                            tv_empty.setVisibility(View.VISIBLE);

                        } else {
                            tv_empty.setVisibility(View.GONE);

                            res = response.body().string();
                            JSONObject object = new JSONObject(res);
                            JSONArray array = object.getJSONArray("requested");


                            arrayList_notif = new ArrayList<notif_list_class>();

                            for (int i = 0; i < array.length(); i++) {
                                notif_list_class notif_list_class = new notif_list_class();
                                JSONObject dataobj = array.getJSONObject(i);

                                notif_list_class.set_id(dataobj.getString("_id"));
                                notif_list_class.setDateofRequest(dataobj.getString("dateOfRequest"));
                                notif_list_class.setPid(dataobj.getString("pid"));
                                notif_list_class.setQuantity(dataobj.getString("quantity"));
                                notif_list_class.setUid(dataobj.getString("uid"));

                                arrayList_notif.add(notif_list_class);


                            }
                            for (int j = 0; j < arrayList_notif.size(); j++) {
                                String pid = arrayList_notif.get(j).getPid();
                                final String quant = arrayList_notif.get(j).getQuantity();
                                final String date = arrayList_notif.get(j).getDateofRequest();
                                final product_class product_class = new product_class();
                                final String uid = arrayList_notif.get(j).getUid();

                                /////////////////////////////////////////////////
                                Call<ResponseBody> call1 = api.get_user(uid);
                                call1.enqueue(new Callback<ResponseBody>() {
                                    @Override
                                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                        String hello = null;
                                        try {
                                            assert response.body() != null;
                                            hello = response.body().string();

                                            //"hello " + hello);
                                            JSONObject object = new JSONObject(hello).getJSONObject("user");
                                            product_class.setBuyer_name(object.getString("name"));
                                            product_class.setBuyer_email(object.getString("email"));

                                        } catch (JSONException | IOException e) {
                                            e.printStackTrace();
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<ResponseBody> call, Throwable t) {

                                    }
                                });

                                ////////////////////////////////////////////////

                                Call<ResponseBody> call2 = api.get_products_by_product(pid);
                                call2.enqueue(new Callback<ResponseBody>() {
                                    @Override
                                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                        showProgress(false);
                                        String hello = null;
                                        try {
                                            assert response.body() != null;
                                            hello = response.body().string();

                                            //"hello " + hello);
                                            JSONObject object = new JSONObject(hello);

                                            JSONObject obj = object.getJSONObject("product");
                                            product_class.setProductName(obj.getString("productName"));
                                            product_class.setProductPrice(obj.getString("productPrice"));
                                            product_class.setProductTypes(obj.getString("productType"));

                                            product_class.setImgThumbnail(obj.getString("imageThumbnail"));
                                            product_class.setBuyer_quantity(quant);
                                            product_class.setDate(getDate(Long.parseLong(date)));
                                            // product_class.setBuyer_name(uid);
                                            arrayList.add(product_class);


                                        } catch (IOException | JSONException e) {
                                            e.printStackTrace();
                                            //e.getMessage());
                                        }

                                        //"arraylist : " + arrayList);

                                        adapter = new notification_adapter(getApplicationContext(), arrayList, notification_list.this);
                                        recyclerView.setAdapter(adapter);
                                        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));


                                    }

                                    @Override
                                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                                        //"error " + t.getMessage());

                                    }
                                });
                            }


                        }
                    }catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {

                }
            });



        }
    private String getDate(long timeStamp){

        try{
            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
            Date netDate = (new Date(timeStamp));
            return sdf.format(netDate);
        }
        catch(Exception ex){
            return "xx";
        }
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mView.setVisibility(show ? View.GONE : View.VISIBLE);
            mView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });

        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            // tvLoad.setVisibility(show ? View.VISIBLE : View.GONE);
            mView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }








   /* @Override
    public void onNewIntent(Intent intent) {
        final product_class product_class = new product_class();
        arrayList = new ArrayList<product_class>();
        arrayList_uid = new ArrayList<ids_class>();
        Bundle extras = intent.getExtras();
        if(extras != null) {
            final String uid = extras.getString("uid");
            String pid = extras.getString("pid");
            final String quantity = extras.getString("quantity");
            String sid = extras.getString("sid");
            final String date = extras.getString("date");

            final ids_class ids_class = new ids_class();
            ids_class.setPid(pid);
            ids_class.setQuantity(quantity);
            ids_class.setUid(uid);
            ids_class.setSid(sid);
            ids_class.setDate(date);


            if (uid != null) {


                Shared_pref shared_pref = new Shared_pref(getApplicationContext());
                final String token = shared_pref.get_token();
                OkHttpClient client = new OkHttpClient.Builder().addInterceptor(new Interceptor() {
                    @Override
                    public okhttp3.Response intercept(Chain chain) throws IOException {
                        Request newRequest = chain.request().newBuilder()
                                .addHeader("Authorization", " Bearer " + token)
                                .build();
                        return chain.proceed(newRequest);
                    }
                }).build();
                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl("https://covid-19-info-123.herokuapp.com/")
                        .client(client)
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();

                final api_header api = retrofit.create(api_header.class);

                Call<ResponseBody> call = api.get_products_by_product(pid);
                call.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        String hello = null;
                        try {
                            assert response.body() != null;
                            hello = response.body().string();

                            //"hello " + hello);
                            JSONObject object = new JSONObject(hello);

                            JSONObject obj = object.getJSONObject("product");
                            product_class.setProductName(obj.getString("productName"));
                            product_class.setProductPrice(obj.getString("productPrice"));
                            product_class.setProductTypes(obj.getString("productType"));

                            product_class.setImgThumbnail(obj.getString("imageThumbnail"));
                            product_class.setBuyer_quantity(quantity);
                            product_class.setDate(date);

                            Call<ResponseBody> call1 = api.get_user(uid);
                            call1.enqueue(new Callback<ResponseBody>() {
                                @Override
                                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                    try {
                                        assert response.body() != null;
                                        String user = response.body().string();
                                        //user);
                                        JSONObject obj = new JSONObject(user);
                                        JSONObject object1 = obj.getJSONObject("user");
                                        product_class.setBuyer_name(object1.getString("name"));

                                        arrayList_uid.add(ids_class);
                                        //"array : " + arrayList_uid.get(0).getPid());
                                         arrayList.add(product_class);
                                        //"array2 : " + arrayList);

                                        adapter = new notification_adapter(getApplicationContext(), arrayList, notification_list.this);
                                        recyclerView.setAdapter(adapter);
                                        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));




                                    } catch (IOException | JSONException e) {
                                        e.printStackTrace();
                                        //e.getMessage());
                                    }

                                }

                                @Override
                                public void onFailure(Call<ResponseBody> call, Throwable t) {

                                }
                            });





                        } catch (IOException | JSONException e) {
                            e.printStackTrace();
                            //e.getMessage());
                        }

                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        //"error " + t.getMessage());

                    }
                });




            }


        }
        }
*/


    public void back_to_product_show(View view) {
        startActivity(new Intent(this,com.example.project.profile.class));
    }

    @Override
    public void OnnoteClick(int position) {

        Toast.makeText(getApplicationContext(),""+position,Toast.LENGTH_LONG).show();
        Intent intent = new Intent(notification_list.this,request.class);
        intent.putExtra("buyer_name",arrayList.get(position).getBuyer_name());
        //"buyer : " + arrayList.get(position).getBuyer_name());
        intent.putExtra("buyer_quantity",arrayList.get(position).getBuyer_quantity());
        intent.putExtra("date",arrayList.get(position).getDate());
        intent.putExtra("product_name",arrayList.get(position).getProductName());
        intent.putExtra("product_type",arrayList.get(position).getProductTypes());
        intent.putExtra("product_price",arrayList.get(position).getProductPrice());
        intent.putExtra("buyer_email",arrayList.get(position).getBuyer_email());


        intent.putExtra("img",arrayList.get(position).getImgThumbnail());

        intent.putExtra("date_request",arrayList_notif.get(position).getDateofRequest());
        intent.putExtra("quantity",arrayList_notif.get(position).getQuantity());
        intent.putExtra("pid",arrayList_notif.get(position).getPid());
        intent.putExtra("uid",arrayList_notif.get(position).getUid());


        startActivity(intent);


    }
}
