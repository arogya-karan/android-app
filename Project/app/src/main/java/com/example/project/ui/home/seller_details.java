package com.example.project.ui.home;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.project.R;
import com.example.project.Shared_pref;
import com.example.project.api_header;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class seller_details extends AppCompatActivity {
    String seller_id;
    TextView tv_sname,tv_id, tv_email, tv_number, tv_type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seller_details);



        if(getIntent().getStringExtra("seller")!=null){
            seller_id = getIntent().getStringExtra("seller");

        }
        tv_sname = findViewById(R.id.sname);
        tv_email = findViewById(R.id.semail);
        tv_id = findViewById(R.id.sid);
        tv_number = findViewById(R.id.sphone);
        tv_type = findViewById(R.id.sbusiness);




        getSeller();
    }

    private void getSeller() {

        Shared_pref shared_pref = new Shared_pref(getApplicationContext());
        final String token = shared_pref.get_token();
        //token);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request newRequest  = chain.request().newBuilder()
                        .addHeader("Authorization", " Bearer " + token)
                        .build();
                return chain.proceed(newRequest);
            }
        }).build();
        Retrofit retrofit  = new Retrofit.Builder()
                .baseUrl("https://covid-19-info-123.herokuapp.com/")
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        api_header api = retrofit.create(api_header.class);

        Call<ResponseBody> call = api.get_seller_from_id(seller_id);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {


                String resp = null;
                try {
                    if(response.code() == 200) {
                        assert response.body() != null;
                        resp = response.body().string();
                        JSONObject object = new JSONObject(resp).getJSONObject("seller");
                        tv_sname.setText(object.getString("sellerName"));
                        tv_email.setText(object.getString("sellerEmail"));
                        tv_id.setText(object.getString("_id"));
                        tv_number.setText(object.getString("sellerPhone"));
                        if(object.getBoolean("isBusiness")){
                            tv_type.setText("Business / Organisation");
                        }
                        else{
                            tv_type.setText("Volunteer / Individual");
                        }

                    }
                    else{
                        Toast.makeText(getApplicationContext(),"error : " + response.message(),Toast.LENGTH_SHORT).show();
                    }
                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(),"error : " + e.getMessage(),Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(getApplicationContext(),"error : " + t.getMessage(),Toast.LENGTH_SHORT).show();

            }
        });
    }

    public void back(View view) {
        startActivity(new Intent(getApplicationContext(),com.example.project.profile.class));
    }

    public void getproducts(View view) {
        Intent intent = new Intent(getApplicationContext(),com.example.project.ui.home.products_of_one_seller.class);
        intent.putExtra("seller",seller_id);
        startActivity(intent);
    }
}
