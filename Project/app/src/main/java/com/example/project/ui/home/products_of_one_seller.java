package com.example.project.ui.home;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.project.R;
import com.example.project.Shared_pref;
import com.example.project.api_header;
import com.example.project.cases;
import com.example.project.ui.send.products_class;
import com.example.project.ui.send.recycler_adapter_products;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class products_of_one_seller extends AppCompatActivity  implements recycler_adapter_prod_by_seller.OnNoteList {

    RecyclerView recyclerView;
    String code;
   ArrayList<String> productid;
   TextView  tv_empty;
   View mview;
   RadioButton rb;

   RadioGroup rg1,rg2;

   EditText etsearch;
    recycler_adapter_prod_by_seller adapter;

    ArrayList<sel> modelRecyclerArrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_products_of_one_seller);
        recyclerView = findViewById(R.id.recycler_view_seller);
        productid = new ArrayList<>();
        tv_empty = findViewById(R.id.tv_empty);
        //filter = findViewById(R.id.filter);




        rg1 = findViewById(R.id.rg1);


        etsearch = findViewById(R.id.et_search2);




        tv_empty.setVisibility(View.GONE);


        getProducts();



    }
    private void getfilter(){

            ArrayList<String> text = getIntent().getStringArrayListExtra("array");
        assert text != null;
        etsearch.setText(text+"");

            filter2(text);



    }
    private void filter2(ArrayList<String> arrayList){

        ArrayList<sel> filteredlist = new ArrayList<sel>();
        //modelRecyclerArrayList);

        for(String s1 : arrayList) {
            //s1);



            for (sel sel : modelRecyclerArrayList) {
                if (sel.getProductType().toLowerCase().contains(s1.toLowerCase()) || sel.getProductType().toLowerCase().equals(s1.toLowerCase())) {
                    filteredlist.add(sel);
                }


            }
        }

            adapter.filteredlist(filteredlist);




    }

    private void getProducts() {

        String seller = getIntent().getStringExtra("seller");


        Shared_pref shared_pref = new Shared_pref(getApplicationContext());
        final String token = shared_pref.get_token();
        //token);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request newRequest  = chain.request().newBuilder()
                        .addHeader("Authorization", " Bearer " + token)
                        .build();
                return chain.proceed(newRequest);
            }
        }).build();
        Retrofit retrofit  = new Retrofit.Builder()
                .baseUrl("https://covid-19-info-123.herokuapp.com/")
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        api_header api = retrofit.create(api_header.class);

        Call<ResponseBody> call = api.get_products_by_seller(seller);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {

                    String hello = null;
                    if (response.body() == null) {
                        tv_empty.setVisibility(View.VISIBLE);

                    } else {
                        tv_empty.setVisibility(View.GONE);
                        hello = response.body().string();


                        //hello);
                        writeRecycler(hello);
                    }
                }catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(getApplicationContext(),t.getMessage(),Toast.LENGTH_SHORT).show();

            }
        });
    }
    private void writeRecycler(String response) {
        try {
            //getting the whole json object from the response
            JSONObject obj = new JSONObject(response);


            modelRecyclerArrayList = new ArrayList<>();
            JSONArray dataArray  = obj.getJSONArray("products");

            for (int i = 0; i < dataArray.length(); i++) {

                sel modelRecycler = new sel();
                JSONObject dataobj = dataArray.getJSONObject(i);

                modelRecycler.setImageThumbnail(dataobj.getString("imageThumbnail"));
                modelRecycler.setProductName(dataobj.getString("productName"));
                modelRecycler.setProductDesc(dataobj.getString("productDesc"));
                modelRecycler.setProductType(dataobj.getString("productType"));
                modelRecycler.setAvailablity(dataobj.getString("availablity"));
                modelRecycler.setIsCertified(dataobj.getString("isCertified"));
                modelRecycler.set_id(dataobj.getString("_id"));

              //  modelRecycler.setQuantity(dataobj.getString("quantity"));
                modelRecycler.setProductPrice(dataobj.getString("productPrice"));
                productid.add(dataobj.getString("_id"));

              //  productCode.add(dataobj.getString("productCode"));



                modelRecyclerArrayList.add(modelRecycler);

            }

            adapter = new recycler_adapter_prod_by_seller(this,modelRecyclerArrayList,this);
            recyclerView.setAdapter(adapter);
            recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));

            if(getIntent().getStringArrayListExtra("array")!=null){
                etsearch.setVisibility(View.VISIBLE);
                // filter.setVisibility(View.VISIBLE);

                getfilter();
            }
            else {
                search2();
            }



        } catch (JSONException e) {
            e.printStackTrace();
            //"error" + e.getMessage());
        }



    }


    public void back_to_product_show(View view) {
        if(getIntent().getStringExtra("text")!=null){
            String seller = getIntent().getStringExtra("seller");
            Intent i = new Intent(this,com.example.project.ui.home.products_of_one_seller.class);
            i.putExtra("seller",seller);
            startActivity(i);
        }
        else {
            startActivity(new Intent(this,com.example.project.profile.class));
        }

    }


    public void search2() {
       // filter.setVisibility(View.VISIBLE);

        etsearch.setVisibility(View.VISIBLE);
       // tvwelcome.setVisibility(View.GONE);
       // tvdesc.setVisibility(View.GONE);

        etsearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {


                    filter(s.toString());



            }
        });


    }
    private void filter(String s){

        ArrayList<sel> filteredlist = new ArrayList<sel>();
        //modelRecyclerArrayList);



        for(sel sel : modelRecyclerArrayList){
            if(sel.getProductType().toLowerCase().contains(s.toLowerCase()) || sel.getProductType().toLowerCase().equals(s.toLowerCase())){
                filteredlist.add(sel);
            }


        }

            adapter.filteredlist(filteredlist);



    }


    @Override
    public void OnnoteClick(sel sel) {

        String id = getIntent().getStringExtra("seller");
        Toast.makeText(this,"click",Toast.LENGTH_LONG).show();
        Intent intent = new Intent(this,product_details.class);
        intent.putExtra("id",sel.get_id());
        //sel.get_id());
        intent.putExtra("seller",id);

        startActivity(intent);



    }



    public void filt(View view) {
        String id = getIntent().getStringExtra("seller");
        Intent intent = new Intent(getApplicationContext(), com.example.project.ui.home.filter.class);
        intent.putExtra("seller",id);

        startActivity(intent);

    }
}
