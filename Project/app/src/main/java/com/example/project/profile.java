package com.example.project;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Bundle;

import com.example.project.ui.home.YourService;
import com.example.project.ui.home.product_details;
import com.example.project.ui.slideshow.SlideshowFragment;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.google.android.material.navigation.NavigationView;
import com.pusher.client.Pusher;
import com.pusher.client.PusherOptions;
import com.pusher.client.channel.Channel;
import com.pusher.client.channel.ChannelEventListener;
import com.pusher.client.channel.PusherEvent;
import com.pusher.client.connection.ConnectionEventListener;
import com.pusher.client.connection.ConnectionState;
import com.pusher.client.connection.ConnectionStateChange;

import androidx.drawerlayout.widget.DrawerLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.Menu;
import android.widget.Toast;

import org.json.JSONObject;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class profile extends AppCompatActivity {

    private AppBarConfiguration mAppBarConfiguration;
    NavigationView navigationView;
    Intent mServiceIntent;
    public YourService mYourService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mYourService = new YourService();
        mServiceIntent = new Intent(this, mYourService.getClass());

        navigationView = findViewById(R.id.nav_view);
        final Shared_pref shared_pref = new Shared_pref(this);

        if(shared_pref.get_Address().equals("earth")){
            Menu nav_Menu = navigationView.getMenu();
            nav_Menu.findItem(R.id.nav_login).setVisible(true);

        }
        else{
            Menu nav_Menu = navigationView.getMenu();
            nav_Menu.findItem(R.id.nav_login).setVisible(false);

        }


        if(shared_pref.get_flag() == 1 ){ //seller

            Menu nav_Menu = navigationView.getMenu();
            nav_Menu.findItem(R.id.nav_send).setVisible(true);
            nav_Menu.findItem(R.id.nav_share).setVisible(false);
            nav_Menu.findItem(R.id.nav_tools).setVisible(true);
            nav_Menu.findItem(R.id.requests_handled).setVisible(true);
            nav_Menu.findItem(R.id.action_settings).setVisible(true);

        }
        else{
            Menu nav_Menu = navigationView.getMenu();
            nav_Menu.findItem(R.id.nav_send).setVisible(false);
            nav_Menu.findItem(R.id.nav_share).setVisible(true);
            nav_Menu.findItem(R.id.nav_tools).setVisible(true);
            nav_Menu.findItem(R.id.requests_handled).setVisible(false);
            nav_Menu.findItem(R.id.action_settings).setVisible(false);


        }




      /*  Uri uri = getIntent().getData();
        if(uri != null){
            List<String> params = uri.getPathSegments();
            String id = params.get(params.size()-1);

           // Toast.makeText(this,"id : " + id,Toast.LENGTH_SHORT).show();
            //"ehllo");

            try {
                JWUtils.decodeJWT(id);


            } catch (Exception e) {
                e.printStackTrace();
                //"mail : : " +e.getMessage());
            }

            //// SHARED PREF

            shared_pref.add_token(id);
            //Toast.makeText(this,"token : done ",Toast.LENGTH_LONG).show();


        }
        else{
           // Toast.makeText(this,"id :  error",Toast.LENGTH_SHORT).show();
        }*/
        //FloatingActionButton fab = findViewById(R.id.fab);

        /*fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/
        final DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_home, R.id.nav_gallery, R.id.nav_slideshow,
                R.id.nav_tools, R.id.nav_share, R.id.nav_send, R.id.nav_login)
                .setDrawerLayout(drawer)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);


     /* navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

                if(menuItem.getItemId() == R.id.nav_login){
                    menuItem.setChecked(true);
                    drawer.closeDrawers();
                    startActivity(new Intent(getApplicationContext(),com.example.project.Register.class));
                    return true;
                }

        });*/

     navigationView.getMenu().findItem(R.id.nav_login).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
         @Override
         public boolean onMenuItemClick(MenuItem item) {
             startActivity(new Intent(getApplicationContext(),com.example.project.Register.class));
             return false;
         }
     });
     navigationView.getMenu().findItem(R.id.action_settings).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                startActivity(new Intent(getApplicationContext(),com.example.project.ui.home.notification_list.class));
                return false;
            }
        });
     navigationView.getMenu().findItem(R.id.requests_handled).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                startActivity(new Intent(getApplicationContext(),com.example.project.ui.home.handled_requests.class));
                return false;
            }
        });

     navigationView.getMenu().findItem(R.id.home_intro).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
         @Override
         public boolean onMenuItemClick(MenuItem item) {
             startActivity(new Intent(getApplicationContext(),com.example.project.ui.welcome.class));
             return false;
         }
     });

     navigationView.getMenu().findItem(R.id.nav_hospitals).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
         @Override
         public boolean onMenuItemClick(MenuItem item) {
             Shared_pref shared_pref1 = new Shared_pref(getApplicationContext());
             String add = shared_pref1.get_Address();
             Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
             List<Address> addresses = null;
             try {
                 addresses = geocoder.getFromLocationName(add, 1);
                 Address address2 = addresses.get(0);
                 double longitude = address2.getLongitude();
                 double latitude = address2.getLatitude();

                 Uri gmm = Uri.parse("geo:"+latitude+","+longitude+"?q=near by hospitals");
                 Intent mapIntent = new Intent(Intent.ACTION_VIEW,gmm);
                 mapIntent.setPackage("com.google.android.apps.maps");
                 startActivity(mapIntent);

                 //startActivity(new Intent(getApplicationContext(), com.example.project.ui.gallery.MapsActivity2.class));

             } catch (IOException e) {
                 e.printStackTrace();
             }
             return false;
         }
     });
     navigationView.getMenu().findItem(R.id.nav_bb).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
         @Override
         public boolean onMenuItemClick(MenuItem item) {

             Shared_pref shared_pref1 = new Shared_pref(getApplicationContext());
             String add = shared_pref1.get_Address();
             Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
             List<Address> addresses = null;
             try {
                 addresses = geocoder.getFromLocationName(add, 1);
                 Address address2 = addresses.get(0);
                 double longitude = address2.getLongitude();
                 double latitude = address2.getLatitude();

                 Uri gmm = Uri.parse("geo:"+latitude+","+longitude+"?q=near by blood bank");
                 Intent mapIntent = new Intent(Intent.ACTION_VIEW,gmm);
                 mapIntent.setPackage("com.google.android.apps.maps");
                 startActivity(mapIntent);

                 //startActivity(new Intent(getApplicationContext(), com.example.project.ui.gallery.MapsActivity2.class));

             } catch (IOException e) {
                 e.printStackTrace();
             }
             return false;
         }
         });

     navigationView.getMenu().findItem(R.id.nav_labs).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
         @Override
         public boolean onMenuItemClick(MenuItem item) {
             /*startActivity(new Intent(getApplicationContext(),com.example.project.ui.gallery.MapsActivity3.class));
             return false;*/
             Shared_pref shared_pref1 = new Shared_pref(getApplicationContext());
             String add = shared_pref1.get_Address();
             Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
             List<Address> addresses = null;
             try {
                 addresses = geocoder.getFromLocationName(add, 1);
                 Address address2 = addresses.get(0);
                 double longitude = address2.getLongitude();
                 double latitude = address2.getLatitude();

                 Uri gmm = Uri.parse("geo:"+latitude+","+longitude+"?q=near by clinical labs");
                 Intent mapIntent = new Intent(Intent.ACTION_VIEW,gmm);
                 mapIntent.setPackage("com.google.android.apps.maps");
                 startActivity(mapIntent);

                 //startActivity(new Intent(getApplicationContext(), com.example.project.ui.gallery.MapsActivity2.class));

             } catch (IOException e) {
                 e.printStackTrace();
             }
             return false;
         }
     });

     navigationView.getMenu().findItem(R.id.nav_info).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
         @Override
         public boolean onMenuItemClick(MenuItem item) {
             startActivity(new Intent(getApplicationContext(),com.example.project.Covid_info.class));
             return false;
         }
     });


     push();



    }
    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                Log.i ("Service status", "Running");
                return true;
            }
        }
        Log.i ("Service status", "Not running");
        return false;






        //launchservice();

    }
    @Override
    protected void onDestroy() {
        stopService(mServiceIntent);
        super.onDestroy();
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.profile, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        /*if (id == R.id.action_settings) {

            Toast.makeText(profile.this, item.getTitle().toString(), Toast.LENGTH_SHORT).show();
            startActivity(new Intent(getApplicationContext(),com.example.project.ui.home.notification_list.class));
        }*/
        if(id == R.id.action_active_requests){
            Toast.makeText(profile.this, item.getTitle().toString(),Toast.LENGTH_SHORT).show();
            startActivity(new Intent(getApplicationContext(),com.example.project.ui.home.active_requests.class));
        }
        else if(id == R.id.action_previously_bought){
            Toast.makeText(profile.this, item.getTitle().toString(),Toast.LENGTH_SHORT).show();
            startActivity(new Intent(getApplicationContext(),com.example.project.ui.home.previously_bought.class));
        }
       /* else if(id == R.id.requests_handled){
            Toast.makeText(profile.this, item.getTitle().toString(),Toast.LENGTH_SHORT).show();
            startActivity(new Intent(getApplicationContext(),com.example.project.ui.home.handled_requests.class));

        }*/
        return super.onOptionsItemSelected(item);

    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }

    public void push() {
        String cluster = getString(R.string.pusher_cluster);
        String key = getString(R.string.pusher_key);
        String channel_name = getString(R.string.channel_name);
        String event_name = getString(R.string.event_name);
        PusherOptions options = new PusherOptions().setCluster(cluster);

        Pusher pusher = new Pusher(key, options);
        pusher.connect(new ConnectionEventListener() {
            @Override
            public void onConnectionStateChange(ConnectionStateChange change) {



            }

            @Override
            public void onError(String message, String code, Exception e) {
                //"There was a problem connecting! : " + message);
            }
        }, ConnectionState.ALL);

        final Channel channel = pusher.subscribe(channel_name, new ChannelEventListener() {         //sos

            @Override
            public void onSubscriptionSucceeded(String channelName) {
                //"Subscribed!");
                //channelName);




            }

            @Override
            public void onEvent(PusherEvent event) { //notification

                //"event" + event.getData());
                try {

                    Shared_pref shared_pref = new Shared_pref(profile.this);
                    JSONObject obj = new JSONObject(event.getData());
                    String sid = obj.getString("sid");
                    String uid = obj.getString("uid");
                    String quantity = obj.getString("quantity");
                    String pid = obj.getString("pid");
                    String user =  JWUtils.decodeJWT(shared_pref.get_token());
                    JSONObject obj2 = new JSONObject(user);
                    //obj2);
                    JSONObject object = obj2.getJSONObject("user");
                    String use = object.getString("_id");

                    //"_id" +use);
                    //"sid " + sid);

                    if(use.equals(sid)){

                        if (!isMyServiceRunning(mYourService.getClass())) {
                            startService(mServiceIntent);
                        }


                        NotificationManagerCompat notificationManagerCompat;
                        notificationManagerCompat = NotificationManagerCompat.from(profile.this);
                        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                            NotificationChannel channel1 = new NotificationChannel(
                                    "channel1","channel1", NotificationManager.IMPORTANCE_DEFAULT
                            );

                            channel1.setDescription("Request received!");
                            NotificationManager manager = getSystemService(NotificationManager.class);
                            assert manager != null;
                            manager.createNotificationChannel(channel1);

                        }

                        Context context = getApplicationContext();
                        Date currentTime = Calendar.getInstance().getTime();
                        Intent myIntent = new Intent(context, com.example.project.ui.home.notification_list.class);
                        myIntent.putExtra("quantity", quantity);
                        myIntent.putExtra("uid",uid);
                        myIntent.putExtra("pid",pid);
                        myIntent.putExtra("sid",sid);
                        myIntent.putExtra("date",currentTime.toString());
                        //"intent : " + uid + sid + pid + quantity);
                        myIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        PendingIntent intent = PendingIntent.getActivity(context,1,myIntent,PendingIntent.FLAG_UPDATE_CURRENT);

                        if (!isMyServiceRunning(mYourService.getClass())) {
                            startService(mServiceIntent);
                        }


                        Notification notification = new NotificationCompat.Builder(profile.this,"channel1").setContentTitle("Request").setContentText("request for product : " + pid)

                                .setPriority(4).setCategory(NotificationCompat.CATEGORY_MESSAGE).setDefaults(Notification.DEFAULT_SOUND).setAutoCancel(true)
                                .setContentIntent(intent)
                                .setAutoCancel(true)
                                .setSmallIcon(R.drawable.ic_menu_send)
                                .build();

                        notificationManagerCompat.notify(1,notification);


                    }



                } catch (Exception e) {
                    e.printStackTrace();
                    //e.getMessage());
                }

            }



        },event_name);

    }



}
