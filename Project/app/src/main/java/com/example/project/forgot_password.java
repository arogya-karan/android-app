package com.example.project;

import androidx.appcompat.app.AppCompatActivity;

import android.net.Uri;
import android.os.Bundle;
import android.util.Base64;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class forgot_password extends AppCompatActivity {
    EditText password, confirmed_password;
    String id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        password = findViewById(R.id.new_password);
        confirmed_password = findViewById(R.id.new_password_confirmed);
        Uri uri = getIntent().getData();
        if (uri != null) {
            List<String> params = uri.getPathSegments();
             id = params.get(params.size() - 1);

            Toast.makeText(this, "id : " + id, Toast.LENGTH_SHORT).show();



                   }

        }





        public void change(View view){
            if(isEmpty(password)|| isEmpty(confirmed_password) || (!password.getText().toString().equals(confirmed_password.getText().toString()))){
                Toast.makeText(getApplicationContext(),"Error",Toast.LENGTH_SHORT).show();
            }
            else{
                try {
                    byte[] decode_byte = Base64.decode(id, Base64.URL_SAFE);
                    String mail = new String(decode_byte, StandardCharsets.UTF_8);
                    forgot_email_password forgot_email_password = new forgot_email_password();
                    forgot_email_password.setEmail(mail);
                    forgot_email_password.setPassword(password.getText().toString());

                    Call<response_here> call = retro_class.getInstance_two().getapi2().forg_put(forgot_email_password);
                    call.enqueue(new Callback<response_here>() {
                        @Override
                        public void onResponse(Call<response_here> call, Response<response_here> response) {

                            assert response.body() != null;
                            //response.body().getMessage());
                            //response.body().isSuccess());
                            Toast.makeText(getApplicationContext(),"A link has been sent to your mail" ,Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onFailure(Call<response_here> call, Throwable t) {
                            //"error : " + t.getMessage());
                            Toast.makeText(getApplicationContext(),"error : " +t.getMessage() ,Toast.LENGTH_SHORT).show();

                        }
                    });


                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(this, "error" + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }






        }

    private boolean isEmpty(EditText etText) {

        if(etText.getText().toString().trim().length() == 0){
            etText.setError("Cannot be Empty");

            return true;
        }
        else{
            return false;
        }
    }

}
