package com.example.project.ui.home;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.project.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class active_request_adapter extends RecyclerView.Adapter<active_request_adapter.MyHolder> {
    private Context context;
    private LayoutInflater inflater;
    private ArrayList<active_req_class> modelArrayList;
    private active_request_adapter.OnNoteList onNoteList;



    public active_request_adapter(Context context, ArrayList<active_req_class> modelArrayList, OnNoteList onNoteList) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.modelArrayList = modelArrayList;
        this.onNoteList = onNoteList;
    }

    @NonNull
    @Override
    public active_request_adapter.MyHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.row_layout_active_request, parent, false);
        active_request_adapter.MyHolder holder = new active_request_adapter.MyHolder(view, onNoteList);

        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull active_request_adapter.MyHolder holder, int position) {

        holder.product_name.setText(modelArrayList.get(position).getProductName());
        holder.product_type.setText("Type : " + modelArrayList.get(position).getProductTypes());
        holder.product_price.setText("Price : " + modelArrayList.get(position).getProductPrice());
        holder.date.setText("Date : " + modelArrayList.get(position).getDate());
        holder.seller_name.setText("Seller Name : " + modelArrayList.get(position).getSeller_name());
        holder.seller_quantity.setText("Quanity Ordered : " + modelArrayList.get(position).getSeller_quantity());
        Picasso.with(context).load(modelArrayList.get(position).getImgThumbnail()).into(holder.iv_request);


    }

    @Override
    public int getItemCount() {
        return modelArrayList.size();
    }

    public class MyHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView product_name, product_type, product_price, seller_name, seller_quantity, date;
        ImageView iv_request;
        active_request_adapter.OnNoteList onNoteList;

        public MyHolder(@NonNull View itemView, OnNoteList onNoteList) {
            super(itemView);

            product_name = itemView.findViewById(R.id.product_name_req_act);
            product_type = itemView.findViewById(R.id.product_type_req_act);
            product_price = itemView.findViewById(R.id.product_price_req_act);
            seller_name = itemView.findViewById(R.id.seller_name_request_act);
            seller_quantity = itemView.findViewById(R.id.seller_quantity_req);
            date = itemView.findViewById(R.id.seller_date);
            iv_request = itemView.findViewById(R.id.iv_product_req_act);
            this.onNoteList = onNoteList;
            itemView.setOnClickListener(this);



        }

        @Override
        public void onClick(View v) {
            onNoteList.OnnoteClick(getAdapterPosition());
        }
    }
    public interface OnNoteList {
        void OnnoteClick(int position);


    }
}
