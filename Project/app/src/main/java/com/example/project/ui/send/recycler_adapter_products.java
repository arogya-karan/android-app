package com.example.project.ui.send;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.cloudinary.Transformation;
import com.example.project.R;
import com.example.project.cases;
import com.example.project.recycler_adapter;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class recycler_adapter_products extends RecyclerView.Adapter<recycler_adapter_products.MyViewHolder> {
    private Context context;

    private LayoutInflater inflater;
    private ArrayList<products_class> dataModelArrayList;
    private OnNoteList2 onNoteList;

    public recycler_adapter_products(Context ctx, ArrayList<products_class> dataModelArrayList, OnNoteList2 onNoteList) {
        inflater = LayoutInflater.from(ctx);
        this.dataModelArrayList = dataModelArrayList;
        this.context = ctx;
        this.onNoteList = onNoteList;
    }

    @NonNull
    @Override
    public recycler_adapter_products.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.row_layout_product, parent, false);
        MyViewHolder holder = new MyViewHolder(view,onNoteList);

        return holder;

    }

    @Override
    public void onBindViewHolder(@NonNull recycler_adapter_products.MyViewHolder holder, int position) {

        Picasso.with(context).load(dataModelArrayList.get(position).getImageThumbnail()).into(holder.iv);
        holder.product_type.setText("Type : "+"\t" +dataModelArrayList.get(position).getProductType());
        holder.product_quantity.setText("Quantity : "+"\t" +dataModelArrayList.get(position).getQuantity());
        holder.product_certified.setText("Certified : "+"\t" +dataModelArrayList.get(position).getCertified().toString());
        if(dataModelArrayList.get(position).getProductDesc().length() > 30 ){
            holder.product_desc.setText("Description : "+dataModelArrayList.get(position).getProductDesc().substring(0,9) + ".....");
        }
        else{
            holder.product_desc.setText("Description : "+dataModelArrayList.get(position).getProductDesc());
        }

       // holder.product_desc.setText("Description : "+"\t"+dataModelArrayList.get(position).getProductDesc());
        holder.product_name.setText(dataModelArrayList.get(position).getProductName());
        holder.product_available.setText("Available : " +"\t"+dataModelArrayList.get(position).getAvailable().toString());
        holder.price.setText("Price : "+"\t" + dataModelArrayList.get(position).getProductPrice() + " Rupees Only");


    }

    @Override
    public int getItemCount() {
        return dataModelArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView product_name , product_desc, product_type, product_quantity, product_available, product_certified , price;
        ImageView iv;
        OnNoteList2 onNoteList;

        public MyViewHolder(@NonNull View itemView, OnNoteList2 onNoteList) {
            super(itemView);

            product_name = itemView.findViewById(R.id.product_name);
            product_desc = itemView.findViewById(R.id.product_desc);
            product_available = itemView.findViewById(R.id.available);
            product_certified = itemView.findViewById(R.id.certified);
            product_type = itemView.findViewById(R.id.product_type);
            product_quantity = itemView.findViewById(R.id.quantity);
            price = itemView.findViewById(R.id.price);
            //this.onNoteList = this.onNoteList;
            this.onNoteList = onNoteList;
            itemView.setOnClickListener(this);
            iv = itemView.findViewById(R.id.iv_product);

        }

        @Override
        public void onClick(View v) {
            onNoteList.OnnoteClick2(getLayoutPosition());
        }

    }
    public interface OnNoteList2 {
        void OnnoteClick2(int position);


    }
}
