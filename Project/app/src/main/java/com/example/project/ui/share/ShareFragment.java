package com.example.project.ui.share;

import android.content.DialogInterface;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.project.JWUtils;
import com.example.project.R;
import com.example.project.Shared_pref;
import com.example.project.add_seller_class;
import com.example.project.api_header;
import com.example.project.response_here;

import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ShareFragment extends Fragment {

    private ShareViewModel shareViewModel;
    AlertDialog.Builder builder;


    Button business, volunteer;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        shareViewModel =
                ViewModelProviders.of(this).get(ShareViewModel.class);
        View root = inflater.inflate(R.layout.fragment_share, container, false);

        shareViewModel.getText().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {

            }
        });
        builder = new AlertDialog.Builder(Objects.requireNonNull(getContext()));
        business = root.findViewById(R.id.btn_business);
        volunteer = root.findViewById(R.id.btn_personal);

        Uri uri = Objects.requireNonNull(getActivity()).getIntent().getData();
        if(uri != null){
            List<String> params = uri.getPathSegments();
            String id = params.get(params.size()-1);

           // Toast.makeText(getContext(),"id : " + id,Toast.LENGTH_SHORT).show();
            //"ehllo");

            try {
                JWUtils.decodeJWT(id);


            } catch (Exception e) {
                e.printStackTrace();
                //e.getMessage());
            }

            //// SHARED PREF
            Shared_pref shared_pref = new Shared_pref(getContext());


            shared_pref.clear();

            shared_pref.add_token(id);
           // Toast.makeText(getContext(),"token : done ",Toast.LENGTH_LONG).show();


        }
        else{
            //Toast.makeText(getContext(),"id :  error",Toast.LENGTH_SHORT).show();
        }



        business.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Shared_pref shared_pref = new Shared_pref(getContext());
                if(shared_pref.get_token() == null || shared_pref.get_token().isEmpty() || shared_pref.get_Address().equals("earth")){
                    startActivity(new Intent(getContext(),com.example.project.Register.class));

                }
                else{
                    startActivity(new Intent(getActivity(),com.example.project.business.class));
                }



            }
        });

        volunteer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Shared_pref shared_pref = new Shared_pref(getContext());
                final String registered_address = shared_pref.get_Address();
                //"address : " + registered_address);
                if (registered_address.equals("earth")) {
                    startActivity(new Intent(getContext(), com.example.project.Register.class));
                } else {


                    // builder.setMessage(R.string.dialog_message) .setTitle(R.string.dialog_title);

                    //Setting message manually and performing action on button click
                    builder.setMessage("Do you want your home address to be your seller address ?")
                            .setCancelable(true)
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {


                                    Geocoder geocoder = new Geocoder(getContext(), Locale.getDefault());
                                    List<Address> addresses = null;
                                    try {
                                        addresses = geocoder.getFromLocationName(registered_address, 1);
                                        Address address2 = addresses.get(0);
                                        double longitude = address2.getLongitude();
                                        double latitude = address2.getLatitude();

                                        add_seller_class add_seller_class = new add_seller_class();
                                        add_seller_class.setNewAddress(false);

                                        add_seller_class.setLat(String.valueOf(latitude));
                                        add_seller_class.setLongi(String.valueOf(longitude));
                                        add_seller_class.setBusiness(false);
                                        //shared_pref.get_token());

                                        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(new Interceptor() {
                                            @Override
                                            public okhttp3.Response intercept(Chain chain) throws IOException {
                                                Request newRequest = chain.request().newBuilder()
                                                        .addHeader("Authorization", " Bearer " + shared_pref.get_token())
                                                        .build();
                                                return chain.proceed(newRequest);
                                            }
                                        }).build();
                                        Retrofit retrofit = new Retrofit.Builder()
                                                .baseUrl("https://covid-19-info-123.herokuapp.com/")
                                                .client(client)
                                                .addConverterFactory(GsonConverterFactory.create())
                                                .build();

                                        api_header api = retrofit.create(api_header.class);


                                        Call<com.example.project.response_here> call = api.add_seller_jwt(add_seller_class);
                                        call.enqueue(new Callback<response_here>() {
                                            @Override
                                            public void onResponse(Call<response_here> call, Response<response_here> response) {
                                                if (response.code() == 200) {
                                                    //response.body());

                                                    assert response.body() != null;
                                                    String new_tok = response.body().getToken();
                                                    //"old : "+shared_pref.get_token());
                                                    shared_pref.clear();
                                                    shared_pref.add_token(new_tok);
                                                    //"new : "+shared_pref.get_token());
                                                    if (shared_pref.get_flag() != 1) {
                                                        shared_pref.add_flag(1);
                                                    }
                                                    shared_pref.is_Business(false);
                                                    startActivity(new Intent(getContext(), com.example.project.ui.send.products_show.class));
                                                } else {
                                                    //response.code());
                                                    try {
                                                        assert response.errorBody() != null;
                                                        Toast.makeText(getContext(),response.errorBody().string(),Toast.LENGTH_SHORT).show();
                                                    } catch (IOException e) {
                                                        e.printStackTrace();
                                                    }


                                                }
                                            }

                                            @Override
                                            public void onFailure(Call<response_here> call, Throwable t) {

                                            }
                                        });


                                        //longitude + "\n" + latitude);

                                    } catch (Exception e) {
                                        e.printStackTrace();

                                        //"errorcatche : " + e.getMessage());

                                    }


                                }
                            })
                            .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //  Action for 'NO' Button
                                    dialog.cancel();
                                    shared_pref.is_Business(false);
                                    startActivity(new Intent(getContext(), new_address.class));


                                }
                            });
                    //Creating dialog box
                    AlertDialog alert = builder.create();
                    //Setting the title manually
                    alert.setTitle("Become a Seller");
                    alert.show();
                }
            }





        });

        return root;
    }

    private void hello(){

    }
}