package com.example.project.ui.home;

import com.google.gson.annotations.SerializedName;

public class buy {
    @SerializedName("pid")
    private String productCode;
    @SerializedName("quantity")
    private String quantity;



    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }


    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }
}
