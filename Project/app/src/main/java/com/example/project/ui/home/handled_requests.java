package com.example.project.ui.home;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.project.R;
import com.example.project.Shared_pref;
import com.example.project.api_header;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class handled_requests extends AppCompatActivity implements notification_adapter.OnNoteList{
    RecyclerView rv;
    TextView tv_empty;
    ArrayList<product_class> arrayList;
    private View mProgressView;
    private View mView;
    notification_adapter adapter;
   // ArrayList<ids_class> arrayList_uid;
    ArrayList<handle_request> arrayList_notif;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_handled_requests);

        rv = findViewById(R.id.rv_handled);
        tv_empty = findViewById(R.id.tv_empty);
        arrayList = new ArrayList<>();
        mProgressView = findViewById(R.id.rh_progress);
        mView = findViewById(R.id.rl_rh);

        showProgress(true);



        getJ();

    }

    private void getJ(){

        Shared_pref shared_pref = new Shared_pref(getApplicationContext());
        final String token = shared_pref.get_token();
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request newRequest = chain.request().newBuilder()
                        .addHeader("Authorization", " Bearer " + token)
                        .build();
                return chain.proceed(newRequest);
            }
        }).build();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://covid-19-info-123.herokuapp.com/")
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        final api_header api = retrofit.create(api_header.class);

        Call<ResponseBody> call = api.seller_accepts();
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                showProgress(false);
                try {

                    String res = null;
                    if (response.body() == null) {
                        tv_empty.setVisibility(View.VISIBLE);

                    } else {
                        tv_empty.setVisibility(View.GONE);

                        res = response.body().string();
                        JSONObject object = new JSONObject(res);
                        JSONArray array = object.getJSONArray("transactions");


                        arrayList_notif = new ArrayList<handle_request>();

                        for (int i = 0; i < array.length(); i++) {
                            handle_request notif_list_class = new handle_request();
                            JSONObject dataobj = array.getJSONObject(i);

                            notif_list_class.set_id(dataobj.getString("_id"));
                            notif_list_class.setDateofRequest(dataobj.getString("date"));
                            notif_list_class.setPid(dataobj.getString("pid"));
                            notif_list_class.setQuantity(dataobj.getString("quantity"));
                            notif_list_class.setUid(dataobj.getString("uid"));
                            notif_list_class.setFinalised(dataobj.getBoolean("finalized"));


                            arrayList_notif.add(notif_list_class);


                        }
                        for (int j = 0; j < arrayList_notif.size(); j++) {
                            String pid = arrayList_notif.get(j).getPid();
                            final String quant = arrayList_notif.get(j).getQuantity();
                            final String date = arrayList_notif.get(j).getDateofRequest();
                            final product_class product_class = new product_class();
                            final String uid = arrayList_notif.get(j).getUid();

                            /////////////////////////////////////////////////
                            Call<ResponseBody> call1 = api.get_user(uid);
                            call1.enqueue(new Callback<ResponseBody>() {
                                @Override
                                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                    String hello = null;
                                    try {
                                        assert response.body() != null;
                                        hello = response.body().string();

                                        //"hello " + hello);
                                        JSONObject object = new JSONObject(hello).getJSONObject("user");
                                        product_class.setBuyer_name(object.getString("name"));

                                    } catch (JSONException | IOException e) {
                                        e.printStackTrace();
                                    }
                                }

                                @Override
                                public void onFailure(Call<ResponseBody> call, Throwable t) {

                                }
                            });

                            ////////////////////////////////////////////////

                            Call<ResponseBody> call2 = api.get_products_by_product(pid);
                            call2.enqueue(new Callback<ResponseBody>() {
                                @Override
                                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                    String hello = null;
                                    try {
                                        assert response.body() != null;
                                        hello = response.body().string();

                                        //"hello " + hello);
                                        JSONObject object = new JSONObject(hello);

                                        JSONObject obj = object.getJSONObject("product");
                                        product_class.setProductName(obj.getString("productName"));
                                        product_class.setProductPrice(obj.getString("productPrice"));
                                        product_class.setProductTypes(obj.getString("productType"));

                                        product_class.setImgThumbnail(obj.getString("imageThumbnail"));
                                        product_class.setBuyer_quantity(quant);
                                        product_class.setDate(getDate(Long.parseLong(date)));
                                        // product_class.setBuyer_name(uid);
                                        arrayList.add(product_class);


                                    } catch (IOException | JSONException e) {
                                        e.printStackTrace();
                                        //e.getMessage());
                                    }

                                    //"arraylist : " + arrayList);

                                    adapter = new notification_adapter(getApplicationContext(), arrayList, handled_requests.this);
                                    rv.setAdapter(adapter);
                                    rv.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));


                                }

                                @Override
                                public void onFailure(Call<ResponseBody> call, Throwable t) {
                                    //"error " + t.getMessage());

                                }
                            });
                        }


                    }
                }catch (IOException | JSONException e) {
                    e.printStackTrace();
                    //e.getMessage());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                //t.getMessage());

            }
        });



    }
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mView.setVisibility(show ? View.GONE : View.VISIBLE);
            mView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });

        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            // tvLoad.setVisibility(show ? View.VISIBLE : View.GONE);
            mView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    private String getDate(long timeStamp){

        try{
            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
            Date netDate = (new Date(timeStamp));
            return sdf.format(netDate);
        }
        catch(Exception ex){
            return "xx";
        }
    }


    @Override
    public void OnnoteClick(int position) {
        if(arrayList_notif.get(position).getFinalised()){
            Toast.makeText(getApplicationContext(),arrayList.get(position).getBuyer_name(),Toast.LENGTH_SHORT).show();

            Intent intent = new Intent(handled_requests.this,request_details.class);
            intent.putExtra("uid",arrayList_notif.get(position).getUid());
            intent.putExtra("img",arrayList.get(position).getImgThumbnail());
            intent.putExtra("date",arrayList_notif.get(position).getDateofRequest());
            intent.putExtra("quantity",arrayList.get(position).getBuyer_quantity());
            intent.putExtra("product_name",arrayList.get(position).getProductName());
            intent.putExtra("product_type",arrayList.get(position).getProductTypes());
            intent.putExtra("product_price",arrayList.get(position).getProductPrice());

            startActivity(intent);
        }
        else{
            Toast.makeText(getApplicationContext(),"Request Rejected",Toast.LENGTH_SHORT).show();
        }

    }

    public void back_to_product_show(View view) {

        startActivity(new Intent(getApplicationContext(),com.example.project.profile.class));
    }
}
