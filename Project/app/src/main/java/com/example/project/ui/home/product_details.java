package com.example.project.ui.home;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CheckedTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.doodle.android.chips.ChipsView;
import com.doodle.android.chips.model.Contact;
import com.example.project.ApplicationClass;
import com.example.project.JWUtils;
import com.example.project.R;
import com.example.project.Shared_pref;
import com.example.project.api_header;
import com.example.project.response_here;
import com.google.gson.Gson;
import com.pusher.client.Pusher;
import com.pusher.client.PusherOptions;
import com.pusher.client.channel.Channel;
import com.pusher.client.channel.ChannelEventListener;
import com.pusher.client.channel.PusherEvent;
import com.pusher.client.channel.SubscriptionEventListener;
import com.pusher.client.connection.ConnectionEventListener;
import com.pusher.client.connection.ConnectionState;
import com.pusher.client.connection.ConnectionStateChange;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.awt.font.TextAttribute;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class product_details extends AppCompatActivity {
    TextView tv_name, tv_dec, tv_type, tv_quantity, tv_price, tv_Seller_name, tv_seller_id, tv_seller_email;
    EditText req_quant;
    String id;
    ChipsView tv_tags;
    private ChipsView mChipsView;
    String productCode , quant ,product_id;
    CheckedTextView tv_avail, tv_certified;
    String name = null;
    String email;

    Intent mServiceIntent;
    public YourService mYourService;
    String id1;


    ImageView iv;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_details);
        tv_dec = findViewById(R.id.tv_desc);
        tv_avail = findViewById(R.id.check_avail);
        tv_certified = findViewById(R.id.check_cert);
        req_quant = findViewById(R.id.reqd_quant);
        tv_seller_id = findViewById(R.id.seller_id_prod);
        tv_Seller_name = findViewById(R.id.seller_name_prod);
        tv_seller_email = findViewById(R.id.seller_email_prod);
        tv_name = findViewById(R.id.tv_name);
        mYourService = new YourService();
        mServiceIntent = new Intent(this, mYourService.getClass());
        tv_type = findViewById(R.id.tv_type);
        tv_price = findViewById(R.id.tv_price);
        tv_quantity = findViewById(R.id.tv_quantity);

        iv = findViewById(R.id.iv_product_seller);
        mChipsView = findViewById(R.id.chips);
        mChipsView.getEditText().setCursorVisible(true);

        mChipsView.setChipsValidator(new ChipsView.ChipValidator() {
            @Override
            public boolean isValid(Contact contact) {
                if (contact.getDisplayName().equals("check")) {
                    return false;
                }
                return true;
            }
        });



        getprod();
        if(getIntent().getStringExtra("selleruid") != null){
            fill_seller_uid();
        }
        else{
            fill_seller();
        }

        push();

    }

    private void fill_seller_uid() {

        String sellerid = getIntent().getStringExtra("selleruid");

        Shared_pref shared_pref = new Shared_pref(getApplicationContext());
        final String token = shared_pref.get_token();
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request newRequest = chain.request().newBuilder()
                        .addHeader("Authorization", " Bearer " + token)
                        .build();
                return chain.proceed(newRequest);
            }
        }).build();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://covid-19-info-123.herokuapp.com/")
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        api_header api = retrofit.create(api_header.class);

        Call<ResponseBody> call = api.get_seller_by_uid(sellerid);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(response.code() == 200){
                    try {
                        String resp = response.body().string();
                        JSONObject object = new JSONObject(resp).getJSONObject("seller");
                        tv_Seller_name.setText(object.getString("sellerName"));
                        tv_seller_id.setText(object.getString("_id"));
                        id1 = object.getString("uid");
                        tv_seller_email.setText(object.getString("sellerEmail"));


                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(getApplicationContext(),e.getMessage(),Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });



    }

    protected void sendEmail(String strings) {


        Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
        emailIntent.setData(Uri.parse("mailto:" + strings));

        //emailIntent.setDataAndType(Uri.parse("mailto:"),"message/rfc822");


        emailIntent.putExtra(Intent.EXTRA_EMAIL, strings);


        try {
            startActivity(Intent.createChooser(emailIntent, "Send mail..."));


        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(product_details.this,
                    "There is no email client installed.", Toast.LENGTH_SHORT).show();
        }
    }
    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                Log.i ("Service status", "Running");
                return true;
            }
        }
        Log.i ("Service status", "Not running");
        return false;






        //launchservice();

    }

    @Override
    protected void onDestroy() {
        stopService(mServiceIntent);
        super.onDestroy();
    }

    public void show_images(View view){
        id = getIntent().getStringExtra("id");
        String uid = getIntent().getStringExtra("selleruid");
        String sellerid = getIntent().getStringExtra("seller");

        Intent intent = new Intent(getApplicationContext(),images.class);
        intent.putExtra("seller",sellerid);
        intent.putExtra("id",id);
        intent.putExtra("uid",uid);
        startActivity(intent);

    }

    private void getprod() {
        id = getIntent().getStringExtra("id");
        Shared_pref shared_pref = new Shared_pref(getApplicationContext());
        final String token = shared_pref.get_token();
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request newRequest = chain.request().newBuilder()
                        .addHeader("Authorization", " Bearer " + token)
                        .build();
                return chain.proceed(newRequest);
            }
        }).build();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://covid-19-info-123.herokuapp.com/")
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        api_header api = retrofit.create(api_header.class);

        Call<ResponseBody> call = api.get_products_by_product(id);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    assert response.body() != null;
                    String hello = response.body().string();

                    JSONObject object = new JSONObject(hello);

                    JSONObject obj = object.getJSONObject("product");

                    tv_name.setText(obj.getString("productName"));
                    tv_dec.setText(obj.getString("productDesc"));
                    tv_type.setText(obj.getString("productType"));

                    if(obj.getString("quantity").equals(String.valueOf(1000000000))){
                        tv_quantity.setText(String.valueOf("not defined by seller"));
                    }
                    else{
                        tv_quantity.setText(obj.getString("quantity"));}
                   // tv_quantity.setText(obj.getString("quantity"));
                    tv_price.setText(obj.getString("productPrice"));
                    productCode = obj.getString("productCode");
                    product_id = obj.getString("_id");
                    quant = obj.getString("quantity");


                    if(obj.getString("availablity").equals("true")){
                        tv_avail.setChecked(true);
                        tv_avail.setCheckMarkDrawable(R.drawable.ic_done_black_24dp);
                    }
                    else{
                        tv_avail.setChecked(false);
                        tv_avail.setCheckMarkDrawable(R.drawable.ic_close_black_24dp);
                    }
                    if(obj.getString("isCertified").equals("true")){
                        tv_certified.setChecked(true);
                        tv_certified.setCheckMarkDrawable(R.drawable.ic_done_black_24dp);
                    }
                    else{
                        tv_certified.setChecked(false);
                        tv_certified.setCheckMarkDrawable(R.drawable.ic_close_black_24dp);
                    }


                    String tags;

                    tags = obj.getString("productTags").replace("[","");
                    tags = tags.replace("]","");

                    String[] tg;

                    tg = tags.split(",");
                    for (String s : tg) {
                        //"tags " +s);
                        Contact contact2 = new Contact(null, null,  s,s,null);

                        mChipsView.addChip(s, null, contact2, false);


                    }
                    TextView editText = null;
                    for (int i = 0; i < mChipsView.getChildCount(); i++) {
                        if(mChipsView.getChildAt(i) instanceof EditText){
                            editText = (TextView) mChipsView.getChildAt(i);
                            editText.setCursorVisible(false);
                            editText.setEnabled(false);
                        }
                    }
                    if(editText != null){
                        // do whatever you want to do with this EditText
                        editText.setCursorVisible(false);
                        editText.setEnabled(false);
                    }

                    //

                    Picasso.with(getApplicationContext()).load(Uri.parse((obj.getString("imageThumbnail")).trim())).into(iv);




                    //   writeRecycler(hello);
                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(),"error : " +e.getMessage() ,Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(getApplicationContext(),"error : " +t.getMessage() ,Toast.LENGTH_SHORT).show();

            }
        });

    }


    private void fill_seller(){
        String sellerid = getIntent().getStringExtra("seller");

        Shared_pref shared_pref = new Shared_pref(getApplicationContext());
        final String token = shared_pref.get_token();
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request newRequest = chain.request().newBuilder()
                        .addHeader("Authorization", " Bearer " + token)
                        .build();
                return chain.proceed(newRequest);
            }
        }).build();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://covid-19-info-123.herokuapp.com/")
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        api_header api = retrofit.create(api_header.class);

        Call<ResponseBody> call = api.get_seller_from_id(sellerid);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(response.code() == 200){
                    try {
                        String resp = response.body().string();
                        JSONObject object = new JSONObject(resp).getJSONObject("seller");
                        tv_Seller_name.setText(object.getString("sellerName"));
                        tv_seller_id.setText(object.getString("_id"));
                        id1 = object.getString("uid");
                        tv_seller_email.setText(object.getString("sellerEmail"));


                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(getApplicationContext(),e.getMessage(),Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });


    }

    public void back(View view) {
        startActivity(new Intent(this, com.example.project.profile.class));
    }

    public void push() {
            String cluster = getString(R.string.pusher_cluster);
            String key = getString(R.string.pusher_key);
            String channel_name = getString(R.string.channel_name);
            String event_name = getString(R.string.event_name);
            PusherOptions options = new PusherOptions().setCluster(cluster);

            Pusher pusher = new Pusher(key, options);
            pusher.connect(new ConnectionEventListener() {
                @Override
                public void onConnectionStateChange(ConnectionStateChange change) {
                  /*  //"State changed to " + change.getCurrentState() +
                            " from " + change.getPreviousState());
*/

                }

                @Override
                public void onError(String message, String code, Exception e) {
                    //"There was a problem connecting! : " + message);
                }
            }, ConnectionState.ALL);

            final Channel channel = pusher.subscribe(channel_name, new ChannelEventListener() {         //sos

                @Override
                public void onSubscriptionSucceeded(String channelName) {
                    //"Subscribed!");
                    //channelName);




                }

                @Override
                public void onEvent(PusherEvent event) { //notification

                    //event.getData());
                    try {
                        Shared_pref shared_pref = new Shared_pref(product_details.this);
                        JSONObject obj = new JSONObject(event.getData());
                        String sid = obj.getString("sid");
                        String uid = obj.getString("uid");
                        String quantity = obj.getString("quantity");
                        String pid = obj.getString("pid");
                        String user =  JWUtils.decodeJWT(shared_pref.get_token());
                        JSONObject obj2 = new JSONObject(user);
                        //obj2);
                        JSONObject object = obj2.getJSONObject("user");
                        String use = object.getString("_id");

                        //"_id" +use);
                        //"sid " + sid);

                        if(use.equals(sid)){

                            if (!isMyServiceRunning(mYourService.getClass())) {
                                startService(mServiceIntent);
                            }


                            NotificationManagerCompat notificationManagerCompat;
                            notificationManagerCompat = NotificationManagerCompat.from(product_details.this);
                            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                                NotificationChannel channel1 = new NotificationChannel(
                                        "channel1","channel1", NotificationManager.IMPORTANCE_DEFAULT
                                );

                                channel1.setDescription("Request received!");
                                NotificationManager manager = getSystemService(NotificationManager.class);
                                assert manager != null;
                                manager.createNotificationChannel(channel1);

                            }

                            Context context = getApplicationContext();
                            Date currentTime = Calendar.getInstance().getTime();
                            Intent myIntent = new Intent(context, com.example.project.ui.home.notification_list.class);
                            myIntent.putExtra("quantity", quantity);
                            myIntent.putExtra("uid",uid);
                            myIntent.putExtra("pid",pid);
                            myIntent.putExtra("sid",sid);
                            myIntent.putExtra("date",currentTime.toString());
                            //"intent : " + uid + sid + pid + quantity);
                            myIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            PendingIntent intent = PendingIntent.getActivity(context,1,myIntent,PendingIntent.FLAG_UPDATE_CURRENT);

                            if (!isMyServiceRunning(mYourService.getClass())) {
                                startService(mServiceIntent);
                            }


                            Notification notification = new NotificationCompat.Builder(product_details.this,"channel1").setContentTitle("Request").setContentText("request for product : " + pid)

                                    .setPriority(4).setCategory(NotificationCompat.CATEGORY_MESSAGE).setDefaults(Notification.DEFAULT_SOUND).setAutoCancel(true)
                                    .setContentIntent(intent)
                                    .setAutoCancel(true)
                                    .setSmallIcon(R.drawable.ic_menu_send)
                                    .build();

                            notificationManagerCompat.notify(1,notification);


                        }



                    } catch (Exception e) {
                        e.printStackTrace();
                        //e.getMessage());
                    }

                }



            },event_name);

        }




    public void buynow(View view) {
       Toast.makeText(this,"ok",Toast.LENGTH_LONG).show();
       final Shared_pref shared_pref = new Shared_pref(this);


        //push();
        if(shared_pref.get_Address().equals("earth")){
            Toast.makeText(this,"You have to register first",Toast.LENGTH_LONG).show();
            startActivity(new Intent(getApplicationContext(),com.example.project.Register.class));

        }
        else {

            if (req_quant.getText().toString().isEmpty()) {
                Toast.makeText(this, "specify the Quantity", Toast.LENGTH_LONG).show();

            } else {


                //final Shared_pref shared_pref = new Shared_pref(this);
                final String token = shared_pref.get_token();
                // //token);
                OkHttpClient client = new OkHttpClient.Builder().addInterceptor(new Interceptor() {
                    @Override
                    public okhttp3.Response intercept(Chain chain) throws IOException {
                        Request newRequest = chain.request().newBuilder()
                                .addHeader("Authorization", " Bearer " + token)
                                .build();
                        return chain.proceed(newRequest);
                    }
                }).build();
                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl("https://covid-19-info-123.herokuapp.com/")
                        .client(client)
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();

                api_header api = retrofit.create(api_header.class);

                buy buy = new buy();
                buy.setProductCode(product_id);

                buy.setQuantity(req_quant.getText().toString());





                Call<response_here> call = api.post_buy(buy);
                call.enqueue(new Callback<response_here>() {
                    @Override
                    public void onResponse(Call<response_here> call, Response<response_here> response) {
                        //"response");
                        if (response.code() == 200) {
                            assert response.body() != null;

                            /*shared_pref.clear();
                            shared_pref.add_token(response.body().getToken());
                            //response.body().getToken());*/

                            push();

                            sendmail();

                            Toast.makeText(getApplicationContext(),"Request Sent!",Toast.LENGTH_LONG).show();

                            startActivity(new Intent(getApplicationContext(),com.example.project.profile.class));
                        } else {

                            try {
                               Toast.makeText(getApplicationContext(),response.errorBody().string(),Toast.LENGTH_SHORT).show();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }

                        }


                    }

                    @Override
                    public void onFailure(Call<response_here> call, Throwable t) {
                        //"error2  : " + t.getMessage());
                    }
                });


            }
        }



    }

    private void sendmail() {
        /*Shared_pref shared_pref = new Shared_pref(getApplicationContext());
        final String token = shared_pref.get_token();
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request newRequest = chain.request().newBuilder()
                        .addHeader("Authorization", " Bearer " + token)
                        .build();
                return chain.proceed(newRequest);
            }
        }).build();*/

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://covid-19-info-123.herokuapp.com/")
               // .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        api_header api = retrofit.create(api_header.class);
        mail mail = new mail();

        Shared_pref shared_pref = new Shared_pref(getApplicationContext());
        try {
            String token = JWUtils.decodeJWT(shared_pref.get_token());
            JSONObject jsonObject = new JSONObject(token).getJSONObject("user");
             name = jsonObject.getString("name");
             email = jsonObject.getString("email");

        } catch (Exception e) {
            e.printStackTrace();


        }


        mail.setEmail(tv_seller_email.getText().toString());
        mail.setSubject("Arogya Karan : Request For Product");
        String content="<h1>Request Received</h1>\n" +
                "        <h2>Request for Product</h2>\n" +
                "        <p>Product Name :  <b>"+ tv_name.getText().toString()  +"</b>.</p>\n" +
                "        <p>Product Type :  <b>"+ tv_type.getText().toString()  +"</b>.</p>\n" +
                "        <p>Requested By :  <b>"+  name  +"</b>.</p>\n" +
                "        <p>Requested By :  <b>"+  email  +"</b>.</p>\n" +
                "        <p>Quantity Requested :  <b>"+  req_quant.getText().toString()  +"</b>.</p>\n";


        mail.setMsg(content);
        mail.setPasscode("0b0YghA8vfVbQhv1q6EB5pRlU3zxUsKCtbOUXNN2JFdNZHttzGEa7q6TsxYAXwk");

        Call<ResponseBody> call = api.send_email(mail);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(response.code() == 200){
                    Toast.makeText(getApplicationContext(),"Mail Sent",Toast.LENGTH_SHORT).show();
                }
                else{
                    Toast.makeText(getApplicationContext(),"error : " + response.message(),Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });


    }


    public void email(View view) {
        String strings = tv_seller_email.getText().toString();
        sendEmail(strings);
    }
}

