package com.example.project;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class business extends AppCompatActivity {

    EditText et_business_name, et_business_email, et_business_phone, et_business_address_line1, et_business_address_line2, et_business_state,
    et_business_city, et_business_pin_code, et_business_country;
    Button btn_business_submit;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_business);

        et_business_address_line1 = findViewById(R.id.et_business_address);
        et_business_address_line2 = findViewById(R.id.et_business_address_line_2);
        et_business_city = findViewById(R.id.et_business_city);
        et_business_state = findViewById(R.id.et_business_state);
        et_business_email = findViewById(R.id.et_business_email);
        et_business_name = findViewById(R.id.et_business_name);
        et_business_phone = findViewById(R.id.et_business_phone_no);
        et_business_pin_code = findViewById(R.id.et_business_pin_code);
        et_business_country = findViewById(R.id.et_business_country);
        btn_business_submit = findViewById(R.id.bt_business_submit);

        //add_seller();




    }
    private void add_seller(){
        final Shared_pref shared_pref = new Shared_pref(this);
        final String token = shared_pref.get_token();

        add_seller_class add_seller_class = new add_seller_class();
        add_seller_class.setName(et_business_name.getText().toString());

        add_seller_class.setAddress(et_business_address_line1.getText().toString() + " , " + et_business_address_line2.getText().toString() +" , " + et_business_city.getText().toString()
         + " , " + et_business_state.getText().toString() + " , " + et_business_pin_code.getText().toString() + " , " + et_business_country.getText().toString());
        String geo = et_business_address_line1.getText().toString() + " , " + et_business_address_line2.getText().toString() +" , " + et_business_city.getText().toString()
                + " , " + et_business_state.getText().toString() + " , " + et_business_pin_code.getText().toString() + " , " + et_business_country.getText().toString();

        add_seller_class.setEmail(et_business_email.getText().toString());
        add_seller_class.setPhone(Long.parseLong(et_business_phone.getText().toString()));

        add_seller_class.setBusiness(true);
       // add_seller_class.setNewAddress(false);

        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        List<Address> addresses = null;
        try {
            addresses = geocoder.getFromLocationName(geo, 1);
            Address address = addresses.get(0);
            double longitude = address.getLongitude();
            double latitude = address.getLatitude();
            add_seller_class.setLongi(String.valueOf(longitude));
            add_seller_class.setLat(String.valueOf(latitude));



        } catch (Exception e) {
            e.printStackTrace();
           Toast.makeText(getApplicationContext(),e.getMessage(),Toast.LENGTH_SHORT).show();
        }




        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request newRequest  = chain.request().newBuilder()
                        .addHeader("Authorization", "Bearer " + token)
                        .build();
                return chain.proceed(newRequest);
            }
        }).build();
        Retrofit retrofit  = new Retrofit.Builder()
                .baseUrl("https://covid-19-info-123.herokuapp.com/")
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        api_header api = retrofit.create(api_header.class);



        Call<response_here> call = api.add_seller_jwt(add_seller_class);
        call.enqueue(new Callback<response_here>() {
            @Override
            public void onResponse(Call<response_here> call, Response<response_here> response) {
                if(response.code() == 200){
                    assert response.body() != null;

                    String new_tok = response.body().getToken();


                    shared_pref.is_Business(true);

                    if(!new_tok.isEmpty()) {
                        shared_pref.clear();
                        shared_pref.add_token(new_tok);

                        if (shared_pref.get_flag()!=1) {

                            shared_pref.add_flag(1);
                        }
                        startActivity(new Intent(getApplicationContext(),com.example.project.ui.send.products_show.class));

                    }


                }
                else{


                    assert response.errorBody() != null;
                    Toast.makeText(getApplicationContext(),""+response.errorBody(),Toast.LENGTH_SHORT).show();


                }

            }

            @Override
            public void onFailure(Call<response_here> call, Throwable t) {

            }
        });
    }

    public void business_submit(View view) {
        if(isEmpty(et_business_name) || isEmpty(et_business_phone) || isEmpty(et_business_email) || isEmpty(et_business_address_line1)||isEmpty(et_business_address_line2)
        || isEmpty(et_business_city) || isEmpty(et_business_state) || isEmpty(et_business_pin_code) || isEmpty(et_business_country)){
            Toast.makeText(getApplicationContext(),"All fields Compulsory",Toast.LENGTH_SHORT).show();
        }
        else{
            add_seller();
        }



    }
    private boolean isEmpty(EditText etText) {

        if(etText.getText().toString().trim().length() == 0){
            etText.setError("Cannot be Empty");

            return true;
        }
        else{
            return false;
        }
    }

    public void businessback(View view) {
        startActivity(new Intent(this,profile.class));
    }
}
