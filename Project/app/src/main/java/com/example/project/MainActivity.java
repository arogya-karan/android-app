package com.example.project;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.TaskStackBuilder;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.tasks.Task;

import org.json.JSONObject;

import java.io.IOException;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.FormUrlEncoded;

public class MainActivity extends AppCompatActivity {
    EditText et_username, et_password, et_enter_email;

    TextView tv_welcome, tv_signin, gotoreg, forgotpassword;
    private View mProgressView;
    private View mView;
    Button btn_submit_forgot, btnsignin;
    //GoogleSignInClient googleSignInClient;

    //String client_web_id";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        et_password = findViewById(R.id.et_password);
        et_username = findViewById(R.id.et_username);
        tv_signin = findViewById(R.id.tvsignin);
        tv_welcome = findViewById(R.id.tv_welcome);
        et_enter_email = findViewById(R.id.et_enter_email);
        mProgressView = findViewById(R.id.proglogin);
        mView = findViewById(R.id.rl_login);
        btn_submit_forgot = findViewById(R.id.btn_forgot_submit);
        btnsignin = findViewById(R.id.btn_signin);
        et_enter_email.setVisibility(View.GONE);
        btn_submit_forgot.setVisibility(View.GONE);
        gotoreg = findViewById(R.id.go_to_reg);
        btnsignin = findViewById(R.id.btn_signin);
        forgotpassword = findViewById(R.id.forgot_password);




        /*GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .requestScopes(new Scope(Scopes.DRIVE_APPFOLDER))
                .requestServerAuthCode(client_web_id)
                .requestIdToken(client_web_id)
                .build();
        googleSignInClient = GoogleSignIn.getClient(this,gso);
*/

    }
/*

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == 100 && resultCode== RESULT_OK){
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handlesigninresult(task);

        }
        else{

        }
    }
*/

   /* private void handlesigninresult(Task<GoogleSignInAccount> task){


        try {
            GoogleSignInAccount account = task.getResult(ApiException.class);

            generateAccessToken(account.getServerAuthCode());

        } catch (ApiException e) {
            e.printStackTrace();
        }
    }
    public  void generateAccessToken(String authcode){
        OkHttpClient client =  new OkHttpClient();
        RequestBody requestBody = new FormBody.Builder()
                .add("grantType" , "authorization code")
                .add("clientid" , client_web_id)
                .add("")
    }
*/
    public void login(View view) {

        if(isEmpty(et_username) || isEmpty(et_password)){

        }
        else{
            login_user login_user = new login_user();
            showProgress(true);
            login_user.setEmail(et_username.getText().toString().trim());
            login_user.setPassword(et_password.getText().toString().trim());
            Call<response_here> call = retro_class.getInstance_two().getapi2().login_user(login_user);
            call.enqueue(new Callback<response_here>() {
                @Override
                public void onResponse(Call<response_here> call, Response<response_here> response) {
                    if(response.code() == 200 ){
                        assert response.body() != null;
                        //response.body().isSuccess());
                        //response.body().getToken());
                        showProgress(false);
                        Toast.makeText(MainActivity.this,"Logged In",Toast.LENGTH_LONG).show();
                        startActivity(new Intent(MainActivity.this,com.example.project.ui.welcome.class));
                        Shared_pref shared_pref = new Shared_pref(MainActivity.this);
                        // shared_pref.add_flag(1);
                        try {
                            String user = JWUtils.decodeJWT(response.body().getToken());
                            shared_pref.clear();
                            shared_pref.add_token(response.body().getToken());
                            JSONObject object = new JSONObject(user);
                            JSONObject obj = object.getJSONObject("user");
                            String address = obj.getString("address");
                            Boolean isSeller = obj.getBoolean("isSeller");

                            if(isSeller){
                                shared_pref.add_flag(1);
                            }

                            if(shared_pref.get_Address().equals("earth")) {
                                shared_pref.add_address(address);
                            }
                            // shared_pref.add_address("earth");
                            //object);

                        } catch (Exception e) {
                            showProgress(false);
                            e.printStackTrace();
                            Toast.makeText(MainActivity.this,"error : " + e.getMessage(),Toast.LENGTH_LONG).show();
                        }


                    }
                    else {
                        showProgress(false);
                        //response.message());
                        try {
                            Toast.makeText(getApplicationContext(),response.errorBody().string(),Toast.LENGTH_SHORT).show();
                        } catch (IOException e) {
                            e.printStackTrace();
                            Toast.makeText(MainActivity.this,"error : " + e.getMessage(),Toast.LENGTH_LONG).show();
                        }
                    }
                }

                @Override
                public void onFailure(Call<response_here> call, Throwable t) {
                    showProgress(false);
                    Toast.makeText(MainActivity.this,"error : " + t.getMessage(),Toast.LENGTH_LONG).show();

                }
            });


        }


    }

    public void got_to_register(View view) {

        startActivity(new Intent(this,com.example.project.Register.class));
    }

    public void forgot_password(View view) {

        et_enter_email.setVisibility(View.VISIBLE);
        et_username.setVisibility(View.GONE);
        et_password.setVisibility(View.GONE);
        btn_submit_forgot.setVisibility(View.VISIBLE);
        tv_welcome.setText("Forgot Password?");
        tv_signin.setText("Enter Email");
        forgotpassword.setVisibility(View.GONE);
        btnsignin.setVisibility(View.GONE);
        gotoreg.setVisibility(View.GONE);



        btn_submit_forgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!et_enter_email.getText().toString().isEmpty()) {

                    pass pass = new pass();
                    pass.setUser_email(et_enter_email.getText().toString());
                    Call<response_here> call = retro_class.getInstance_two().getapi2().forg(pass);
                    showProgress(true);
                    call.enqueue(new Callback<response_here>() {
                        @Override
                        public void onResponse(Call<response_here> call, Response<response_here> response) {
                            //et_enter_email.getText().toString());
                            if (response.code() == 200) {

                                assert response.body() != null;
                                showProgress(false);
                                Toast.makeText(getApplicationContext(),"A link has been sent to your email",Toast.LENGTH_SHORT).show();
                                //"success log : " + response.body().isSuccess() + response.body().getMessage());
                            } else {
                                try {
                                    assert response.errorBody() != null;
                                    showProgress(false);
                                    Toast.makeText(getApplicationContext(),response.errorBody().string(),Toast.LENGTH_SHORT).show();
                                    Toast.makeText(MainActivity.this,"error " + response.message(),Toast.LENGTH_LONG).show();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }


                        }

                        @Override
                        public void onFailure(Call<response_here> call, Throwable t) {
                            showProgress(false);
                            Toast.makeText(MainActivity.this,"error : " + t.getMessage(),Toast.LENGTH_LONG).show();

                        }
                    });
                }
                else{
                    showProgress(false);
                    Toast.makeText(MainActivity.this,"Enter Email",Toast.LENGTH_LONG).show();
                }

        }
            });





       


    }

    private boolean isEmpty(EditText etText) {

        if(etText.getText().toString().trim().length() == 0){
            etText.setError("Cannot be Empty");

            return true;
        }
        else{
            return false;
        }
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mView.setVisibility(show ? View.GONE : View.VISIBLE);
            mView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });

        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            // tvLoad.setVisibility(show ? View.VISIBLE : View.GONE);
            mView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }



}
